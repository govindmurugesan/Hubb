var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jquery = require('jquery');
var mongoose = require('mongoose');

var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var session = require('express-session');

var index = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
/*var passport1 = require('./utils/passportAuthentication');*/

var app = express();

/*require('./utils/passportAuthentication')(passport);*/
/*app.use(passport.initialize());
app.use(passport.session());*/

// configure passport

/*var aaaa = require('./dbModel/loginUsers.js');*/
/*passport.use(new localStrategy(aaaa.authenticate()));
passport.serializeUser(aaaa.serializeUser());
passport.deserializeUser(aaaa.deserializeUser());*/






// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*app.use('/', index);*/
app.use('/', login);
app.use('/index', index);
app.use('/users', users);
app.use('/login', login);

app.use('/menu', require('./server/menus'));

app.use(session({
	secret: "learn node",
	resave: true,
	saveUninitialized: false
}))


var db = require('./dbModel/dbConnection.js');
var redis = require('./utils/redis.js');
/*var invite1 = require('./server/invites');*/

app.use('/adddd', function(req, res, next){console.log('dsadasd')});
app.use('/invite', require('./server/invites'));
app.use('/loginuser', require('./server/userLogin'));
app.use('/roles', require('./server/roles'));
app.use('/add', require('./server/addTestData'));
app.use('/activateuser', require('./routes/userActivation'));
app.use('/passwordReset', require('./routes/passwordReset'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
   var err = new Error('Not Found');
   err.status = 404;
   next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
