var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
    /*FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require( 'passport-google-oauth2' ).Strategy,*/
    /*appConstant = require('./appConstant');*/
    /*constantList = require('./constantList');*/









exports.authenticationMiddleware = authenticationMiddleware;
module.exports = function(){

    /*passport.use( new FacebookStrategy({
            clientID: appConstant.facebookClientID,
            clientSecret : appConstant.facebookClientSecret,
            callbackURL : appConstant.facebookCallBack,
            profileFields: ['email', 'id', 'name', 'picture', 'birthday', 'gender', 'about', 'education']
        },
        function(accessToken, refreshToken, profile, done) {

            process.nextTick(function () {
            
                var profileInfo =  {
                    name: {
                        first: profile.name.givenName,
                        last: profile.name.familyName
                    },
                    email : [{
                        address : profile.emails[0].value,
                    }],
                    gender : profile.gender,
                    photos:  profile.photos,
                    providerinfo: {
                        name: profile.provider,
                        id: profile.id,
                    },
                    status: constantList.activeStatus,
                    joinDate : Date.now()
                }

                return done(null, profileInfo);
            })
          
        }
    ));

    passport.use(new GoogleStrategy({
        clientID:     appConstant.googleClientID,
        clientSecret: appConstant.googleClientSecret,
        callbackURL: appConstant.googlekCallBack,
    },
    function(request, accessToken, refreshToken, profile, done) {
        var profileInfo =  {
                    name: {
                        first: profile.name.givenName,
                        last: profile.name.familyName
                    },
                    email : [{
                        address : profile.email,
                    }],
                    gender : profile.gender,
                    photos:  profile.photos,
                    providerinfo: {
                        name: profile.provider,
                        id: profile.id,
                    },
                    status: constantList.activeStatus,
                    joinDate : Date.now()
                }
                return done(null, profileInfo);
    }));
*/

    passport.use(new LocalStrategy(  
      function(username, password, done) {
        findUser(username, function (err, user) {
          if (err) {
            return done(err)
          }
          if (!user) {
            return done(null, false)
          }
          if (password !== user.password  ) {
            return done(null, false)
          }
          return done(null, user)
        })
      }

    passport.serializeUser(function(user, done) {
       // console.log("serializeUser",user)
        done(null, user);
    });

    /*passport.deserializeUser(function(user, done) {
        done(null, user);
    });*/

 
    passport.deserializeUser(function(id, done) {
      Users.findById(id, function(err, user) {
        done(err, user);
      });
    });
};