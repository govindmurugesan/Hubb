var  redis = require('./redis');
var redisClientObj  = redis.redisClient;

module.exports.verifyToken = verifyToken;

function verifyToken(req, res, next){

  var checkToken;
  checkToken = getToken(req.headers);
  if(checkToken.exists){
   redisClientObj.hgetall(checkToken.redisToken, function(err, reply){
      if(err){
        if(res) return res.sendStatus(500);
        else next(new Error(err));
      }
      if(reply === null || undefined){
        if(res)return res.sendStatus(401); 
        else return next(new Error('You need to be logged in to do this'));
      } else {
        console.log("its verified")
        next({verified: true});
      }
    });
  }else{
     res.json({error: 'Error creating authentication token'});
  }
}


function getToken(reqheaders){
  var tokenInfo = {exists: false, token: ''};
  if(reqheaders && reqheaders.authorization){
    var auth = reqheaders.authorization;
    if(auth){
      tokenInfo.exists = true;
      tokenInfo.token = auth;
      tokenInfo.redisToken = '_token:' + auth;
      return tokenInfo;
    } else {
      return tokenInfo;
    }
  } else {
    return tokenInfo;
  }

}
