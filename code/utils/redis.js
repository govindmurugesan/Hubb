var redis = require('redis'),
  redisClient;

  redisClient = redis.createClient(6379);
  console.log('Redis local: Connecting to local redis.');


redisClient.on('error', function(err) {
  console.log('Redis: ' + err);
    process.exit(1);
});

redisClient.on('connect', function() {
  console.log('Redis: Started');
});

exports.redis = redis;
exports.redisClient = redisClient;
