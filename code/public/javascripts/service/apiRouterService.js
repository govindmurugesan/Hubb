(function() {
'use strict';

	var apiRoutesUrl = {
		METHOD_GET : 'GET',
		METHOD_POST : 'POST',

		//invite user
		inviteUser : 'invite/services',

		//login
		loginUser : 'loginuser/login',
		getUser:'loginuser/getUser',
		addUser: 'loginuser/addUser',
		resetUser: 'loginuser/resetUser',
		forgot: 'loginuser/forgot',

		menus: 'menu/getMenus',

		//roles
		getRoles:'roles/getRoles',
		roleUrl:'roles/addrole',
		roleAppUrl:'roles/addroleapp',
		getRolesApp:'roles/getRoleApp'



		
	};	

	angular.module('app').constant('apiRouterService', apiRoutesUrl);

	})();
