angular.module('app').factory('userService', userService);

userService.$inject = ['$q', '$http', 'apiRouterService'];

function userService($q, $http, apiRouterService){

	var service = {
		addUser: addUser,
        getUser: getUser
	}
	return service;

	function addUser(invite){
		var deferred = $q.defer();
        var userInvite = {
            email : invite.email,
            role : invite.role
        }
        console.log("userInvite ",userInvite);
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.inviteUser,
            data : userInvite
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}

    function getUser(){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_GET,
            url : apiRouterService.getUser
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
    }

}