angular.module('app').factory('menuService', menuService);

menuService.$inject = ['$q', '$http', 'apiRouterService'];

function menuService($q, $http, apiRouterService){
	var service = {
		getAllMenu: getAllMenu
	}
	return service;

	function getAllMenu(){
		console.log("service menu");
		var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_GET,
            url : apiRouterService.menus
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}
}