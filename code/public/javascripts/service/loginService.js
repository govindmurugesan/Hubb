angular.module('app').factory('loginService', loginService);

loginService.$inject = ['$q', '$http', 'apiRouterService'];

function loginService($q, $http, apiRouterService){

	var service = {
		login: login,
		forgot: forgot,
		activateUser: activateUser,
        passwordReset: passwordReset
	}
	return service;

	function login(user){
		var deferred = $q.defer();
		var userData = {
			email: user.email,
			password: user.password
		}
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.loginUser,
            data : userData
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}

function forgot(email){
    console.log("email",email);
    var deferred = $q.defer();
    var userData = {
            token: email.token,
            email: email
        }
		
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.forgot,
            data : userData
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}
	function activateUser(user, InviteId){
		var deferred = $q.defer();
		var activeData = {
			password: user.newPassword,
			InviteId: InviteId
		}
		console.log("activeData ",activeData);
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.addUser,
            data : activeData
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}
    function passwordReset(user){
        var deferred = $q.defer();
        var activeData = {
            token: user.token,
            password: user.newPassword,
        }
        console.log("activeData ",activeData);
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.resetUser,
            data : activeData
        }).then(function(data){
            console.log("data",data)
            deferred.resolve(data);
        }, function(error){    
        console.log("error",error)        
            deferred.reject(error);
        });
        return deferred.promise;
    }
}