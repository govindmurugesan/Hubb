angular.module('app').factory('roleService', roleService);

roleService.$inject = ['$q', '$http', 'apiRouterService'];

function roleService($q, $http, apiRouterService){
	var service = {
		addRoleApp: addRoleApp,
        addRole: addRole,
        getAllRoles: getAllRoles,
        getAllRolesApp: getAllRolesApp
	}
	return service;

	function addRoleApp(roleapp){
		console.log("service addRoleApp", roleapp);
		var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.roleAppUrl,
            data : roleapp
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
	}

    function addRole(role){
        console.log("service addRoleApp", role);
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.roleUrl,
            data : role
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getAllRoles(){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_GET,
            url : apiRouterService.getRoles            
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
    }

     function getAllRolesApp(id){
        console.log("id  ", id);
        var dataa = {
            userid : id 
        }
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_GET,
            url : apiRouterService.getRolesApp+'?roleId='+id
        }).then(function(data){
            deferred.resolve(data);
        }, function(error){            
            deferred.reject(error);
        });
        return deferred.promise;
    }

}