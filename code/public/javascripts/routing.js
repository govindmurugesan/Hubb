angular.module('app').config(config);
   config.$inject = ['$routeProvider']

    	function config($routeProvider){
	    	   $routeProvider.
	    	    when('/', {
	               templateUrl: '/templates/dashboard.html',
	               controller: 'dashboardController as vm'
	            }).
	    	    when('/dashboard', {
	              templateUrl: '/templates/dashboard.html',
	              controller: 'dashboardController as vm'
	           }).
	    	    when('/users', {
	              templateUrl: '/templates/users.html',
	              controller: 'userController as vm'
	           }).
	    	    when('/roles', {
	              templateUrl: '/templates/roles.html',
	              controller: 'rolesController as vm'
	           }).
	    	     when('/configuration', {
	              templateUrl: '/templates/configuration.html',
	            	controller: 'configurationController as configurationControllerScope'
	           }).
	    	     when('/companyinfomation', {
	              templateUrl: '/templates/companyinformation.html',
	            	controller: 'companyinformationController as companyinformationControllerScope'
	           }).
	    	    
	    	    otherwise({
	              redirectTo: '/'
	           });
        }