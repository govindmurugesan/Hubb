angular.module('app').controller('loginController', loginController);


  loginController.$inject = ['$scope', '$rootScope', '$location', '$http', 'loginService','$window', '$location', '$cookies', '$rootScope'];

  function loginController($scope, $rootScope, $location, $http, loginService,$window,$location, $cookies, $rootScope) {
  	var vm = this;
  	vm.login = login;
    vm.forgot = forgot;
   

  	function login(user){
  		loginService.login(user).then(function(data){
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 1);        
       $cookies.putObject('customerInfoCookies', data.data.token, {'expires': expireDate});
       //$cookies.putObject('customerInfoCookies', data.data.token);
        $rootScope.$emit("CallParentMethod", {});
        // $state.go("/");
        $window.open('http://localhost:3000/index', '_self');
  		})
  	}

      function forgot(user){
           console.log("vm.invite",vm.invite.email);
        loginService.forgot(vm.invite.email).then(function(data){
          var expireDate = new Date();
          expireDate.setDate(expireDate.getDate() + 1);        
         $cookies.putObject('customerInfoCookies', data.data.token, {'expires': expireDate});
         //$cookies.putObject('customerInfoCookies', data.data.token);
          $rootScope.$emit("CallParentMethod", {});
          // $state.go("/");
        })
      }
    }
