angular.module('app').controller('rolesController', rolesController);

  rolesController.$inject = ['$scope', '$rootScope', '$location', '$http', 'menuService', 'roleService'];

  function rolesController($scope, $rootScope, $location, $http, menuService, roleService) {
  	var vm = this;
  	vm.getAllOption = getAllOption;
  	vm.addRoleApp = addRoleApp;
    vm.addRole = addRole;
    vm.getAllRoles = getAllRoles;
    vm.selectOption = selectOption;
  	vm.mainMenus = [];
  	vm.subMenus = [];
    vm.Allroles = [];
    vm.allMenus = [];
    vm.allOption = true;
    vm.roleSave = false;
    vm.roleAppSave = true;

  	
  	getAllOption();
    getAllRoles();
    function selectOption(record){
       angular.forEach(vm.Allroles , function(value , key){              
            if(value._id == record){
              vm.roleRecord = value;
               roleService.getAllRolesApp(value._id).then(function(record){   
                  if(record.data[0]) {           
                      vm.selectedRoles = record.data[0].appId.split(',');
                      console.log(vm.selectedRoles);
                      angular.forEach(vm.mainMenus, function(value1 , key){
                          value1.selected = false;
                          if(vm.selectedRoles.indexOf(value1.appId.toString()) != -1 ){
                              value1.selected = true;
                          }
                      });
                      angular.forEach(vm.subMenus, function(value1 , key){
                          value1.selected = false;
                          if(vm.selectedRoles.indexOf(value1.appGroup.toString()) != -1 ){
                              value1.selected = true;
                          }
                      });
                    }else{
                        angular.forEach(vm.allMenus, function(value1 , key){
                          value1.selected = false;
                      });
                    }
               });
            }
          });
    }


    function getAllRoles(){
      roleService.getAllRoles().then(function(response){
        vm.Allroles = response.data; 
        vm.selectRole = response.data[0]._id;
        selectOption(vm.selectRole);
      });
    }

  	function getAllOption(){
  		menuService.getAllMenu().then(function(response){
	          angular.forEach(response.data , function(value , key){
              vm.allMenus = response.data; 
	          	value.selected = false;
	          	if(value.appGroup == 0)	vm.mainMenus.push(value);
	          	else vm.subMenus.push(value);
	          });
	      	});
  	}

    

  	vm.toggleAll = function(items) {
	    angular.forEach(vm.subMenus, 
	    	function(itm){ 
	    		if(items.appId == itm.appGroup) itm.selected = items.selected; 
	    	});
    }

    function addRole(addRole){
      console.log("role.roleName ",vm.role);
      if(addRole){
        addRole.isActive ="A";
        addRole.createdBy ="chethan";
        addRole.modifiedBy ="prasad";

        roleService.addRole(addRole).then(function(data){
          vm.role = "";
          vm.roleAppSave = true;
          vm.roleSave = false;
          vm.allOption = true;
          vm.roleSaveButn = false
          vm.Allroles = [];
           console.log("addRole ",addRole);
          getAllRoles()
        })
      }else{
        addRoleApp();
      }
    }
  	
  	function addRoleApp(){
  		var allappid = [];

  		angular.forEach(vm.mainMenus, 
	    	function(record){ 
	    		if(record.selected){ 
	    			allappid.push(record.appId);

	    		}
	    	});
  		angular.forEach(vm.subMenus, 
	    	function(record){ 
	    		if(record.selected){
	    			console.log(record.appGroup);
	    			if (allappid.indexOf(record.appGroup) == -1) {
				         allappid.push(record.appId);
				     }
	    		} 
	    	});
      console.log("vm.selectRole  ",vm.selectRole);
  		var roleapp = {	roleId: vm.selectRole, isActive: 'Y', createdBy: 'Chethan', modifiedBy: 'chethan',
  			appId: allappid
  		}
  		roleService.addRoleApp(roleapp).then(function(){
        getAllRoles();
  		})
  	}


  }