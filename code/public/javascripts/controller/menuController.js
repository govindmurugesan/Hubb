angular.module('app').controller('menuController', menuController);

  menuController.$inject = ['$scope', '$rootScope', '$location','menuService'];

  function menuController($scope, $rootScope, $location, menuService) {
  	    var vm = this;
	  	vm.getAllMenu = getAllMenu;
	  	vm.mainMenus = [];
	  	vm.subMenus = [];
	  	vm.showMenu = true;

	  	getAllMenu();
	  	function getAllMenu(){
	  		menuService.getAllMenu().then(function(response){
	        
	          angular.forEach(response.data , function(value , key){
	          	if(value.appGroup == 0)	vm.mainMenus.push(value);
	          	else vm.subMenus.push(value);
	          });
	      	});
	  	}

	  	$rootScope.$on("CallParentMethod", function(){
	  		vm.showMenu = !vm.showMenu;
	  		
        });

       
        	


  }