var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userMenu = new Schema({
	appId: { type: Number, default: 1},
	appName: { type: String },
	appDesc: { type: String },
	appGroup: { type: Number },
	isActive: { type: String },
	createdBy: { type: String },
	createdOn: { type: Date, default: Date.now },
	modifiedBy: { type: String },
	modifiedOn: { type: Date, default: Date.now }

},{strict: false});

module.exports =  mongoose.model('App', userMenu)