var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userRoles = new Schema({
	roleId: { type: Number ,  default: 100101},
	roleName: { type: String },
	roleDesc: { type: String },
	isActive: { type: String },
	createdBy: { type: String },
	createdOn: { type: Date, default: Date.now },
	modifiedBy: { type: String },
	modifiedOn: { type: Date, default: Date.now }

},{strict: false});

module.exports =  mongoose.model('Roles', userRoles)