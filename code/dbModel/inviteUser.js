var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var newUser = new Schema({
	email: { type: String },
	role: { type: Schema.Types.ObjectId, ref: 'Roles' },
	password: { type: String },
	token: {type: String},
	updatedon: { type: Date, default: Date.now }

},{strict: false});

module.exports =  mongoose.model('inviteUser', newUser)