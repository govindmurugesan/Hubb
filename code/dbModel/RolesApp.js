var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var rolesApp = new Schema({
	roleId: { type: Schema.Types.ObjectId ,ref: 'Roles'},
	appId: { type: String },
	isActive: { type: String },
	createdBy: { type: String },
	createdOn: { type: Date, default: Date.now },
	modifiedBy: { type: String },
	modifiedOn: { type: Date, default: Date.now }

},{strict: false});

module.exports =  mongoose.model('RolesApp', rolesApp)