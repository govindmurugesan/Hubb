var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var newUser = new Schema({
	userId: { type: Number,seq: 0 },
	roleId: { type: Schema.Types.ObjectId, ref:"Roles" },
 	email: { type: String },
 	password: { type: String },
 	token: {type: String},
 	mobile: { type: String },
 	pass1: { type: String },
 	pass2: { type: String },
 	pass3: { type: String },
 	isLocked: { type: String},
 	attempts: { type: Number , default: 0},
 	isActive: { type: String },
 	createdBy: { type: String },
 	createdOn: { type: Date, default: Date.now },
 	modifiedBy: { type: String },
 	modifiedOn: { type: Date, default: Date.now }
},{strict: false});
	


module.exports =  mongoose.model('Users', newUser)