var express = require('express'),
	router = express.Router(),
	menuController = require('./controller'),
	tokenVerifier = require('../../utils/tokenVerifier');

router
	.route('/getMenus')
	.get( menuController.getAllMenus);



module.exports = router;