var menuDBModel = require('../../dbModel/menuItems'),
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	tokenVerifier = require('../../utils/tokenVerifier');

module.exports.getAllMenus = getAllMenus;

function getAllMenus( req, res, next){
	menuDBModel.find().exec(function(err, detail){
		if(err){ return res.status(httpStatusCode.DBERROR).send(err);  }
		if(detail != null){
			res.json(detail);
		}	
	});
}