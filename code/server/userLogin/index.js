var express = require('express'),
	router = express.Router(),
	passport = require('passport'),
	loginController = require('./controller');


router.route('/login')
	  .post(loginController.login);

router.route('/forgot')
	  .post(loginController.forgot);
router.route('/getUser')
	  .get(loginController.getUser);

router.route('/activateuser')
	  .get(loginController.activateuser);

router.route('/addUser')
	  .post(loginController.addUser);
router.route('/resetUser')
	  .post(loginController.resetUser);
/*router.route('/forgot')
	  .post(loginController.forgot);*/
	  

router.get('/' ,function(req, res, next) {
  res.send('respond with login');
});
/*router.get('/', passport.authenticate('local') , function(req,res){
    res.json(req);
});*/

module.exports = router;