var loginUserDBModel = require('../../dbModel/loginUsers'),
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	passwordGenerator = require('../../utils/passwordGenerator'),
	resetRedis = require('../../utils/resetRedis.js'),
	jwt         = require('../../node_modules/jsonwebtoken'),
	appConstant = require('../../utils/appConstants.js'),
	inviteDBModel = require('../../dbModel/inviteUser');
	mailSender = require('../../utils/mailSender');
	


module.exports.login = login;
module.exports.getUser = getUser;
module.exports.activateuser = activateuser;
module.exports.passwordReset = passwordReset;
module.exports.addUser = addUser;
module.exports.forgot= forgot;
module.exports.resetUser= resetUser;

function login(req,res,next){
	 	loginUserDBModel.findOne({'email': req.body.email,'password': req.body.password})
	.exec(function(err, detail){
		console.log("544544465464");
	
		if (err) { return res.status(httpStatusCode.DBERROR).send(err); }

		if(detail != null){
			resetRedis(detail, function(token, date){
	    		var tokenInfo = JSON.stringify({ userid: detail._id, token: token, created: date});
		    		/*res.redirect(
	  					appConstant.appUrl+'/#/loginSuccess/'+tokenInfo
					);*/
					res.json({
		        		email: detail.email,
		        		userId : detail.userId,
		        		roleId : detail.roleId,
		         		mobile : detail.mobile,
		        		isLocked : detail.isLocked,
		        		token : tokenInfo
     				});
	    	}, function(err){
	      		res.redirect(appConstant.appUrl);
	    	});
		}
	});
}
 	
function getUser(req,res,next){
	loginUserDBModel.find({},{ password:0, pass1:0, pass2:0, pass3:0 })
	.populate('roleId')
	.exec(function(err, detail){
		if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
		if(detail != null){
			res.json(detail);
	    }
	})	
}
function activateuser(req,res,next){
		/*loginUserDBModel.find({},{ password:0, pass1:0, pass2:0, pass3:0 })
	.populate('roleId')
	.exec(function(err, detail){
		if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
		if(detail != null){
			res.json(detail);
	    }
	})*/
}
function passwordReset(req,res,next){
		/*loginUserDBModel.find({},{ password:0, pass1:0, pass2:0, pass3:0 })
	.populate('roleId')
	.exec(function(err, detail){
		if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
		if(detail != null){
			res.json(detail);
	    }
	})*/
}
function addUser(req,res,next){
	console.log("req.body._id  ", req.body);
	inviteDBModel.findOne({"_id": req.body.InviteId})
	.exec(function(err, detail){
		if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
		console.log("============ ")
		if(detail != null){
			loginUser = new loginUserDBModel({
				email: detail.email,
				roleId: detail.role,
				password: req.body.password,
				isLocked: "N",
				isActive: "A"
			})
			
			loginUser.save(function(err, detail){
				if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
				console.log("-----detail--------------",detail);
				loginUserDBModel.findOneAndUpdate({ _id: detail._id }, { $inc: { userId: 1 } }
            	,function(err, detail1){
					if (err) { return res.status(httpStatusCode.DBERROR).send(err); }
					if(detail1 != null){
						console.log("-----detail1--------------",detail1);
						res.json(detail1);
					}
				});
			});
	    }
	})
}

function resetUser(req,res,next){
	 	loginUserDBModel.findOne({'email': req.body.email})
	.exec(function(err, detail){
		console.log("reset");
	
		if (err) { return res.status(httpStatusCode.DBERROR).send(err); }

		if(detail != null){
			resetRedis(detail, function(token){
				console.log('token',token);
	    		var tokenInfo = JSON.stringify({ token: token});
		    		res.json({
		        		email: detail.email,
		        		password: req.body.password,
		        		token : tokenInfo
     				});
	    	}, function(err){
	      		res.redirect(appConstant.appUrl);
	    	});
		}
	});
}

function forgot(req,res,next){
	console.log("req.body.email ", req.body.email);
	loginUserDBModel.findOne({"email": req.body.email})
	.exec(function(err, detail){
		if(err){ return  res.status(httpStatusCode.DBERROR).send(err); }
		console.log("============ ")
		if(detail != null){
			console.log("ths s detail",detail.email);
			var token = jwt.sign({detail: req.body.email}, "thssatpredissecret");
			console.log("ths is detail",detail.email);
			console.log("token  ", token);
			var date  = new Date();
			var expire = new Date();
			expire.setDate( date.getDate() + 1 );
			var forgotToken  = '_token:' + token;
			console.log("token  ",token);
			Redis.hmset(forgotToken, {storedToken:token }, function(err, rply){
		      	if(err){
		        	console.log("err cal back",err);
		        	failCb(err);
		      	} else {
		      		console.log("sasasassa");
		        	next(token, date);
		      	}
		  	});
			if(req.body.email == detail.email){
				console.log("inside if")
				mailOptions ={
					type : "forgotpassword",
					token: detail.token,
					email: detail.email,
					password: "sdds"
				}
				mailSender.sendMail(mailOptions, function(error, resp){
					console.log("response", resp);
					if(resp == 'sent'){
						res.json({
							token: detail.token,
			        		email: detail.email,
			        		registermsg : 'mailsent'
	     				});
					}else{
						res.json({
			        		msg: 'Something went wrong please try again later'
	     				});
					}
				});
			}
				
		}
	});
}