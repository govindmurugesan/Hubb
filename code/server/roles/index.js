var express = require('express'),
	router = express.Router(),
	roleAppController = require('./controller'),
	tokenVerifier = require('../../utils/tokenVerifier');

router
	.route('/getRoles')
	.get( roleAppController.getRoles);
router
	.route('/addroleapp')
	.post( roleAppController.addRoleApp);
router
	.route('/addrole')
	.post( roleAppController.addRole);
router
	.route('/getRoleApp')
	.get( roleAppController.getRoleApp);

router.get('/', function(req, res, next) {
  res.send('respond with a roles');
});


module.exports = router;