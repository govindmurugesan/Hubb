var roleAppDBModel = require('../../dbModel/RolesApp'),
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	tokenVerifier = require('../../utils/tokenVerifier'),
	roleDBModel = require('../../dbModel/userRoles');

module.exports.addRole = addRole;
module.exports.addRoleApp = addRoleApp;
module.exports.getRoles =getRoles;
module.exports.getRoleApp = getRoleApp;

function getRoles(req, res, next){
	roleDBModel.find().exec(function(err, detail){
		if(err){ return res.status(httpStatusCode.DBERROR).send(err);  }
		if(detail != null){
			res.json(detail);
		}	
	});
}
function getRoleApp(req, res, next){
	roleAppDBModel.find({"roleId":req.query.roleId}).exec(function(err, detail){
		if(err){ return res.status(httpStatusCode.DBERROR).send(err);  }
		if(detail != null){
			res.json(detail);
		}	
	});
}

function addRole( req, res, next){
	console.log("req.body  ",req.body);
	roleApp = new roleDBModel({
		
		roleName: req.body.roleName,
		roleDesc: req.body.RoleDesc,
		isActive: req.body.isActive,
		createdBy: req.body.createdBy,
		modifiedBy: req.body.modifiedBy
	})
	console.log("roleApp   ",roleApp);
	roleApp.save(function(err, detail){
		if(err){ return res.status(httpStatusCode.DBERROR).send(err);  }
		if(detail != null){
			
			roleDBModel.findOneAndUpdate({ _id: detail._id }, { $inc: { roleId: 1 } }
            	,function(err, detail1){
					if (err) { return res.status(httpStatusCode.DBERROR).send(err); }
					if(detail1 != null){
						res.json(detail1);
					}
				});
			
		}	
	});
}

function addRoleApp( req, res, next){
	console.log("req.body  ",req.body);
	console.log("req ", req);
	roleApp = new roleAppDBModel({
		roleId: req.body.roleId,
		appId: req.body.appId,
		isActive: req.body.isActive,
		createdBy: req.body.createdBy,
		modifiedBy: req.body.modifiedBy
	})
	console.log("roleApp   ",roleApp);
	roleApp.save(function(err, detail){
		if(err){ return res.status(httpStatusCode.DBERROR).send(err);  }
		if(detail != null){
			console.log("all menu  ",detail);
			res.json(detail);
		}	
	});
}