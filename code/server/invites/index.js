var express = require('express'),
	router = express.Router(),
	userController = require('./controller'),
	tokenVerifier = require('../../utils/tokenVerifier');

router
	.route('/services')
	.post( userController.inviteNewUser);

router.get('/',function(req, res, next) {
  res.send('respond with a chethananas');
});

module.exports = router;