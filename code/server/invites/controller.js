var inviteUserDBModel = require('../../dbModel/inviteUser'),
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	passwordGenerator = require('../../utils/passwordGenerator'),
	mailSender = require('../../utils/mailSender');
	tokenVerifier = require('../../utils/tokenVerifier');

var  redis = require('../../utils/redis');
var redisClientObj  = redis.redisClient;

module.exports.inviteNewUser = inviteNewUser;


function inviteNewUser( req, res, next){
     
	passwordGenerator.pass(function(err,tempPassword){
		if(err){ console.log('Error during password generation', err)}
		userdetail = new inviteUserDBModel({email: req.body.email,
							   role: req.body.role,
							   password: tempPassword});
	 	userdetail.save(function(err, detail){
			if (err) { return res.status(httpStatusCode.DBERROR).send(err); }

			if(detail != null){
				console.log(detail);
			    var mailOptions = {
			    	_id:detail._id,
					email: detail.email,
					role: req.body.role,
					password: tempPassword,
					type : 'invited'					
				}
				mailSender.sendMail(mailOptions, function(error, resp){
					if(resp == 'sent'){
						res.json({
			        		email: detail.email,
			        		registermsg : 'mailsent'
	     				});
					}else{
						res.json({
			        		msg: 'Something went wrong please try again later'
	     				});
					}
				});
			}
		});
	});
}


