angular
    .module('ePortal')
    .controller('viewVendorController', viewVendorController);

viewVendorController.$inject = ['$scope', '$location', 'vendorService', 'filterFilter', 'companyInformationService', 'configrationService', '$filter', '$rootScope', 'constantsService'];

function viewVendorController($scope, $location, vendorService, filterFilter, companyInformationService, configrationService, $filter, $rootScope, constantsService) {
    var viewVendorControllerScope = this;

    viewVendorControllerScope.showVendorList = true;
    viewVendorControllerScope.vendorSearchKeyword = "";
    viewVendorControllerScope.title = "Vendors";
    getVendorList();

    viewVendorControllerScope.showVendorDetail = showVendorDetail;
    viewVendorControllerScope.showEdit = showEdit;
    viewVendorControllerScope.decline = decline;
    viewVendorControllerScope.updateDetails = updateDetails;
    viewVendorControllerScope.vendorListView = vendorListView;
    viewVendorControllerScope.hideAlertPopup = hideAlertPopup;
    viewVendorControllerScope.gotoCreateVendor = gotoCreateVendor;
    viewVendorControllerScope.selectedVendorType = selectedVendorType;
    viewVendorControllerScope.selectedCountry = selectedCountry;
    viewVendorControllerScope.selectedState = selectedState;
    viewVendorControllerScope.selectedStatus = selectedStatus;
    viewVendorControllerScope.selectedCountryCodeType = selectedCountryCodeType;

    $rootScope.currentMenu = "Vendor";

    viewVendorControllerScope.statusUpdate = [{
        status: constantsService.activeState

    }, {
        status: constantsService.inactiveState

    }]

    var vendorDetailInfoBackup;

    function selectedVendorType(vendor) {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.selectVendorType = vendor;
        viewVendorControllerScope.spinner = false;
    }

    function selectedCountry(country) {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.selectCountry = country.name;
        viewVendorControllerScope.stateList = country.state;
        viewVendorControllerScope.selectState = { name: "Select State" };
        viewVendorControllerScope.spinner = false;
    }

    function selectedState(state) {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.selectState = { name: state };
        viewVendorControllerScope.spinner = false;
    }

    function selectedStatus(status) {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.selectStatus = status;
        viewVendorControllerScope.spinner = false;
    }

    function selectedCountryCodeType(countryCode) {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.selectCountryCodeType = { countryCode: countryCode.callingCode + ' (' + countryCode.countryCode + ')' };
        viewVendorControllerScope.spinner = false;
    }

    function getVendorList() {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.infoMsg = "";
        vendorService.getVendorList().then(function(response) {
            if (response.data.msg) {
                viewVendorControllerScope.spinner = false;
                viewVendorControllerScope.infoMsg = response.data.msg;
                viewVendorControllerScope.vendorList = '';
            } else {
                if (response.data) {
                    viewVendorControllerScope.vendorList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = viewVendorControllerScope.vendorList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(viewVendorControllerScope.vendorList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    viewVendorControllerScope.spinner = false;

                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function showVendorDetail(vendorDetails) {
        viewVendorControllerScope.vendorInfo = "";
        vendorDetailInfoBackup = "";

        viewVendorControllerScope.showEditButton = true;
        viewVendorControllerScope.nonEditableField = true;
        viewVendorControllerScope.edititemtrue = true;
        viewVendorControllerScope.edititem = false;
        viewVendorControllerScope.showVendorList = false;
        viewVendorControllerScope.vendorSearchKeyword = "";
        viewVendorControllerScope.title = "Vendor Detail"
        vendorDetailInfoBackup = angular.copy(vendorDetails);
        viewVendorControllerScope.vendorInfo = angular.copy(vendorDetails);
        viewVendorControllerScope.selectVendorType = { name: vendorDetails.venderType };
        viewVendorControllerScope.selectCountry = vendorDetails.country;
        viewVendorControllerScope.selectState = { name: vendorDetails.state };
        viewVendorControllerScope.selectStatus = vendorDetails.status;
        viewVendorControllerScope.selectCountryCodeType = { countryCode: vendorDetails.countryCode };

        // viewVendorControllerScope.selectCountryCodeType = {countryCode:countryCode.callingCode+' '+countryCode.countryCode};
        /*if(viewVendorControllerScope.vendorInfo.status == 'a') viewVendorControllerScope.vendorInfo.status = viewVendorControllerScope.statusUpdate[0];
        else viewVendorControllerScope.vendorInfo.status = viewVendorControllerScope.statusUpdate[1];*/
        viewVendorControllerScope.fieldEditable = true;
    }

    function showEdit() {
        //viewVendorControllerScope.vendorInfo.countryDetail = viewVendorControllerScope.vendorInfo.country[0];
        getCountryList();
        getVendorType();
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.nonEditableField = false;
        viewVendorControllerScope.edititemtrue = true;
        viewVendorControllerScope.edititem = true;
        viewVendorControllerScope.showEditButton = false;
        viewVendorControllerScope.fieldEditable = false;
        viewVendorControllerScope.spinner = false;
    }

    function decline() {

        vendorListView();


    }

    function getCountryList() {
        viewVendorControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {

            } else {
                if (response.data) {
                    viewVendorControllerScope.countryList = response.data;
                    viewVendorControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getVendorType() {
        viewVendorControllerScope.spinner = true;
        configrationService.getVendorType().then(function(response) {
            if (response.data.msg) {


            } else {
                if (response.data.length > 0) {
                    var c = { status: 'a' };
                    var ss = $filter('filter')(response.data, { status: "a" });
                    viewVendorControllerScope.vendorTypeList = response.data;
                    viewVendorControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function updateDetails() {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.vendorInfo.venderType = viewVendorControllerScope.selectVendorType.name;
        viewVendorControllerScope.vendorInfo.country = viewVendorControllerScope.selectCountry;
        viewVendorControllerScope.vendorInfo.state = viewVendorControllerScope.selectState.name;
        viewVendorControllerScope.vendorInfo.status = viewVendorControllerScope.selectStatus;
        viewVendorControllerScope.vendorInfo.countryCode = viewVendorControllerScope.selectCountryCodeType.countryCode;
        vendorService.updateVendorDetail(viewVendorControllerScope.vendorInfo).then(function(response) {
            if (response.data.msg) {
                viewVendorControllerScope.spinner = false;
                viewVendorControllerScope.infoMsg = response.data.msg;
                viewVendorControllerScope.vendorList = '';
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    viewVendorControllerScope.alertMsg = "Vendor Updated Successfully"
                    viewVendorControllerScope.showVendorList = true;
                    viewVendorControllerScope.vendorSearchKeyword = "";
                    viewVendorControllerScope.title = "Vendors";
                    getVendorList();
                    viewVendorControllerScope.spinner = false;
                }
            }
        }, function(err) {
            console.log("errormsg", err);
        });
    }

    function vendorListView() {
        viewVendorControllerScope.spinner = true;
        viewVendorControllerScope.showVendorList = true;
        viewVendorControllerScope.vendorSearchKeyword = "";
        viewVendorControllerScope.title = "Vendors";
        getVendorList();
        viewVendorControllerScope.spinner = false;
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function gotoCreateVendor() {
        $location.url('/createVendor')
    }

}
