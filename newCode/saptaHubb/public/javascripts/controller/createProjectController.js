angular
    .module('ePortal')
    .controller('createProjectController', createProjectController);

createProjectController.$inject = ['$scope', '$location', 'projectService', 'customerService', 'filterFilter', 'configrationService', '$rootScope'];

function createProjectController($scope, $location, projectService, customerService, filterFilter, configrationService, $rootScope) {
    var createProjectControllerScope = this;

    createProjectControllerScope.spinner = false;
    createProjectControllerScope.getCustomerList = getCustomerList;
    createProjectControllerScope.openStartDateCalender = openStartDateCalender;
    createProjectControllerScope.openEndDateCalender = openEndDateCalender;
    createProjectControllerScope.addProject = addProject;
    createProjectControllerScope.decline = decline;
    createProjectControllerScope.hideAlertPopup = hideAlertPopup;
    createProjectControllerScope.selectedCustomerFunction = selectedCustomerFunction;

    $rootScope.currentMenu = "Create Project";

    createProjectControllerScope.selectedCustomer = { name: "Select Customer" };

    $scope.initDate = new Date();
    $scope.formats = ['dd-MMM-yyyy','dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    function selectedCustomerFunction(customer) {
        createProjectControllerScope.selectedCustomer = customer;
    }

    function addProject() {
        createProjectControllerScope.spinner = true;
        createProjectControllerScope.project.customer = createProjectControllerScope.selectedCustomer;
        projectService.addProject(createProjectControllerScope.project).then(function(response) {
            if (response.data.msg) {
                createProjectControllerScope.infoMsg = response.data.msg;
                createProjectControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    createProjectControllerScope.alertMsg = "Project added successfully";
                    createProjectControllerScope.project = "";
                    createProjectControllerScope.selectedCustomer = { name: "Select Customer" };
                    createProjectControllerScope.createProjectForm.$setPristine();
                    createProjectControllerScope.createProjectForm.$setUntouched();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });

        createProjectControllerScope.spinner = false;

    }

    function getCustomerList() {
        createProjectControllerScope.spinner = true;

        customerService.getCustomerList().then(function(response) {
            if (response.data.msg) {
                createProjectControllerScope.infoMsg = response.data.msg;
                createProjectControllerScope.customerList = '';
            } else {
                if (response.data) {
                    createProjectControllerScope.customerList = response.data;
                }
            }

        });

        createProjectControllerScope.spinner = false;
    }

    function openStartDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createProjectControllerScope.startDateCalender = !createProjectControllerScope.startDateCalender;
    }

    function openEndDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createProjectControllerScope.endDateCalender = !createProjectControllerScope.endDateCalender;
    };

    function decline() {
        createProjectControllerScope.spinner = true;
        createProjectControllerScope.project = "";
        createProjectControllerScope.createProjectForm.$setPristine();
        createProjectControllerScope.createProjectForm.$setUntouched();
        createProjectControllerScope.spinner = false;
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewProject');
    }

    getCustomerList();

}
