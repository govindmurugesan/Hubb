angular
    .module('ePortal')
    .controller('configurationController', configurationController);

configurationController.$inject = ['$scope', '$location', 'configrationService', 'filterFilter', 'constantsService', '$routeParams', '$rootScope'];

function configurationController($scope, $location, configrationService, filterFilter, constantsService, $routeParams, $rootScope) {
    var configurationControllerScope = this;

    configurationControllerScope.divValue = 'country';
    configurationControllerScope.spinner = false;
    $scope.modalShown = false;
    configurationControllerScope.activeState = constantsService.activeState;
    configurationControllerScope.inactiveState = constantsService.inactiveState;
    $rootScope.currentMenu = "Configuration";

    configurationControllerScope.addEmplyeeType = addEmplyeeType;
    configurationControllerScope.addCustomerType = addCustomerType;
    configurationControllerScope.addDepartment = addDepartment;
    configurationControllerScope.addTimeOffType = addTimeOffType;
    configurationControllerScope.addInvoiceType = addInvoiceType;
    configurationControllerScope.addPaymentTerm = addPaymentTerm;
    configurationControllerScope.addInvoicePaymentTerm = addInvoicePaymentTerm;
    configurationControllerScope.addEmployeeRole = addEmployeeRole;
    configurationControllerScope.addVendorType = addVendorType;
    configurationControllerScope.changeDiv = changeDiv;
    configurationControllerScope.updateEmployeeStatus = updateEmployeeStatus;
    configurationControllerScope.updateCustomerStatus = updateCustomerStatus;
    configurationControllerScope.updateTimeOffTypeStatus = updateTimeOffTypeStatus;
    configurationControllerScope.updateInvoiceStatus = updateInvoiceStatus;
    configurationControllerScope.updatePaymentTermStatus = updatePaymentTermStatus;
    configurationControllerScope.updateInvoicePaymentTermStatus = updateInvoicePaymentTermStatus;
    configurationControllerScope.updateEmployeeRoleStatus = updateEmployeeRoleStatus;
    configurationControllerScope.updateVendorStatus = updateVendorStatus;
    configurationControllerScope.updateDepartmentStatus = updateDepartmentStatus;
    configurationControllerScope.deleteEmployeeType = deleteEmployeeType;
    configurationControllerScope.deleteCustomerType = deleteCustomerType;
    configurationControllerScope.deleteTimeOffType = deleteTimeOffType;
    configurationControllerScope.deleteInvoiceType = deleteInvoiceType;
    configurationControllerScope.deletePaymentTerm = deletePaymentTerm;
    configurationControllerScope.deleteInvoicePaymentTerm = deleteInvoicePaymentTerm;
    configurationControllerScope.deleteEmployeeRoleType = deleteEmployeeRoleType;
    configurationControllerScope.deleteDepartment = deleteDepartment;
    configurationControllerScope.hideAlertPopup = hideAlertPopup;
    configurationControllerScope.closeModel = closeModel;
    configurationControllerScope.addNewCountry = addNewCountry;
    configurationControllerScope.addNewState = addNewState;
    configurationControllerScope.deleteCountry = deleteCountry;
    configurationControllerScope.deleteState = deleteState;
    configurationControllerScope.deleteVendorType = deleteVendorType;
    configurationControllerScope.deleteWorkLocation = deleteWorkLocation;
    configurationControllerScope.addWorkLocation = addWorkLocation;
    configurationControllerScope.updateCountryStatus = updateCountryStatus;
    configurationControllerScope.updateStateStatus = updateStateStatus;
    configurationControllerScope.updateWorkLocationStatus = updateWorkLocationStatus;
    configurationControllerScope.getCountryList = getCountryList;
    configurationControllerScope.getStateDetals = getStateDetals;
    configurationControllerScope.getWorkLocationDetails = getWorkLocationDetails;
    configurationControllerScope.declineMethod = declineMethod;
    configurationControllerScope.editCountryDetailForm = editCountryDetailForm;
    configurationControllerScope.editWorkLocationForm = editWorkLocationForm;
    configurationControllerScope.editDepartmentForm = editDepartmentForm;
    configurationControllerScope.editEmployeeTypeForm = editEmployeeTypeForm;
    configurationControllerScope.editCustomerTypeForm = editCustomerTypeForm;
    configurationControllerScope.editTimeOffTypeForm = editTimeOffTypeForm;
    configurationControllerScope.editInvoiceTypesForm = editInvoiceTypesForm;
    configurationControllerScope.editPaymentTermForm = editPaymentTermForm;
    configurationControllerScope.editInvoicePaymentTermForm = editInvoicePaymentTermForm;
    configurationControllerScope.editEmployeeRoleForm = editEmployeeRoleForm;
    configurationControllerScope.editVendorTypeForm = editVendorTypeForm;

    configurationControllerScope.editStateDetailForm = editStateDetailForm;
    configurationControllerScope.countryChangeEvent = countryChangeEvent;
    configurationControllerScope.openWorkLocationForm = openWorkLocationForm;
    configurationControllerScope.selectedCountryFun = selectedCountryFun;
    configurationControllerScope.selectedWorkloaction = selectedWorkloaction;
    configurationControllerScope.selectedState = selectedState;

    configurationControllerScope.showUl = false;
    configurationControllerScope.showStateUl = false;

    function selectedCountryFun(countryName, countryId) {
        configurationControllerScope.showUl = !configurationControllerScope.showUl;
        configurationControllerScope.stateInfo = { _id: countryId, name: countryName };
    }

    function selectedWorkloaction(countryName, state) {
        configurationControllerScope.showUl = !configurationControllerScope.showUl;
        configurationControllerScope.workLocation = { country: countryName, state: configurationControllerScope.workLocation.state };
        configurationControllerScope.stateDetailsList = state;
    }

    function selectedState(stateName) {
        configurationControllerScope.showStateUl = !configurationControllerScope.showStateUl;
        configurationControllerScope.workLocation = { country: configurationControllerScope.workLocation.country, state: stateName };
    }

    configurationControllerScope.workLocation = { state: "Select State" };
    configurationControllerScope.stateInfo = { name: "Select Country" };

    changeDiv($routeParams.getText);

    function changeDiv(selectedDiv) {
        getCountryDetals();
        if (selectedDiv == 'work_Location') {
            configurationControllerScope.divValue = 'work_Location';
            getWorkLocationDetails();
        } else if (selectedDiv == 'department') {
            configurationControllerScope.divValue = 'department';
            getDepartment();
        } else if (selectedDiv == 'employee_Types') {

            configurationControllerScope.divValue = 'employee_Types';
            getEmployeeType();

        } else if (selectedDiv == 'customer_Types') {

            configurationControllerScope.divValue = 'customer_Types';
            getCustomerType();

        } else if (selectedDiv == 'vendor_Types') {

            configurationControllerScope.divValue = 'vendor_Types';
            getVendorType();

        } else if (selectedDiv == 'time_Off_Types') {
            configurationControllerScope.divValue = 'time_Off_Types';
            getTimeOffType();

        } else if (selectedDiv == 'invoicing_Types') {
            configurationControllerScope.divValue = 'invoicing_Types';
            getInvoiceType();

        } else if (selectedDiv == 'payment_Terms') {
            configurationControllerScope.divValue = 'payment_Terms';
            getPaymentTerm();

        } else if (selectedDiv == 'invoice_Payment_Terms') {
            configurationControllerScope.divValue = 'invoice_Payment_Terms';
            getInvoicePaymentTerm();

        } else if (selectedDiv == 'employee_Role') {
            configurationControllerScope.divValue = 'employee_Role';
            getEmployeeRole();

        } else if (selectedDiv == 'country') {
            configurationControllerScope.divValue = 'country';
            getCountryDetals();
        } else if (selectedDiv == 'state') {
            configurationControllerScope.divValue = 'state';
            getCountryDetals();
            getStateDetals();
        }


    }


    function addEmplyeeType() {
        $('#employeeTypes').modal('hide');
        configurationControllerScope.spinner = true;

        var employeetype = {
            name: configurationControllerScope.employeetype.name
        }

        configurationControllerScope.employeetype = "";
        configurationControllerScope.employeeForm.$setPristine();
        configurationControllerScope.employeeForm.$setUntouched();

        configrationService.addEmployeeType(employeetype).then(function(response) {
            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Employee added successfully"
                getEmployeeType();
            }
        }, function(error_response) {
            console.log("error response", error_response);
        });
    }

    function addVendorType() {
        $('#vendorTypes').modal('hide');
        configurationControllerScope.spinner = true;

        var vendortype = {
            name: configurationControllerScope.vendortype.name
        }

        configurationControllerScope.vendortype = "";
        configurationControllerScope.vendorForm.$setPristine();
        configurationControllerScope.vendorForm.$setUntouched();


        configrationService.addVendorType(vendortype).then(function(response) {
            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Vendor added successfully"
                getVendorType();

            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addCustomerType() {
        $('#customerTypes').modal('hide');
        configurationControllerScope.spinner = true;

        var customertype = {
            name: configurationControllerScope.customertype.name
        }

        configurationControllerScope.customertype = "";
        configurationControllerScope.customerForm.$setPristine();
        configurationControllerScope.customerForm.$setUntouched();


        configrationService.addCustomerType(customertype).then(function(response) {
            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Customer added successfully"
                getCustomerType();
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }


    function addTimeOffType() {
        $('#timeOffTypes').modal('hide');
        configurationControllerScope.spinner = true;
        var timeofftype = {
            name: configurationControllerScope.timeofftype.name
        }

        configurationControllerScope.timeofftype = "";
        configurationControllerScope.timeOffTypeForm.$setPristine();
        configurationControllerScope.timeOffTypeForm.$setUntouched();



        configrationService.addTimeOffType(timeofftype).then(function(response) {
            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Time Off Type added successfully"
                getTimeOffType();
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addInvoiceType() {
        $('#invoicingTypes').modal('hide');
        configurationControllerScope.spinner = true;
        var invoicetype = {
            name: configurationControllerScope.invoicetype.name
        }

        configurationControllerScope.invoicetype = "";
        configurationControllerScope.invoicingTypes.$setPristine();
        configurationControllerScope.invoicingTypes.$setUntouched();



        configrationService.addInvoiceType(invoicetype).then(function(response) {
            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Invoice added successfully"
                getInvoiceType();
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addPaymentTerm() {
        $('#paymentTerms').modal('hide');
        configurationControllerScope.spinner = true;
        configrationService.addPaymentTerm(configurationControllerScope.paymentTerm).then(function(response) {

            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Payment added successfully"
                getPaymentTerm();
                configurationControllerScope.paymentTerm = "";
                configurationControllerScope.paymentTerms.$setPristine();
                configurationControllerScope.paymentTerms.$setUntouched();
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addInvoicePaymentTerm() {
        $('#invoicePaymentTerms').modal('hide');
        configurationControllerScope.spinner = true;
        var invoicepaymentterm = {
            name: configurationControllerScope.invoicePaymentTerm.name
        }

        configurationControllerScope.invoicePaymentTerm = "";
        configurationControllerScope.invoicePaymentTerms.$setPristine();
        configurationControllerScope.invoicePaymentTerms.$setUntouched();

        configrationService.addInvoicePaymentTerm(invoicepaymentterm).then(function(response) {


            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Invoice Payment Terms added successfully"
                getInvoicePaymentTerm();
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addEmployeeRole() {
        $('#employeeRoles').modal('hide');
        configurationControllerScope.spinner = true;
        var employeerole = {
            name: configurationControllerScope.employeeRole.name
        }

        configurationControllerScope.employeeRole = "";
        configurationControllerScope.employeeRoleForm.$setPristine();
        configurationControllerScope.employeeRoleForm.$setUntouched();



        configrationService.addEmployeeRole(employeerole).then(function(response) {


            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Employee Role added successfully"
                getEmployeeRole();

            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addDepartment() {
        $('#department').modal('hide');
        configurationControllerScope.spinner = true;
        var Department = {
            name: configurationControllerScope.department.name
        }

        configurationControllerScope.department = "";
        configurationControllerScope.departmentForm.$setPristine();
        configurationControllerScope.departmentForm.$setUntouched();

        configrationService.addDepartment(Department).then(function(response) {


            if (response.data.successmsg = 'success') {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Department added successfully"
                getDepartment();

            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }



    function addNewCountry() {
        $('#country').modal('hide');
        configurationControllerScope.spinner = true;
        configrationService.addCountry(configurationControllerScope.country).then(function(response) {
            if (response.data.successmsg = 'success') {
                configurationControllerScope.country = "";
                configurationControllerScope.countryDetailForm.$setPristine();
                configurationControllerScope.countryDetailForm.$setUntouched();
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Country added successfully "
                getCountryDetals();
            }
        }, function(error_response) {
            console.log("error response", error_response);
        });
    }

    function addNewState() {
        $('#state').modal('hide');
        configurationControllerScope.spinner = true;
        configrationService.addState(configurationControllerScope.stateInfo).then(function(response) {
            if (response.data.successmsg = 'success') {
                configurationControllerScope.state = "";
                configurationControllerScope.stateDetailForm.$setPristine();
                configurationControllerScope.stateDetailForm.$setUntouched();
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "State added successfully "
                getStateDetals();
            }
        }, function(error_response) {
            console.log("error response", error_response);
        });
    }

    function addWorkLocation() {
        $('#workLocation').modal('hide');
        configurationControllerScope.spinner = true;
        configrationService.addWorkLocation(configurationControllerScope.workLocation).then(function(response) {
            if (response.data.successmsg = 'success') {
                configurationControllerScope.workLocation = "";
                configurationControllerScope.workLocationForm.$setPristine();
                configurationControllerScope.workLocationForm.$setUntouched();
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = "Work Location added successfully "
                getWorkLocationDetails();
            }
        }, function(error_response) {
            console.log("error response", error_response);
        });
    }

    function getEmployeeType() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;

        configrationService.getEmployeeType().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.employeeTypeList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.employeeTypeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.employeeTypeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.employeeTypeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;

                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getVendorType() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getVendorType().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.vendorsTypeList = '';

            } else {
                if (response.data.length > 0) {
                    configurationControllerScope.vendorsTypeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.vendorsTypeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.vendorsTypeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    configurationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getCustomerType() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getCustomerType().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.customerTypeList = '';

            } else {
                if (response.data.length > 0) {
                    configurationControllerScope.customerTypeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.customerTypeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.customerTypeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    configurationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getTimeOffType() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getTimeOffType().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.timeOffTypeList = '';
            } else {
                if (response.data.length > 0) {
                    configurationControllerScope.timeOffTypeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.timeOffTypeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.timeOffTypeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getInvoiceType() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getInvoiceType().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.invoiceTypeList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.invoiceTypeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.invoiceTypeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.invoiceTypeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getPaymentTerm() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getPaymentTerm().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.paymentTermList = '';
            } else {
                if (response.data.length > 0) {
                    configurationControllerScope.paymentTermList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.paymentTermList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.paymentTermList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getInvoicePaymentTerm() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getInvoicePaymentTerm().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.invoicePaymentTermList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.invoicePaymentTermList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.invoicePaymentTermList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.invoicePaymentTermList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getEmployeeRole() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getEmployeeRole().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.employeeRoleList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.employeeRoleList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.employeeRoleList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.employeeRoleList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getDepartment() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getDepartment().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.departmentList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.departmentList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.departmentList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.departmentList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }


    function getCountryList() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.countryList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.countryList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.countryList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.countryList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });

    }


    function getCountryDetals() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.countryDetailsList = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.countryDetailsList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.countryDetailsList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.countryDetailsList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getStateDetals() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.stateDetailsList = '';
            } else {

                if (response.data.length > 0) {
                    var stateCompleteDetail = [];
                    angular.forEach(response.data, function(countryDetails, index) {
                        if (countryDetails.status == configurationControllerScope.activeState) {
                            for (var j in countryDetails.state) {
                                var stateDetail = {
                                    name: countryDetails.name,
                                    statename: countryDetails.state[j].name,
                                    currencyType: countryDetails.currencyType,
                                    countryCode: countryDetails.countryCode,
                                    callingCode: countryDetails.callingCode,
                                    countryStatus: countryDetails.status,
                                    stateStatus: countryDetails.state[j].status,
                                    stateId: countryDetails.state[j]._id,
                                    countryId: countryDetails._id
                                }
                                stateCompleteDetail.push(stateDetail);
                            }
                        }
                    });

                    configurationControllerScope.stateDetailsList = stateCompleteDetail;
                   $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.stateDetailsList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.stateDetailsList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getWorkLocationDetails() {
        configurationControllerScope.infoMsg = "";
        configurationControllerScope.spinner = true;
        configrationService.getWorkLocationDetails().then(function(response) {
            if (response.data.msg) {
                configurationControllerScope.spinner = false;
                configurationControllerScope.infoMsg = response.data.msg;
                configurationControllerScope.worklocationListType = '';
            } else {

                if (response.data.length > 0) {
                    configurationControllerScope.worklocationListType = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = configurationControllerScope.worklocationListType.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(configurationControllerScope.worklocationListType, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    configurationControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function updateVendorStatus(vendorstatus, id, oppType) {

        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.vendortype.oppType = 'update'

            configrationService.updateVendorType(configurationControllerScope.vendortype).then(function(response) {

                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.vendortype = "";
                        configurationControllerScope.vendorForm.$setPristine();
                        configurationControllerScope.vendorForm.$setUntouched();
                        configurationControllerScope.vendorUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#vendorTypes').modal('hide');
                        getVendorType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {

            var newStatus = {
                status: vendorstatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateVendorType(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getVendorType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }

    function updateEmployeeStatus(empstatus, id) {
        var newStatus = {
            status: empstatus,
            id: id
        }

        configrationService.updateEmployeeType(newStatus).then(function(response) {
            if (response.data.msg) {

            } else {
                if (response.data.successMsg) {
                    getEmployeeType();
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });

    }

    function updateCustomerStatus(cusstatus, id, oppType) {

        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.customertype.oppType = 'update'

            configrationService.updateCustomerType(configurationControllerScope.customertype).then(function(response) {

                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.customertype = "";
                        configurationControllerScope.customerForm.$setPristine();
                        configurationControllerScope.customerForm.$setUntouched();
                        configurationControllerScope.customerUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#customerTypes').modal('hide');
                        getCustomerType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {

            var newStatus = {
                status: cusstatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateCustomerType(newStatus).then(function(response) {

                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getCustomerType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }


    function updateEmployeeRoleStatus(employeeRoleStatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.employeeRole.oppType = 'update'
            configrationService.updateEmployeeRole(configurationControllerScope.employeeRole).then(function(response) {

                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.employeeRole = "";
                        configurationControllerScope.employeeRoleForm.$setPristine();
                        configurationControllerScope.employeeRoleForm.$setUntouched();
                        configurationControllerScope.employeeRoleUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#employeeRoles').modal('hide');
                        getEmployeeRole();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: employeeRoleStatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateEmployeeRole(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getEmployeeRole();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }

    function updateInvoicePaymentTermStatus(invoicePaymenttermStatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.invoicePaymentTerm.oppType = 'update'
            configrationService.updateInvoicePaymentTerm(configurationControllerScope.invoicePaymentTerm).then(function(response) {

                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.invoicePaymentTerm = "";
                        configurationControllerScope.invoicePaymentTerms.$setPristine();
                        configurationControllerScope.invoicePaymentTerms.$setUntouched();
                        configurationControllerScope.invoicePaymentTermUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#invoicePaymentTerms').modal('hide');
                        getInvoicePaymentTerm();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: invoicePaymenttermStatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateInvoicePaymentTerm(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getInvoicePaymentTerm();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }

    function updatePaymentTermStatus(PaymenttermStatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.paymentTerm.oppType = 'update'
            configrationService.updatePaymentTerm(configurationControllerScope.paymentTerm).then(function(response) {
                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.paymentTerm = "";
                        configurationControllerScope.paymentTerms.$setPristine();
                        configurationControllerScope.paymentTerms.$setUntouched();
                        configurationControllerScope.paymentTermsUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#paymentTerms').modal('hide');
                        getPaymentTerm();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: PaymenttermStatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updatePaymentTerm(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getPaymentTerm();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }


    function updateInvoiceStatus(invoicetypestatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.invoicetype.oppType = 'update'
            configrationService.updateInvoiceType(configurationControllerScope.invoicetype).then(function(response) {
                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.invoicetype = "";
                        configurationControllerScope.invoicingTypes.$setPristine();
                        configurationControllerScope.invoicingTypes.$setUntouched();
                        configurationControllerScope.invoiceTypeUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#invoicingTypes').modal('hide');
                        getInvoiceType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: invoicetypestatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateInvoiceType(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getInvoiceType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }


    function updateTimeOffTypeStatus(timeofftypestatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.timeofftype.oppType = 'update'
            configrationService.updateTimeOffType(configurationControllerScope.timeofftype).then(function(response) {
                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.timeofftype = "";
                        configurationControllerScope.timeOffTypeForm.$setPristine();
                        configurationControllerScope.timeOffTypeForm.$setUntouched();
                        configurationControllerScope.timeOffTypeUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#timeOffTypes').modal('hide');
                        getTimeOffType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: timeofftypestatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateTimeOffType(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {

                    if (response.data.successMsg) {
                        getTimeOffType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }

    function updateDepartmentStatus(departmentStatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.department.oppType = 'update'
            configrationService.updateDepartment(configurationControllerScope.department).then(function(response) {

                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.department = "";
                        configurationControllerScope.departmentForm.$setPristine();
                        configurationControllerScope.departmentForm.$setUntouched();
                        configurationControllerScope.departmentUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#department').modal('hide');
                        getDepartment();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: departmentStatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateDepartment(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {

                    if (response.data.successMsg) {
                        getDepartment();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
    }


    function updateEmployeeStatus(empstatus, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOpp') {
            configurationControllerScope.employeetype.oppType = 'update'
            configrationService.updateEmployeeType(configurationControllerScope.employeetype).then(function(response) {
                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.employeetype = "";
                        configurationControllerScope.employeeForm.$setPristine();
                        configurationControllerScope.employeeForm.$setUntouched();
                        configurationControllerScope.employeeUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#employeeTypes').modal('hide');
                        getEmployeeType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: empstatus,
                id: id,
                oppType: 'statusUpdate'
            }

            configrationService.updateEmployeeType(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getEmployeeType();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });

        }
    }

    function updateCountryStatus(status, id, oppType) {
        configurationControllerScope.spinner = true;
        if (oppType == 'updateOp') {
            configurationControllerScope.country.oppType = 'update'
            configrationService.updateCountryStatus(configurationControllerScope.country).then(function(response) {
                if (response.data.msg) {
                    configurationControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.country = "";
                        configurationControllerScope.countryDetailForm.$setPristine();
                        configurationControllerScope.countryDetailForm.$setUntouched();
                        configurationControllerScope.countryUpdate = false;
                        configurationControllerScope.spinner = false;
                        $('#country').modal('hide');
                        getCountryDetals();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            })
        }
        if (oppType == 'statusUpdate') {
            var newStatus = {
                status: status,
                id: id,
                oppType: 'statusUpdate'
            }
            configrationService.updateCountryStatus(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getCountryDetals();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }

    }

    function updateStateStatus(status, id, stateId, oppType) {
        if (oppType == 'updateOp') {
            configurationControllerScope.stateInfo.oppType = "update";
            configrationService.updateStateStatus(configurationControllerScope.stateInfo).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.stateInfo = "";
                        configurationControllerScope.stateDetailForm.$setPristine();
                        configurationControllerScope.stateDetailForm.$setUntouched();
                        configurationControllerScope.stateUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#state').modal('hide');
                        getStateDetals();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });

        }

        if (oppType == 'statusEdit') {
            var newStatus = {
                status: status,
                id: id,
                stateId: stateId,
                oppType: 'statusEdit'
            }
            configrationService.updateStateStatus(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getStateDetals();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }

    }

    function updateWorkLocationStatus(status, id, updateOpp) {
        if (updateOpp == 'updateOpp') {
            configurationControllerScope.workLocation.oppType = "update";
            var updatedList;
            if (configurationControllerScope.workLocation.country.name) {
                updatedList = {
                    country: configurationControllerScope.workLocation.country.name,
                    oppType: 'update',
                    state: configurationControllerScope.workLocation.state.name,
                    _id: configurationControllerScope.workLocation._id,
                    worklocation: configurationControllerScope.workLocation.worklocation,
                    status: configurationControllerScope.workLocation.status
                }
            } else {
                updatedList = {
                    country: configurationControllerScope.workLocation.country,
                    oppType: 'update',
                    state: configurationControllerScope.workLocation.state,
                    _id: configurationControllerScope.workLocation._id,
                    worklocation: configurationControllerScope.workLocation.worklocation,
                    status: configurationControllerScope.workLocation.status
                }
            }

            configrationService.updateWorkLocationStatus(updatedList).then(function(response) {

                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        configurationControllerScope.workLocation = "";
                        configurationControllerScope.workLocationForm.$setPristine();
                        configurationControllerScope.workLocationForm.$setUntouched();
                        configurationControllerScope.workLocationUpdateBtn = false;
                        configurationControllerScope.spinner = false;
                        $('#workLocation').modal('hide');
                        getWorkLocationDetails();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }
        if (updateOpp == 'statusEdit') {
            var newStatus = {
                status: status,
                id: id,
                oppType: 'statusEdit'
            }

            configrationService.updateWorkLocationStatus(newStatus).then(function(response) {
                if (response.data.msg) {

                } else {
                    if (response.data.successMsg) {
                        getWorkLocationDetails();
                    }
                }
            }, function(error_response) {
                console.log("error_response", error_response);
            });
        }



    }

    function deleteEmployeeType(employeetypeId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteEmployeeType(employeetypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Employee Type deleted successfully "
                    getEmployeeType();

                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteCustomerType(customertypeId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteCustomerType(customertypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Customer Type deleted successfully"
                    getCustomerType();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }


    function deleteTimeOffType(timeOffTypeId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteTimeOffType(timeOffTypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "TimeOff deleted successfully"
                    getTimeOffType();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteInvoiceType(invoiceTypeId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteInvoiceType(invoiceTypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Invoice Type deleted successfully"
                    getInvoiceType();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deletePaymentTerm(paymentTermId) {
        configurationControllerScope.spinner = true;
        configrationService.deletePaymentTerm(paymentTermId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Payment Term deleted successfully"
                    getPaymentTerm();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteInvoicePaymentTerm(invoicePaymentTermId) {
        configrationService.deleteInvoicePaymentTerm(invoicePaymentTermId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Invoice PaymentTerm deleted successfully "
                    getInvoicePaymentTerm();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteEmployeeRoleType(deleteEmployeeRoleTypeId) {
        configrationService.deleteEmployeeRoleType(deleteEmployeeRoleTypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Employee Role deleted successfully "
                    getEmployeeRole();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteDepartment(departmentId) {
        configrationService.deleteDepartment(departmentId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Department deleted successfully "
                    getDepartment();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function closeModel(formId, fieldName) {
        var form = 'configurationControllerScope.' + formId;
        $('#formId').modal('hide');
    }





    function deleteCountry(countryReferenceId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteCountry(countryReferenceId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Country deleted successfully"
                    getCountryDetals();
                }
            }
        }, function(error) {
            console.log("error", error);
        });

    }

    function deleteVendorType(vendortypeId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteVendorType(vendortypeId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "Vendor Type deleted successfully"
                    getVendorType();
                }
            }
        }, function(error) {
            console.log("error", error);
        });
    }

    function deleteState(stateReferenceId, countryReferenceId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteState(stateReferenceId, countryReferenceId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "State deleted successfully"
                    configurationControllerScope.spinner = false;
                    getStateDetals();
                }
            }
        }, function(error) {
            console.log("error", error);
        });

    }

    function deleteWorkLocation(workLocationReferenceId) {
        configurationControllerScope.spinner = true;
        configrationService.deleteWorkLocation(workLocationReferenceId).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                configurationControllerScope.alertMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    configurationControllerScope.alertMsg = "addWorkLocation deleted successfully"
                    getWorkLocationDetails();
                }
            }
        }, function(error) {
            console.log("error", error);
        });

    }

    function declineMethod(response) {
        if (response == 'payment') {
            configurationControllerScope.paymentTerm = "";
            configurationControllerScope.paymentTerms.$setPristine();
            configurationControllerScope.paymentTerms.$setUntouched();
            configurationControllerScope.paymentTermsUpdateBtn = false;
        } else if (response == 'country') {
            configurationControllerScope.country = "";
            configurationControllerScope.countryDetailForm.$setPristine();
            configurationControllerScope.countryDetailForm.$setUntouched();
            configurationControllerScope.countryUpdate = false;
        } else if (response == 'state') {
            configurationControllerScope.stateInfo = "";
            configurationControllerScope.stateDetailForm.$setPristine();
            configurationControllerScope.stateDetailForm.$setUntouched();
            configurationControllerScope.stateUpdateBtn = false;
        } else if (response == 'workLocation') {
            configurationControllerScope.workLocationUpdateBtn = false;
            configurationControllerScope.workLocation = "";
            configurationControllerScope.workLocationForm.$setPristine();
            configurationControllerScope.workLocationForm.$setUntouched();
        } else if (response == 'department') {
            configurationControllerScope.department = "";
            configurationControllerScope.departmentForm.$setPristine();
            configurationControllerScope.departmentForm.$setUntouched();
            configurationControllerScope.departmentUpdateBtn = false;
        } else if (response == 'employeetype') {
            configurationControllerScope.employeetype = "";
            configurationControllerScope.employeeForm.$setPristine();
            configurationControllerScope.employeeForm.$setUntouched();
            configurationControllerScope.employeeUpdateBtn = false;
        } else if (response == 'customertype') {
            configurationControllerScope.customertype = "";
            configurationControllerScope.customerForm.$setPristine();
            configurationControllerScope.customerForm.$setUntouched();
            configurationControllerScope.customerUpdateBtn = false;
        } else if (response == 'vendortype') {
            configurationControllerScope.vendortype = "";
            configurationControllerScope.vendorForm.$setPristine();
            configurationControllerScope.vendorForm.$setUntouched();
            configurationControllerScope.vendorUpdateBtn = false;

        } else if (response == 'timeofftype') {
            configurationControllerScope.timeofftype = "";
            configurationControllerScope.timeOffTypeForm.$setPristine();
            configurationControllerScope.timeOffTypeForm.$setUntouched();
            configurationControllerScope.timeOffTypeUpdateBtn = false;
        } else if (response == 'invoicetype') {
            configurationControllerScope.invoicetype = "";
            configurationControllerScope.invoicingTypes.$setPristine();
            configurationControllerScope.invoicingTypes.$setUntouched();
            configurationControllerScope.invoiceTypeUpdateBtn = false;
        } else if (response == 'invoicepaymentterm') {
            configurationControllerScope.invoicePaymentTerm = "";
            configurationControllerScope.invoicePaymentTerms.$setPristine();
            configurationControllerScope.invoicePaymentTerms.$setUntouched();
            configurationControllerScope.invoicePaymentTermUpdateBtn = false;
        } else if (response == 'employeerole') {
            configurationControllerScope.employeeRole = "";
            configurationControllerScope.employeeRoleForm.$setPristine();
            configurationControllerScope.employeeRoleForm.$setUntouched();
            configurationControllerScope.employeeRoleUpdateBtn = false
        }
    }


    $("ul").on("click", ".init", function() {
        $(this).closest("ul").children('li:not(.init)').toggle();
    });

    var allOptions = $("ul").children('li:not(.init)');
    $("ul").on("click", "li:not(.init)", function() {
        allOptions.removeClass('selected');
        $(this).addClass('selected');
        $("ul").children('.init').html($(this).html());
        allOptions.toggle();
    });


    function editCountryDetailForm(countryDetails) {
        configurationControllerScope.countryUpdate = true;
        $('#country').modal('show');
        configurationControllerScope.country = countryDetails;
    }

    function editWorkLocationForm(workLocationDrtails) {
        configurationControllerScope.workLocationUpdateBtn = true;
        $('#workLocation').modal('show');
        configurationControllerScope.workLocation = angular.copy(workLocationDrtails);
        configurationControllerScope.workLocation.country = configurationControllerScope.workLocation.country;
        configurationControllerScope.workLocation.state = configurationControllerScope.workLocation.state;
        configurationControllerScope.workLocation.city = configurationControllerScope.workLocation.city;

    }

    function editDepartmentForm(departmentDetails) {
        configurationControllerScope.departmentUpdateBtn = true;
        $('#department').modal('show');

        configurationControllerScope.department = departmentDetails;
    }

    function editEmployeeTypeForm(employeeTypeDetails) {
        configurationControllerScope.employeeUpdateBtn = true;
        $('#employeeTypes').modal('show');

        configurationControllerScope.employeetype = employeeTypeDetails;
    }

    function editCustomerTypeForm(customerTypeDetails) {
        configurationControllerScope.customerUpdateBtn = true;
        $('#customerTypes').modal('show');

        configurationControllerScope.customertype = customerTypeDetails;
    }

    function editTimeOffTypeForm(timeOffTypeDetails) {
        configurationControllerScope.timeOffTypeUpdateBtn = true;
        $('#timeOffTypes').modal('show');

        configurationControllerScope.timeofftype = timeOffTypeDetails;
    }

    function editInvoiceTypesForm(invoiceTypesDetails) {
        configurationControllerScope.invoiceTypeUpdateBtn = true;
        $('#invoicingTypes').modal('show');

        configurationControllerScope.invoicetype = invoiceTypesDetails;
    }

    function editPaymentTermForm(paymentTermFormDetails) {
        configurationControllerScope.paymentTermsUpdateBtn = true;
        $('#paymentTerms').modal('show');

        configurationControllerScope.paymentTerm = paymentTermFormDetails;
    }

    function editInvoicePaymentTermForm(invoicePaymentTermFormDetails) {
        configurationControllerScope.invoicePaymentTermUpdateBtn = true;
        $('#invoicePaymentTerms').modal('show');

        configurationControllerScope.invoicePaymentTerm = invoicePaymentTermFormDetails;
    }

    function editEmployeeRoleForm(employeeRoleFormDetails) {
        configurationControllerScope.employeeRoleUpdateBtn = true;
        $('#employeeRoles').modal('show');

        configurationControllerScope.employeeRole = employeeRoleFormDetails;
    }

    function editVendorTypeForm(vendorTypeFormDetails) {
        configurationControllerScope.vendorUpdateBtn = true;
        $('#vendorTypes').modal('show');

        configurationControllerScope.vendortype = vendorTypeFormDetails;
    }

    function editStateDetailForm(stateDetails) {
        configurationControllerScope.stateUpdateBtn = true;
        $('#state').modal('show');
        getCountryDetals();
        configurationControllerScope.stateInfo = angular.copy(stateDetails);
    }

    function countryChangeEvent() {
        configurationControllerScope.stateInfo.statename = "";
    }

    function openWorkLocationForm() {
        getCountryDetals();
        configurationControllerScope.workLocation = {
            country: "Select Country",
            state: "Select State"
        }
        $('#workLocation').modal('show');
    }

}
