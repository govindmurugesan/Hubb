angular
    .module('ePortal')
    .controller('leadController', leadController);

leadController.$inject = ['$scope', '$location', 'leadService', 'configrationService', 'billServices', '$rootScope', '$cookies', '$filter', 'employeeService', 'constantsService', 'filterFilter'];

function leadController($scope, $location, leadService, configrationService, billServices, $rootScope, $cookies, $filter, employeeService, constantsService, filterFilter) {

    var leadControllerScope = this;
    var createdEmployee = "",
        leadInfoBackup = "";
    $scope.goSrch = false;

    leadControllerScope.followup = {};
    leadControllerScope.spinner = false;
    leadControllerScope.showLeadList = true;
    leadControllerScope.showStatusUl = false;
    leadControllerScope.openFollowupDateContactCalender = openFollowupDateContactCalender;
    leadControllerScope.openDateOfContactCalender = openDateOfContactCalender;
    leadControllerScope.addLead = addLead;
    leadControllerScope.getLead = getLead;
    leadControllerScope.newLeadDecline = newLeadDecline;
    leadControllerScope.decline = decline;
    leadControllerScope.declineContact = declineContact;
    leadControllerScope.followDecline = followDecline;
    leadControllerScope.hideAlertPopup = hideAlertPopup;
    leadControllerScope.showLeadDetail = showLeadDetail;
    leadControllerScope.showEdit = showEdit;
    leadControllerScope.updateLead = updateLead;
    leadControllerScope.selectedStatus = selectedStatus;
    leadControllerScope.leadListView = leadListView;
    leadControllerScope.followedupListView = followedupListView;
    leadControllerScope.showFollowedupListDetail = showFollowedupListDetail;
    leadControllerScope.showFollowupEdit = showFollowupEdit;
    leadControllerScope.addFollowupDate = addFollowupDate;
    leadControllerScope.statusList = constantsService.leadStatus;
    leadControllerScope.selectedStatusFunc = selectedStatusFunc;
    leadControllerScope.createFollowup = createFollowup;
    leadControllerScope.newFollowupContact = newFollowupContact;
    leadControllerScope.leadFun = leadFun;
    leadControllerScope.ContactPersonFun = ContactPersonFun;
    leadControllerScope.contactList = contactList;
    leadControllerScope.showContactDetail = showContactDetail;
    leadControllerScope.declineContactUpdate = declineContactUpdate;
    leadControllerScope.contactEdit = contactEdit;
    leadControllerScope.updateContact = updateContact;
    leadControllerScope.showFollowedupListDetail = showFollowedupListDetail;
    leadControllerScope.declineFollowupUpdate = declineFollowupUpdate;
    leadControllerScope.followupEdit = followupEdit;
    leadControllerScope.updateFollowup = updateFollowup;
    leadControllerScope.updateLeadStatus = updateLeadStatus;
    leadControllerScope.showLead = showLead;
    leadControllerScope.oneLeadListView = oneLeadListView;
    leadControllerScope.addNewContact = addNewContact;

    leadControllerScope.title = "Leads";
    $rootScope.currentMenu = "Sales";
    $scope.initDate = new Date();
    $scope.formats = ['dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    leadControllerScope.contactLead = { leadName: "Select Lead" };
    leadControllerScope.ContactList = { contactPerson: "Select Person" };


    getLoginUserInfo();
    getLead();

    function showLead(lead) {
        leadControllerScope.spinner = true;
        $location.path('/viewLead');
        $rootScope.detailFromViewFollowup = lead;
        leadControllerScope.spinner = false;
    }

    function oneLeadListView() {
        leadControllerScope.spinner = true;
        showLeadDetail(leadInfoBackup);
        leadControllerScope.spinner = false;
    }

    function addNewContact() {
        leadControllerScope.spinner = true;
        hideSection();
        hideButton();
        leadControllerScope.contactLead.leadName = leadControllerScope.leadInfo.leadName;
        leadControllerScope.title = "Contact";
        leadControllerScope.isNewContact = true;
        leadControllerScope.spinner = false;
    }

    function addLead() {
        leadControllerScope.spinner = true;
        var mob = leadControllerScope.lead.mobileNumber,
            phone = leadControllerScope.lead.phoneNumber,
            email = leadControllerScope.lead.email;
        if ((mob == "" || mob == undefined) && (phone == "" || phone == undefined) && (email == "" || email == undefined)) {
            $scope.modalShown = true;
            leadControllerScope.alertMsg = "One of contact is mandatory";
        } else {

            leadService.addLead(leadControllerScope.lead).then(function(response) {
                if (response.data.msg) {
                    leadControllerScope.infoMsg = response.data.msg;
                    leadControllerScope.spinner = false;
                } else {
                    if (response.data.successMsg) {
                        $scope.modalShown = true;
                        leadControllerScope.alertMsg = "Lead added successfully";
                        leadControllerScope.lead = { createdEmployee: createdEmployee };
                        leadControllerScope.newLeadForm.$setPristine();
                        leadControllerScope.newLeadForm.$setUntouched();
                    }
                }
            }, function(error_response) {
                console.log("error", error_response);
            });

        }
        leadControllerScope.spinner = false;

    }

    function newFollowupContact() {
        leadControllerScope.spinner = true;
        var mob = leadControllerScope.followupContact.mobileNumber,
            phone = leadControllerScope.followupContact.phoneNumber,
            email = leadControllerScope.followupContact.email;
        if ((mob == "" || mob == undefined) && (phone == "" || phone == undefined) && (email == "" || email == undefined)) {
            $scope.modalShown = true;
            leadControllerScope.alertMsg = "One of contact is mandatory";
        } else {

            leadControllerScope.followupContact.leadID = leadInfoBackup._id;
            leadService.newFollowupContact(leadControllerScope.followupContact).then(function(response) {
                if (response.data.msg) {
                    leadControllerScope.infoViewMsg = response.data.msg;
                    leadControllerScope.spinner = false;
                } else {
                    if (response.data.successmsg) {
                        $scope.modalShown = true;
                        leadControllerScope.alertMsg = "Followup Contact added successfully for lead " + leadControllerScope.contactLead.leadName;
                        getLeadByReference(leadControllerScope.followupContact.leadID);
                        leadControllerScope.followupContact = "";
                        leadControllerScope.contactLead = { leadName: "Select Lead" };
                        leadControllerScope.newFollowupForm.$setPristine();
                        leadControllerScope.newFollowupForm.$setUntouched();
                        contactList();
                    }
                }
            }, function(error_response) {
                console.log("error", error_response);
            });

        }
        leadControllerScope.spinner = false;

    }

    function updateContact() {
        leadControllerScope.spinner = true;
        leadService.updateContact(leadControllerScope.leadFollowUpContact).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoViewMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    leadControllerScope.alertMsg = "Contact Detail updated successfully";
                    leadControllerScope.leadFollowupContactUpdateForm.$setPristine();
                    leadControllerScope.leadFollowupContactUpdateForm.$setUntouched();
                    contactList();
                }
            }

        });

        leadControllerScope.spinner = false;

        leadControllerScope.spinner = false;
    }

    $scope.reverse = true;
    $scope.show = true;
    $scope.orderByMe = function(followup) {
        $scope.reverse = (followup === followup) ? !$scope.reverse : false;
        $scope.myOrderBy = followup;
        $scope.show = false;
    }

    function getLead() {
        leadControllerScope.spinner = true;
        leadService.getLead().then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoViewMsg = response.data.msg;
                leadControllerScope.leadList = '';

            } else {
                if (response.data) {
                    leadControllerScope.leadList = response.data;

                    leadControllerScope.latestFollowupList = [];
                    leadControllerScope.selectedLeadStatus = [];
                    count = 0;
                    angular.forEach(response.data, function(lead, key) {
                        leadControllerScope.selectedLeadStatus[key] = lead.status;
                        if (lead.followup != undefined && lead.followup.length > 0) {
                            leadControllerScope.latestFollowupList[count] = lead.followup[lead.followup.length - 1];
                            leadControllerScope.latestFollowupList[count].leadName = lead.leadName;
                            leadControllerScope.latestFollowupList[count].lead = lead;
                            count++;
                        }
                    });
                    $scope.currentPage = 1;
                    $scope.totalItems = leadControllerScope.leadList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(leadControllerScope.leadList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                    $scope.currentPage = 1;
                    $scope.totalItems = leadControllerScope.latestFollowupList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                }
            }

        });
        if ($rootScope.detailFromViewFollowup) {
            showLeadDetail($rootScope.detailFromViewFollowup);
            $rootScope.detailFromViewFollowup = "";
        }
        leadControllerScope.spinner = false;
    }

    function updateLead(status) {
        leadControllerScope.spinner = true;
        leadControllerScope.leadInfo.status = status;
        if (leadControllerScope.leadInfo.followup)
            leadControllerScope.leadInfo.followup = "";
        leadService.updateLeadDetail(leadControllerScope.leadInfo).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoMsg = response.data.msg;
                leadControllerScope.leadList = '';
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    leadControllerScope.alertMsg = "Lead Details updated successfully";
                    leadControllerScope.leadSearchKeyword = "";
                    leadControllerScope.viewTitle = "Leads";
                    leadControllerScope.showLeadList = true;
                    leadControllerScope.todoFollowupList = true;
                    leadListView();
                    // showLeadDetail(leadInfoBackup);
                }
            }

        });

        leadControllerScope.spinner = false;
    }

    function createFollowup() {
        leadControllerScope.spinner = true;
        leadControllerScope.followup.leadID = leadControllerScope.leadInfo._id;
        leadControllerScope.followup.contactPerson = leadControllerScope.ContactList.contactPerson;
        leadControllerScope.followup.addedEmployee = createdEmployee.firstName + ' ' + createdEmployee.middleName + ' ' + createdEmployee.lastName;
        leadControllerScope.followup.addedEmployeeID = createdEmployee._id;
        leadService.createFollowup(leadControllerScope.followup).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoViewMsg = response.data.msg;
                leadControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    leadControllerScope.alertMsg = "Followup added successfully";
                    getLeadByReference(leadControllerScope.followup.leadID);
                    hideButton();
                    hideSection();
                    leadControllerScope.isFollowedupList = true;
                    leadControllerScope.showUpdateFollowup = false;
                    leadControllerScope.viewOneLeadButton = true;
                    leadControllerScope.followup = {};
                    leadControllerScope.ContactList = { contactPerson: "Select Contact Person" };
                    leadControllerScope.followupDetailForm.$setPristine();
                    leadControllerScope.followupDetailForm.$setUntouched();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
        leadControllerScope.spinner = false;
    }

    function updateFollowup() {
        leadControllerScope.spinner = true;
        leadControllerScope.followedupInfo.addedEmployeeID = createdEmployee._id;
        leadControllerScope.followedupInfo.addedEmployee = createdEmployee.firstName +' '+createdEmployee.middleName+' '+createdEmployee.lastName;
        console.log('createdEmployee',createdEmployee);
        leadService.updateFollowUp(leadControllerScope.followedupInfo).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoMsg = response.data.msg;
                leadControllerScope.leadList = '';
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    leadControllerScope.alertMsg = "Follow Details updated successfully";
                    leadControllerScope.leadSearchKeyword = "";
                    leadControllerScope.title = "Follow-ups";
                    hideButton();
                    hideSection();
                    getLeadByReference(leadControllerScope.leadInfo._id);
                    leadControllerScope.isFollowedupList = true;
                    leadControllerScope.showUpdateFollowup = false;
                    leadControllerScope.viewOneLeadButton = true;
                    leadControllerScope.edititemtrue = false;
                }
            }

        });

        leadControllerScope.spinner = false;

    }

    function getLeadByReference(lead) {
        leadControllerScope.spinner = true;
        leadService.getLeadByReference(lead).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoViewMsg = response.data.msg;
                leadControllerScope.leadFollowList = '';
            } else {
                if (response.data) {
                    leadControllerScope.infoViewMsg = '';
                    leadControllerScope.leadFollowList = response.data[0].followup;
                    leadControllerScope.followupContactList = response.data[0].followupContact;
                    // followedupListView(response.data[0]);
                    $scope.currentPage = 1;
                    $scope.totalItems = leadControllerScope.leadFollowList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {
                        $scope.filtered = filterFilter(leadControllerScope.leadFollowList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                }
            }

        });
        leadControllerScope.spinner = false;
    }


    function showContactDetail(contact) {
        leadControllerScope.spinner = true;
        hideSection();
        hideButton();
        leadControllerScope.title = "Contact Detail";
        leadControllerScope.leadFollowUpContact = contact;
        leadControllerScope.isEditContact = true;
        leadControllerScope.showEditContact = true;
        leadControllerScope.showDeclineEditContact = true;
        leadControllerScope.spinner = false;
    }

    function showLeadDetail(leadDetail) {
        leadControllerScope.spinner = true;
        leadControllerScope.leadInfo = "";
        leadControllerScope.leadSearchKeyword = "";
        leadControllerScope.title = "Lead Detail";
        leadControllerScope.leadInfo = leadDetail;
        leadInfoBackup = angular.copy(leadDetail);
        leadControllerScope.followupContactList = leadDetail.followupContact;
        leadControllerScope.selectedStatus = leadDetail.status;
        $scope.goSrch = false;
        hideButton();
        hideSection();
        getLeadByReference(leadInfoBackup._id);
        leadControllerScope.showEditButton = true;
        leadControllerScope.nonEditableField = true;
        leadControllerScope.edititemtrue = true;
        leadControllerScope.edititem = false;
        leadControllerScope.fieldEditable = true;
        leadControllerScope.viewLeadButton = true;
        leadControllerScope.viewAddFollowupButton = true;
        leadControllerScope.isLeadUpdate = true;
        leadControllerScope.showLeadEditButton = true;
        leadControllerScope.showUpdateFollowup = false;
        leadControllerScope.todoFollowupList = true;
        leadControllerScope.spinner = false;
    }

    function showFollowedupListDetail(followup) {
        leadControllerScope.spinner = true;
        leadControllerScope.followedupInfo = followup;
        leadControllerScope.followedupInfo.contactTime = followup.dateOfContactTime;
        leadControllerScope.followedupInfo.followTime = followup.followupTime;
        leadControllerScope.followedupInfo.dateOfContactTime = moment(followup.dateOfContactTime);
        leadControllerScope.followedupInfo.followupTime = moment(followup.followupTime);
        leadControllerScope.title = "Follow-up Detail";
        hideButton();
        hideSection();
        leadControllerScope.showDeclineEditFollowup = true;
        leadControllerScope.showEditFollowup = true;
        leadControllerScope.isFollowedupDetail = true;
        leadControllerScope.nonEditableField = true;
        leadControllerScope.edititemtrue = true;
        leadControllerScope.edititem = false;
        leadControllerScope.spinner = false;
    }

    function leadListView() {
        leadControllerScope.spinner = true;
        leadControllerScope.showLeadList = true;
        leadControllerScope.leadSearchKeyword = "";
        leadControllerScope.title = "Leads";
        $scope.goSrch = false;
        hideSection();
        hideButton();
        leadControllerScope.showLeadList = true;
        getLead();
        leadControllerScope.spinner = false;
    }

    function selectedStatusFunc(status) {
        leadControllerScope.spinner = true;
        leadControllerScope.showStatusUl = !leadControllerScope.showStatusUl;
        leadControllerScope.selectedStatus = status;
        leadControllerScope.spinner = false;
    }

    function updateLeadStatus(lead, status) {
        leadControllerScope.spinner = true;
        lead.status = status;
        if (lead.followup)
            lead.followup = "";
        leadService.updateLeadDetail(lead).then(function(response) {
            if (response.data.msg) {
                leadControllerScope.infoMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    getLead();
                }
            }

        });

        leadControllerScope.spinner = false;
    }

    function showEdit() {
        leadControllerScope.spinner = true;
        leadControllerScope.nonEditableField = false;
        leadControllerScope.edititemtrue = true;
        leadControllerScope.edititem = true;
        leadControllerScope.fieldEditable = false;
        $scope.goSrch = false;
        hideButton();
        leadControllerScope.todoFollowupList = false;
        leadControllerScope.showLeadUpdateDeclineButton = true;
        leadControllerScope.showLeadUpdateButton = true;
        leadControllerScope.spinner = false;
    }

    function followupEdit() {
        leadControllerScope.spinner = true;
        leadControllerScope.nonEditableField = false;
        leadControllerScope.edititemtrue = true;
        leadControllerScope.edititem = true;
        leadControllerScope.fieldEditable = false;
        $scope.goSrch = false;
        hideButton();
        leadControllerScope.showLeadUpdateDeclineButton = true;
        leadControllerScope.showUpdateFollowup = true;
        leadControllerScope.spinner = false;
    }

    function contactEdit() {
        leadControllerScope.spinner = true;
        showEdit();
        hideButton();
        leadControllerScope.showDeclineEditContact = true;
        leadControllerScope.showUpdateContact = true;
        // leadControllerScope.isEditContact=true;
        leadControllerScope.spinner = false;
    }

    function addFollowupDate() {
        leadControllerScope.spinner = true;
        hideButton();
        leadControllerScope.title = "Followup";
        $scope.goSrch = false;
        hideSection();
        leadControllerScope.showCreateFollowupButton = true;
        leadControllerScope.showFollowupDeclineButton = true;
        leadControllerScope.isAddFollowup = true;
        leadControllerScope.spinner = false;
    }


    function followedupListView(followedupDetails) {
        leadControllerScope.spinner = true;
        leadControllerScope.leadFollowList = followedupDetails.followup;
        $scope.goSrch = false;
        leadControllerScope.title = "Follow-ups";
        hideSection();
        hideButton();
        leadControllerScope.showLeadEditButton = false;
        leadControllerScope.viewLeadButton = true;
        leadControllerScope.isFollowedupList = true;
        leadControllerScope.spinner = false;
    }

    function showFollowupEdit() {
        leadControllerScope.spinner = true;
        leadControllerScope.nonEditableField = false;
        leadControllerScope.edititemtrue = true;
        $scope.goSrch = false;
        leadControllerScope.edititem = true;
        hideButton();
        leadControllerScope.fieldEditable = false;
        leadControllerScope.spinner = false;
    }

    function getLoginUserInfo() {
        leadControllerScope.spinner = true;
        employeeService.getEmployeeDetailsByEmail($cookies.getObject('saptaePortalCookies').email).then(function(response) {

            if (response.data.msg) {
                leadControllerScope.lead = { createdEmployee: "" };
            } else {
                if (response.data) {
                    leadControllerScope.lead = { createdEmployee: response.data };
                    createdEmployee = response.data;
                    leadControllerScope.followedupInfo = { createdEmployee: response.data };
                }
            }
        }, function(err) {
            console.log(err);
        });
        leadControllerScope.spinner = false;
    }

    function openFollowupDateContactCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        leadControllerScope.followupDate = !leadControllerScope.followupDate;
    }

    function openDateOfContactCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        leadControllerScope.dateOfContactCalender = !leadControllerScope.dateOfContactCalender;
    };

    function decline() {
        leadControllerScope.spinner = true;
        showLeadDetail(leadInfoBackup);
        leadControllerScope.todoFollowupList = true;
        leadControllerScope.spinner = false;
    }

    function followDecline() {
        leadControllerScope.spinner = true;
        leadControllerScope.followup = "";
        leadControllerScope.ContactList = { contactPerson: "Select Person" };
        leadControllerScope.followupDetailForm.$setPristine();
        showLeadDetail(leadInfoBackup);
        leadControllerScope.followupDetailForm.$setUntouched();
        leadControllerScope.spinner = false;
    }

    function declineFollowupUpdate() {
        leadControllerScope.spinner = true;
        leadControllerScope.followedupInfo = "";
        leadControllerScope.followedupDetailUpdateForm.$setPristine();
        leadControllerScope.followedupDetailUpdateForm.$setUntouched();
        hideButton();
        hideSection();
        showLeadDetail(leadInfoBackup);
        leadControllerScope.spinner = false;
    }

    function declineContact() {
        leadControllerScope.spinner = true;
        leadControllerScope.followupContact = "";
        leadControllerScope.followupContact = { email: "" };
        leadControllerScope.followupContact = { phoneNumber: "" };
        leadControllerScope.followupContact = { mobileNumber: "" };
        leadControllerScope.contactLead = { leadName: "Select Lead" };
        leadControllerScope.newFollowupForm.$setPristine();
        leadControllerScope.newFollowupForm.$setUntouched();
        contactList();
        leadControllerScope.spinner = false;
    }

    function declineContactUpdate() {
        leadControllerScope.spinner = true;
        leadControllerScope.leadFollowupContactUpdateForm.$setPristine();
        leadControllerScope.leadFollowupContactUpdateForm.$setUntouched();
        contactList();
        leadControllerScope.spinner = false;
    }

    function newLeadDecline() {
        leadControllerScope.spinner = true;
        leadControllerScope.lead = "";
        leadControllerScope.newLeadForm.$setPristine();
        leadControllerScope.newLeadForm.$setUntouched();
        leadControllerScope.spinner = false;
    }


    function selectedStatus(status) {
        leadControllerScope.spinner = true;
        leadControllerScope.showStatusUl = !leadControllerScope.showStatusUl;
        leadControllerScope.changeActive = "";
        leadControllerScope.leadInfo.statusUpdate = status;
        leadControllerScope.spinner = false;
    }

    function leadFun(lead) {
        leadControllerScope.spinner = true;
        leadControllerScope.contactLead = lead;
        leadControllerScope.spinner = false;
        leadControllerScope.keyword = " ";
    }

    function ContactPersonFun(person) {
        leadControllerScope.spinner = true;
        leadControllerScope.followup = {};
        leadControllerScope.ContactList = person;
        leadControllerScope.followup.designation = person.designation;
        leadControllerScope.followup.im = person.im;
        leadControllerScope.followup.mobileNumber = person.mobileNumber;
        leadControllerScope.followup.phoneNumber = person.phoneNumber;
        leadControllerScope.followup.email = person.email;
        leadControllerScope.followedupInfo = {}
        leadControllerScope.followedupInfo.contactPerson = person.contactPerson;
        leadControllerScope.followedupInfo.designation = person.designation;
        leadControllerScope.followedupInfo.im = person.im;
        leadControllerScope.followedupInfo.mobileNumber = person.mobileNumber;
        leadControllerScope.followedupInfo.phoneNumber = person.phoneNumber;
        leadControllerScope.followedupInfo.email = person.email;
        leadControllerScope.spinner = false;
    }

    function contactList() {
        leadControllerScope.spinner = true;
        hideSection();
        hideButton();
        leadControllerScope.title = "Contacts";
        leadControllerScope.viewOneLeadButton = true;
        leadControllerScope.viewNewContactButton = true;
        leadControllerScope.isContact = true;
        leadControllerScope.spinner = false;

    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewLead');
    }

    function hideSection() {
        leadControllerScope.isLeadUpdate = false;
        leadControllerScope.isFollowedupList = false;
        leadControllerScope.isFollowedupDetail = false;
        leadControllerScope.isAddFollowup = false;
        leadControllerScope.isContact = false;
        leadControllerScope.isEditContact = false;
        leadControllerScope.isFollowedupDetail = false;
        leadControllerScope.isNewContact = false;
    }

    function hideButton() {
        leadControllerScope.spinner = true;
        leadControllerScope.viewLeadButton = false;
        leadControllerScope.viewFollowupButton = false;
        leadControllerScope.addFollowupButton = false;
        leadControllerScope.showEditButton = false;
        leadControllerScope.followupDeclineButton = false;
        leadControllerScope.followupUpdateButton = false;
        leadControllerScope.showLeadEditButton = false;
        leadControllerScope.showLeadUpdateButton = false;
        leadControllerScope.showLeadList = false;
        leadControllerScope.showLeadUpdateDeclineButton = false;
        leadControllerScope.showCreateFollowupButton = false;
        leadControllerScope.showUpdateContact = false;
        leadControllerScope.showDeclineEditContact = false;
        leadControllerScope.showEditContact = false;
        leadControllerScope.showFollowupDeclineButton = false;
        leadControllerScope.spinner = false;
        leadControllerScope.showDeclineEditFollowup = false;
        leadControllerScope.showEditFollowup = false;
        leadControllerScope.showUpdateFollowup = false;
        leadControllerScope.viewOneLeadButton = false;
        leadControllerScope.viewNewContactButton = false;
        leadControllerScope.viewAddFollowupButton = false;
    }

}
