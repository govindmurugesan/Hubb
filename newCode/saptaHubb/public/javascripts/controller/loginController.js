angular
    .module('ePortal')
    .controller('loginController', loginController);

loginController.$inject = ['$scope', '$location', 'usersService', '$cookies'];

function loginController($scope, $location, usersService, $cookies) {
    var loginControllerScope = this;

    loginControllerScope.login = login;
    loginControllerScope.forgotPassword = forgotPassword;
    loginControllerScope.showForgotPassword = showForgotPassword;
    loginControllerScope.showLogin = showLogin;


    loginControllerScope.loginDiv = true;
    loginControllerScope.resetPasswordDiv = false;
    loginControllerScope.resetPasswordSuccessMsgDiv = false;
    loginControllerScope.errMsgDiv = false;
    loginControllerScope.spinner = false;

    function login() {
        loginControllerScope.spinner = true;
        loginControllerScope.errMsgDiv = false;
        usersService.userlogin(loginControllerScope.user).then(function(response) {
            if (response.data.msg) {
                loginControllerScope.errMsgDiv = true;
                loginControllerScope.errMsg = response.data.msg;
                loginControllerScope.spinner = false;
            } else {
                var expireDate = new Date();
                expireDate.setDate(expireDate.getDate() + 7);
                $cookies.putObject('saptaePortalCookies', JSON.parse(response.data), { 'expires': expireDate });
                $location.url('/dashboard');
                loginControllerScope.spinner = false;
            }

        }, function(err) {});
    }

    function forgotPassword() {
        loginControllerScope.spinner = true;
        loginControllerScope.errMsgDiv = false;
        usersService.forgotPassword(loginControllerScope.mailId).then(function(response) {
            if (response.data.msg) {
                loginControllerScope.errMsgDiv = true;
                loginControllerScope.errMsg = response.data.msg;
                loginControllerScope.spinner = false;
            } else {
                if (response.data.successMsg) {
                    loginControllerScope.loginDiv = false;
                    loginControllerScope.resetPasswordDiv = false;
                    loginControllerScope.resetPasswordSuccessMsgDiv = true;
                    loginControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function showForgotPassword() {
        loginControllerScope.spinner = true;
        loginControllerScope.mailId = "";
        loginControllerScope.resetPasswordForm.$setPristine();
        loginControllerScope.resetPasswordForm.$setUntouched();
        loginControllerScope.errMsgDiv = false;
        loginControllerScope.loginDiv = false;
        loginControllerScope.resetPasswordSuccessMsgDiv = false;
        loginControllerScope.resetPasswordDiv = true;
        loginControllerScope.spinner = false;
    }

    function showLogin() {
        loginControllerScope.spinner = true;
        loginControllerScope.errMsgDiv = false;
        loginControllerScope.user = "";
        loginControllerScope.loginForm.$setPristine();
        loginControllerScope.loginForm.$setUntouched();
        loginControllerScope.loginDiv = true;
        loginControllerScope.resetPasswordSuccessMsgDiv = false;
        loginControllerScope.resetPasswordDiv = false;
        loginControllerScope.spinner = false;
    }
}
