angular
    .module('ePortal')
    .controller('createCustomerController', createCustomerController);

createCustomerController.$inject = ['$scope', '$location', 'companyInformationService', 'configrationService', 'customerService', '$rootScope'];




function createCustomerController($scope, $location, companyInformationService, configrationService, customerService, $rootScope) {
    var createCustomerControllerScope = this;



    getCountryList();

    createCustomerControllerScope.addCustomer = addCustomer;
    createCustomerControllerScope.decline = decline;
    createCustomerControllerScope.hideAlertPopup = hideAlertPopup;
    createCustomerControllerScope.countrySelectChange = countrySelectChange;
    createCustomerControllerScope.selectedCountryFun = selectedCountryFun;
    createCustomerControllerScope.selectedStateFun = selectedStateFun;
    createCustomerControllerScope.selectedCustomerFun = selectedCustomerFun;
    createCustomerControllerScope.selectedCountryCodeFun = selectedCountryCodeFun;

    $rootScope.currentMenu = "Create Customer";


    createCustomerControllerScope.customer = {
        country: { name: "Select Country" },
        state: { name: "Select State" },
        customerType: "Select Customer Type",
        name: '',
        address1: '',
        address2: '',
        city: '',
        zip: '',
        countryCode: 'Code',
        contactno: '',
        email: '',
        website: ''
    };

    var customerBackup = angular.copy(createCustomerControllerScope.customer);

    function putDataToCustomerScope(
        country = createCustomerControllerScope.customer.country,
        state = createCustomerControllerScope.customer.state,
        customerType = createCustomerControllerScope.customer.customerType,
        name = createCustomerControllerScope.customer.name,
        address1 = createCustomerControllerScope.customer.address1,
        address2 = createCustomerControllerScope.customer.address2,
        city = createCustomerControllerScope.customer.city,
        zip = createCustomerControllerScope.customer.zip,
        countryCode = createCustomerControllerScope.customer.countryCode,
        contactno = createCustomerControllerScope.customer.contactno,
        email = createCustomerControllerScope.customer.email,
        website = createCustomerControllerScope.customer.website) {

        createCustomerControllerScope.customer = {
            country: country,
            state: state,
            name: name,
            customerType: customerType,
            address1: address1,
            address2: address2,
            city: city,
            zip: zip,
            countryCode: countryCode,
            contactno: contactno,
            email: email,
            website: website
        };
    }



    function selectedCountryFun(countryName, state) {
        createCustomerControllerScope.stateDetailList = state;
        putDataToCustomerScope(countryName);
    }

    function selectedStateFun(stateName) {
        putDataToCustomerScope(createCustomerControllerScope.customer.country, stateName);
    }


    function selectedCustomerFun(customerType) {
        putDataToCustomerScope(createCustomerControllerScope.customer.country, createCustomerControllerScope.customer.state, customerType);
    }


    function selectedCountryCodeFun(phoneCode) {
        putDataToCustomerScope(createCustomerControllerScope.customer.country,
            createCustomerControllerScope.customer.state,
            createCustomerControllerScope.customer.customerType,
            createCustomerControllerScope.customer.name,
            createCustomerControllerScope.customer.address1,
            createCustomerControllerScope.customer.address2,
            createCustomerControllerScope.customer.city,
            createCustomerControllerScope.customer.zip,
            phoneCode);
    }

    function getCountryList() {
        createCustomerControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                createCustomerControllerScope.alertMsg = response.data.msg;
                createCustomerControllerScope.spinner = false;
            } else {
                if (response.data) {
                    createCustomerControllerScope.countryList = response.data;
                    createCustomerControllerScope.spinner = false;
                    getCustomerType();

                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getCustomerType() {
        createCustomerControllerScope.spinner = true;
        configrationService.getCustomerType().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                createCustomerControllerScope.alertMsg = response.data.msg;
                createCustomerControllerScope.spinner = false;

            } else {
                if (response.data.length > 0) {
                    createCustomerControllerScope.customerTypeList = response.data;
                    createCustomerControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addCustomer() {
        createCustomerControllerScope.spinner = true;
        createCustomerControllerScope.infoMsg = "";
        customerService.addCustomer(createCustomerControllerScope.customer).then(function(response) {

            if (response.data.msg) {
                createCustomerControllerScope.spinner = false;
                createCustomerControllerScope.infoMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    createCustomerControllerScope.alertMsg = "Customer Created successfully"
                    createCustomerControllerScope.customer = customerBackup;
                    createCustomerControllerScope.createCustomerForm.$setPristine();
                    createCustomerControllerScope.createCustomerForm.$setUntouched();
                    createCustomerControllerScope.spinner = false;
                    createCustomerControllerScope.infoMsg = response.data.msg;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });

    }

    function decline() {
        createCustomerControllerScope.spinner = true;
        createCustomerControllerScope.customer = "";
        createCustomerControllerScope.createCustomerForm.$setPristine();
        createCustomerControllerScope.createCustomerForm.$setUntouched();
        createCustomerControllerScope.spinner = false;
        createCustomerControllerScope.customer = {
            country: { name: "Select Country" },
            state: { name: "Select State" },
            customerType: "Select Customer Type",
            countryCode: 'Code'
        };

    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewCustomer');
    }


    function countrySelectChange(countryDetail) {
        createCustomerControllerScope.customer.countryCode = countryDetail.callingCode + '(' + countryDetail.name + ')'
    }
}
