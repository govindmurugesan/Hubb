angular
    .module('ePortal')
    .controller('viewBillController', viewBillController);

viewBillController.$inject = ['$scope', '$location', 'vendorService', 'configrationService', 'billServices', 'filterFilter', '$filter', '$rootScope','constantsService'];

function viewBillController($scope, $location, vendorService, configrationService, billServices, filterFilter, $filter, $rootScope,constantsService) {

    var viewBillControllerScope = this;

    viewBillControllerScope.spinner = false;
    viewBillControllerScope.showBillList = true;
    viewBillControllerScope.dueDateCalender = false;
    viewBillControllerScope.billingDateCalender = false;
    viewBillControllerScope.selectVendorType="Select Vendor";
    viewBillControllerScope.selectCurrencyType="Select Currency";
    viewBillControllerScope.selectStatus="Select Status";
    viewBillControllerScope.title = "Bills";
    viewBillControllerScope.infoMsg = '';
    viewBillControllerScope.gotoNewBill = gotoNewBill;
    viewBillControllerScope.getBillList = getBillList;
    viewBillControllerScope.showBillDetail = showBillDetail;
    viewBillControllerScope.updateBill = updateBill;
    viewBillControllerScope.billListView = billListView;
    viewBillControllerScope.decline = decline;
    viewBillControllerScope.showEdit = showEdit;
    viewBillControllerScope.openbillingDateCalender = openbillingDateCalender;
    viewBillControllerScope.opendueDateCalender = opendueDateCalender;
    viewBillControllerScope.hideAlertPopup = hideAlertPopup;
    viewBillControllerScope.selectedStatusFunc=selectedStatusFunc;
    viewBillControllerScope.selectedVendorFunc=selectedVendorFunc;
    viewBillControllerScope.selectedshowCurrencyTypeFunc=selectedshowCurrencyTypeFunc;

    $rootScope.currentMenu = "View Bills";


    viewBillControllerScope.statusList = [{
        status: constantsService.activeState

    }, {
        status: constantsService.inactiveState

    }];

    var billDetailInfoBackup;


    getBillList();


    function updateBill() {

        viewBillControllerScope.spinner = true;

        viewBillControllerScope.billInfo.vendor=viewBillControllerScope.selectVendorType;
        viewBillControllerScope.billInfo.status=viewBillControllerScope.selectStatus;
        viewBillControllerScope.billInfo.currencytype=viewBillControllerScope.selectCurrencyType;
        billServices.updateBill(viewBillControllerScope.billInfo).then(function(response) {

            if (response.data.msg) {
                viewBillControllerScope.infoMsg = response.data.msg;
                viewBillControllerScope.currencyTypeList = '';
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    viewBillControllerScope.alertMsg = "Bill Details Updated Successfully"
                    viewBillControllerScope.showBillList = true;
                    viewBillControllerScope.billSearchKeyword = "";
                    viewBillControllerScope.title = "Bills";
                    getBillList();
                }

            }
        });
        viewBillControllerScope.spinner = false;

    }

    function getBillList() {

        viewBillControllerScope.infoMsg = "";
        viewBillControllerScope.spinner = true;
        billServices.getBill().then(function(response) {
            if (response.data.msg) {
                viewBillControllerScope.spinner = false;
                viewBillControllerScope.infoMsg = response.data.msg;
                viewBillControllerScope.billList = '';
            } else {
                if (response.data) {
                    viewBillControllerScope.billList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = viewBillControllerScope.billList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(viewBillControllerScope.billList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    viewBillControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getVndorList() {
        viewBillControllerScope.spinner = true;
        vendorService.getVendorList().then(function(response) {
            if (response.data.msg) {
                viewBillControllerScope.spinner = false;
                viewBillControllerScope.infoMsg = response.data.msg;
                viewBillControllerScope.vendorList = '';
            } else {
                viewBillControllerScope.spinner = false;
                viewBillControllerScope.vendorList = response.data;
            }
        });
    }

    function showBillDetail(billDetails) {
        viewBillControllerScope.billInfo = "";
        billDetailInfoBackup = "";

        viewBillControllerScope.showEditButton = true;
        viewBillControllerScope.showBillList = false;
        viewBillControllerScope.nonEditableField = true;
        viewBillControllerScope.edititemtrue = true;
        viewBillControllerScope.edititem = false;
        viewBillControllerScope.showbillList = false;
        viewBillControllerScope.billsearchKeyword = "";
        viewBillControllerScope.title = "Bill Detail";
        billDetailInfoBackup = angular.copy(billDetails);
        viewBillControllerScope.selectVendorType=billDetails.vendor;
        viewBillControllerScope.selectStatus=billDetails.status;
        viewBillControllerScope.selectCurrencyType=billDetails.currencytype;
        billDetails.billdate = $filter('date')(billDetails.billdate, $scope.format);
        billDetails.duedate = $filter('date')(billDetails.duedate, $scope.format);
        viewBillControllerScope.billInfo = angular.copy(billDetails);
        viewBillControllerScope.fieldEditable = true;
    }

    function showEdit() {
        getVndorList();
        getCurrencyType();
        viewBillControllerScope.spinner = true;
        viewBillControllerScope.nonEditableField = false;
        viewBillControllerScope.edititemtrue = true;
        viewBillControllerScope.edititem = true;
        viewBillControllerScope.showEditButton = false;
        viewBillControllerScope.spinner = false;
    }

    function getCurrencyType() {
        viewBillControllerScope.spinner = true;
        configrationService.getCurrencyType().then(function(response) {
            if (response.data.msg) {
                viewBillControllerScope.spinner = false;
                viewBillControllerScope.infoMsg = response.data.msg;
                viewBillControllerScope.currencyTypeList = '';
            } else {
                viewBillControllerScope.spinner = false;
                viewBillControllerScope.currencyTypeList = response.data;
            }
        });
    }

    function gotoNewBill() {
        $location.url('/newBill')
    }

    function opendueDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewBillControllerScope.dueDateCalender = !viewBillControllerScope.dueDateCalender;
    }

    function openbillingDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewBillControllerScope.billingDateCalender = !viewBillControllerScope.billingDateCalender;
    }

    function billListView() {
        viewBillControllerScope.spinner = true;
        viewBillControllerScope.showBillList = true;
        viewBillControllerScope.billSearchKeyword = "";
        viewBillControllerScope.title = "Bills";
        getVndorList();
        getCurrencyType();
        viewBillControllerScope.spinner = false;
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function decline() {
        billListView();
    }

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    $scope.initDate = new Date();
    $scope.formats = ['dd-MMMM-yyyy','dd MMMM yyyy','MMMM d, yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    function selectedVendorFunc(vendor){
        viewBillControllerScope.spinner = true;
        viewBillControllerScope.selectVendorType=vendor;
        viewBillControllerScope.spinner = false;
    }

     function selectedStatusFunc(status){
        viewBillControllerScope.spinner = true;
        viewBillControllerScope.selectStatus=status;
        viewBillControllerScope.spinner = false;
    }
    function selectedshowCurrencyTypeFunc(currency){
        viewBillControllerScope.spinner = true;
        viewBillControllerScope.selectCurrencyType=currency;
        viewBillControllerScope.spinner = false;
    }

}
