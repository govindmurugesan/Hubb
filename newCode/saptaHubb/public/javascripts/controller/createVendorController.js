angular
    .module('ePortal')
    .controller('createVendorController', createVendorController);

createVendorController.$inject = ['$scope', '$location', 'companyInformationService', 'configrationService', 'vendorService', '$rootScope'];

function createVendorController($scope, $location, companyInformationService, configrationService, vendorService, $rootScope) {
    var createVendorControllerScope = this;

    createVendorControllerScope.selectedCountryFun = selectedCountryFun;
    createVendorControllerScope.selectedstateFun = selectedstateFun;
    createVendorControllerScope.selectedvendorFun = selectedvendorFun;
    createVendorControllerScope.selectedcodeFun = selectedcodeFun;

    createVendorControllerScope.selectedCountry = { name: 'Select Country' };
    createVendorControllerScope.selectedState = { name: 'Select Select' };
    createVendorControllerScope.selectedvendor = { name: 'Select Vendor' };
    createVendorControllerScope.selectCountryCodeType = { countryCode: 'Code' };

    createVendorControllerScope.stateList = '';

    getCountryList();


    createVendorControllerScope.addVendor = addVendor;
    createVendorControllerScope.decline = decline;
    createVendorControllerScope.hideAlertPopup = hideAlertPopup;
    $rootScope.currentMenu = "Create Vendor";

    createVendorControllerScope.countrySelectChange = countrySelectChange;


    function getCountryList() {
        createVendorControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                createVendorControllerScope.alertMsg = response.data.msg;
                createVendorControllerScope.spinner = false;

            } else {
                if (response.data) {
                    createVendorControllerScope.countryList = response.data;
                    createVendorControllerScope.spinner = false;
                    getVendorType();
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getVendorType() {
        createVendorControllerScope.spinner = true;
        configrationService.getVendorType().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                createVendorControllerScope.alertMsg = response.data.msg;
                createVendorControllerScope.spinner = false;
            } else {
                if (response.data.length > 0) {
                    createVendorControllerScope.vendorTypeList = response.data;
                    createVendorControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function addVendor() {
        createVendorControllerScope.spinner = true;
        createVendorControllerScope.infoMsg = "";

        createVendorControllerScope.vendor.countryCode = createVendorControllerScope.selectCountryCodeType.countryCode;
        createVendorControllerScope.vendor.vendorType = createVendorControllerScope.selectedvendor.name;
        createVendorControllerScope.vendor.country = createVendorControllerScope.selectedCountry;
        createVendorControllerScope.vendor.state = createVendorControllerScope.selectedState;
        vendorService.addVendor(createVendorControllerScope.vendor).then(function(response) {

            if (response.data.msg) {
                createVendorControllerScope.infoMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    createVendorControllerScope.alertMsg = "Vendor Created successfully"
                    createVendorControllerScope.vendor = "";
                    createVendorControllerScope.createVendorForm.$setPristine();
                    createVendorControllerScope.createVendorForm.$setUntouched();
                    createVendorControllerScope.selectedCountry = { name: 'Select Country' };
                    createVendorControllerScope.selectedState = { name: 'Select Select' };
                    createVendorControllerScope.selectedvendor = { name: 'Select Vendor' };
                    createVendorControllerScope.selectedCode = { name: 'Select Country Code' };
                    createVendorControllerScope.infoMsg = response.data.msg;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
        createVendorControllerScope.spinner = false;

    }

    function decline() {
        createVendorControllerScope.spinner = true;
        createVendorControllerScope.vendor = "";
        createVendorControllerScope.selectedCountry = { name: 'Select Country' };
        createVendorControllerScope.selectedState = { name: 'Select Select' };
        createVendorControllerScope.selectedvendor = { name: 'Select Vendor' };
        createVendorControllerScope.selectedCode = { name: 'Select Country Code' };
        createVendorControllerScope.createVendorForm.$setPristine();
        createVendorControllerScope.createVendorForm.$setUntouched();
        createVendorControllerScope.spinner = false;
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewVendor');
    }

    function countrySelectChange(countryDetail) {
        createVendorControllerScope.vendor.countryCode = countryDetail.callingCode + '(' + countryDetail.name + ')'
    }

    function selectedCountryFun(country) {

        createVendorControllerScope.selectedCountry = country;
        createVendorControllerScope.stateList = country.state;
    }

    function selectedstateFun(state) {
        createVendorControllerScope.selectedState = state;
    }

    function selectedvendorFun(vendor) {
        createVendorControllerScope.selectedvendor = vendor;
    }

    function selectedcodeFun(code) {
        createVendorControllerScope.selectCountryCodeType = { countryCode: code.callingCode + ' (' + code.countryCode + ')' };
    }
}
