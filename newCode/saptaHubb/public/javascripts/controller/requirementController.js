angular
    .module('ePortal')
    .controller('requirementController', requirementController);

requirementController.$inject = ['$scope', '$location', 'customerService', 'projectService', 'requirementService', 'configrationService', 'filterFilter', '$filter', '$rootScope'];

function requirementController($scope, $location, customerService, projectService, requirementService, configrationService, filterFilter, $filter, $rootScope) {

    var requirementControllerScope = this;
    requirementControllerScope.showBillList = true;
    requirementControllerScope.spinner = false;
    requirementControllerScope.showUl = false;
    requirementControllerScope.showUls = false;
    requirementControllerScope.showRequirementList = true;
    requirementControllerScope.requirementList = '';
    requirementControllerScope.requirement = '';
    requirementControllerScope.viewRequirementTitle = "Requirements";


    requirementControllerScope.selectedCustomer = { name: 'Choose Customer' };
    requirementControllerScope.selectedProject = { projectName: 'Choose Project' };


    requirementControllerScope.addRequirements = addRequirements;
    requirementControllerScope.getCustomerList = getCustomerList;
    requirementControllerScope.addRequirement = addRequirement;
    requirementControllerScope.getRequirement = getRequirement;
    requirementControllerScope.hideAlertPopup = hideAlertPopup;
    requirementControllerScope.decline = decline;
    requirementControllerScope.selectedCustomerFun = selectedCustomerFun;
    requirementControllerScope.selectedProjectFun = selectedProjectFun;
    requirementControllerScope.showRequirementDetail = showRequirementDetail;
    requirementControllerScope.requirementListView = requirementListView;
    requirementControllerScope.showEdit = showEdit;
    requirementControllerScope.declineViewRequirement = declineViewRequirement;
    requirementControllerScope.updateRequirement = updateRequirement;


    getCustomerList();
    getProject();
    getRequirement();

    function selectedCustomerFun(customer) {
        requirementControllerScope.showUl = !requirementControllerScope.showUl;
        requirementControllerScope.selectedCustomer = customer;
    }

    function selectedProjectFun(project) {
        requirementControllerScope.showUls = !requirementControllerScope.showUls;
        requirementControllerScope.selectedProject = project;
    }



    function addRequirement() {
        requirementControllerScope.spinner = true;

        requirementControllerScope.requirement.customer = requirementControllerScope.selectedCustomer;
        requirementControllerScope.requirement.projectName = requirementControllerScope.selectedProject;

        requirementService.addRequirement(requirementControllerScope.requirement).then(function(response) {
            if (response.data.msg) {
                requirementControllerScope.infoMsg = response.data.msg;
                requirementControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    requirementControllerScope.alertMsg = "Requirement added successfully";
                    requirementControllerScope.requirement = "";
                    requirementControllerScope.selectedCustomer = { name: 'Choose Customer' };
                    requirementControllerScope.selectedProject = { projectName: "Choose Project" };
                    requirementControllerScope.createRequirementForm.$setPristine();
                    requirementControllerScope.createRequirementForm.$setUntouched();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });

        requirementControllerScope.spinner = false;

    }

    function updateRequirement() {
        requirementControllerScope.spinner = true;

        requirementControllerScope.requirementInfo.customer = requirementControllerScope.selectedCustomer;
        requirementControllerScope.requirementInfo.projectName = requirementControllerScope.selectedProject;


        requirementService.updateRequirementDetail(requirementControllerScope.requirementInfo).then(function(response) {
            if (response.data.msg) {
                requirementControllerScope.infoMsg = response.data.msg;
                requirementControllerScope.requirementList = '';
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    requirementControllerScope.alertMsg = "Requirement Details updated successfully";
                    requirementControllerScope.requirementSearchKeyword = "";
                    requirementControllerScope.viewRequirementTitle = "Requirements";
                    requirementListView();
                }
            }
        });

        requirementControllerScope.spinner = false;
    }

    function showEdit() {
        requirementControllerScope.spinner = true;
        requirementControllerScope.nonEditableField = false;
        requirementControllerScope.edititemtrue = true;
        requirementControllerScope.edititem = true;
        requirementControllerScope.showEditButton = false;
        requirementControllerScope.fieldEditable = false;
        requirementControllerScope.spinner = false;
    }

    function showRequirementDetail(requirementDetail) {
        requirementControllerScope.spinner = true;
        requirementControllerScope.requirementInfo = requirementDetail;
        requirementControllerScope.selectedCustomer = requirementDetail.customer;
        requirementControllerScope.selectedProject = requirementDetail.projectName;
        requirementControllerScope.viewRequirementTitle = "Requirement Detail";
        requirementControllerScope.showEditButton = true;
        requirementControllerScope.nonEditableField = true;
        requirementControllerScope.edititemtrue = true;
        requirementControllerScope.edititem = false;
        requirementControllerScope.showRequirementList = false;
        requirementControllerScope.spinner = false;

    }

    function requirementListView() {
        requirementControllerScope.spinner = true;
        requirementControllerScope.viewRequirementTitle = "Requirements";
        requirementControllerScope.showRequirementList = true;
        requirementControllerScope.spinner = false;

    }

    function getCustomerList() {
        requirementControllerScope.spinner = true;
        customerService.getCustomerList().then(function(response) {
            if (response.data.msg) {
                requirementControllerScope.infoMsg = response.data.msg;
                requirementControllerScope.customerList = '';
            } else {
                if (response.data) {
                    requirementControllerScope.customerList = response.data;
                }
            }

        });

        requirementControllerScope.spinner = false;
    }


    function getProject() {

        requirementControllerScope.spinner = true;
        projectService.getProject().then(function(response) {
            if (response.data.msg) {
                requirementControllerScope.infoMsg = response.data.msg;
                requirementControllerScope.projectList = '';

            } else {
                if (response.data) {
                    requirementControllerScope.projectList = response.data;
                }
            }

        });
        requirementControllerScope.spinner = false;
    }


    function getRequirement() {
        requirementService.getRequirement().then(function(response) {
            if (response.data.msg) {
                requirementControllerScope.spinner = false;
                requirementControllerScope.infoViewMsg = response.data.msg;
                requirementControllerScope.requirementList = '';
            } else {
                requirementControllerScope.requirementList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = requirementControllerScope.requirementList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(requirementControllerScope.requirementList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
            }
        });
    }

    function decline() {
        requirementControllerScope.spinner = true;
        requirementControllerScope.requirement = "";
        requirementControllerScope.createRequirementForm.$setPristine();
        requirementControllerScope.createRequirementForm.$setUntouched();
        requirementControllerScope.spinner = false;
    }

    function declineViewRequirement() {
        requirementListView();
    }

    function addRequirements() {
        $location.url('/createRequirement');
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
                    $location.path('/viewRequirement');
    }
}
