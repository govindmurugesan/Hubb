angular
    .module('ePortal')
    .controller('offersController', offersController);

offersController.$inject = ['$scope', '$location', 'projectService', 'offerService', 'configrationService', 'billServices', 'filterFilter', '$filter', '$rootScope'];

function offersController($scope, $location, projectService, offerService, configrationService, billServices, filterFilter, $filter, $rootScope) {

    var offersControllerScope = this;
    offersControllerScope.showUls = false;
    offersControllerScope.spinner = false;
    offersControllerScope.showOffersList = true;
    offersControllerScope.addOffers = addOffers;
    offersControllerScope.openOfferingDateCalender = openOfferingDateCalender;
    offersControllerScope.openJoiningDateCalender = openJoiningDateCalender;
    offersControllerScope.selectedRequirementFun = selectedRequirementFun;
    offersControllerScope.addOffer = addOffer;
    offersControllerScope.offersListView = offersListView;
    offersControllerScope.showOffersDetail = showOffersDetail;
    offersControllerScope.showEdit = showEdit;
    offersControllerScope.hideAlertPopup = hideAlertPopup;
    offersControllerScope.decline = decline;
    offersControllerScope.updateOffer = updateOffer;
    offersControllerScope.printOffer = printOffer;
    // offersControllerScope.printToCart = printToCart;
    offersControllerScope.exportss = exportss;

    $rootScope.currentMenu = "Offers";
    offersControllerScope.viewTitle = "Offers";

    offersControllerScope.selectedRequirement = { projectName: 'Select Requirement' };


    function selectedRequirementFun(project) {
        offersControllerScope.showUls = !offersControllerScope.showUls;
        offersControllerScope.selectedRequirement = project;
    }

    getProject();
    getOffer();

    function addOffer() {
        offersControllerScope.spinner = true;
        offersControllerScope.offer.projectName = offersControllerScope.selectedRequirement;

        offerService.addOffer(offersControllerScope.offer).then(function(response) {
            if (response.data.msg) {
                offersControllerScope.infoMsg = response.data.msg;
                offersControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    offersControllerScope.alertMsg = "Offer Details added successfully";
                    offersControllerScope.offer = "";
                    offersControllerScope.selectedRequirement = { projectName: "Choose Requirement" };
                    offersControllerScope.createOfferForm.$setPristine();
                    offersControllerScope.createOfferForm.$setUntouched();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });

        offersControllerScope.spinner = false;

    }

    function updateOffer() {

        offersControllerScope.spinner = true;
        offersControllerScope.offersInfo.projectName = offersControllerScope.selectedRequirement;
        offerService.updateOffer(offersControllerScope.offersInfo).then(function(response) {
            if (response.data.msg) {
                offersControllerScope.infoMsg = response.data.msg;
                offersControllerScope.offersList = '';
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    offersControllerScope.alertMsg = "Offer Details updated successfully";
                    offersControllerScope.offersSearchKeyword = "";
                    offersControllerScope.viewTitle = "Offers";
                    offersListView();
                }
            }
        });

        offersControllerScope.spinner = false;
    }

    function showEdit() {
        offersControllerScope.spinner = true;
        offersControllerScope.nonEditableField = false;
        offersControllerScope.edititemtrue = true;
        offersControllerScope.edititem = true;
        offersControllerScope.showEditButton = false;
        offersControllerScope.fieldEditable = false;
        offersControllerScope.spinner = false;
    }

    function showOffersDetail(offerDetail) {
        offersControllerScope.spinner = true;
        offersControllerScope.offersInfo = offerDetail;
        offersControllerScope.selectedRequirement = offerDetail.projectName;
        offersControllerScope.viewTitle = "Offer Detail";
        offersControllerScope.showEditButton = true;
        offersControllerScope.nonEditableField = true;
        offersControllerScope.edititemtrue = true;
        offersControllerScope.edititem = false;
        offersControllerScope.showOffersList = false;
        offersControllerScope.spinner = false;

    }

    function getOffer() {

        offersControllerScope.spinner = true;
        offerService.getOffer().then(function(response) {
            if (response.data.msg) {
                offersControllerScope.infoViewMsg = response.data.msg;
                offersControllerScope.offersList = '';

            } else {
                if (response.data) {
                    offersControllerScope.offersList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = offersControllerScope.offersList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(offersControllerScope.offersList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);
                }
            }

        });

        offersControllerScope.spinner = false;

    }

    function getProject() {

        offersControllerScope.spinner = true;
        projectService.getProject().then(function(response) {
            if (response.data.msg) {
                offersControllerScope.infoMsg = response.data.msg;
                offersControllerScope.projectList = '';

            } else {
                if (response.data) {
                    offersControllerScope.projectList = response.data;
                }
            }

        });

        offersControllerScope.spinner = false;
    }

    function offersListView() {
        offersControllerScope.spinner = true;
        offersControllerScope.showOffersList = true;
        offersControllerScope.offersSearchKeyword = "";
        offersControllerScope.viewTitle = "Offers";
        getOffer();
        offersControllerScope.spinner = false;
    }


    function openOfferingDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        offersControllerScope.offerDateCalender = !offersControllerScope.offerDateCalender;
    }


    function openJoiningDateCalender($event) {

        $event.preventDefault();
        $event.stopPropagation();
        offersControllerScope.joiningDateCalender = !offersControllerScope.joiningDateCalender;
    }

    function addOffers() {
        $location.url('/createOffers');
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewOffers');
    }


    function decline() {
        offersListView();
    }
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    function printOffer() {
        //print Offer Data
        exportss();
    }

    // function printToCart(printSectionId) {
    //  console.log("print")
    //      var innerContents = document.getElementById(printSectionId).innerHTML;
    //      var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    //      popupWinindow.document.open();
    //      popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
    //      popupWinindow.document.close();
    //    }


    function exportss() {
        html2canvas(document.getElementById('exportthis'), {
            onrendered: function(canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).open("test.pdf");
            }
        });
    }

    $scope.initDate = new Date();
    $scope.formats = ['dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}
