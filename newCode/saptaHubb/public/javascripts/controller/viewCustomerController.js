angular
    .module('ePortal')
    .controller('viewCustomerController', viewCustomerController);

viewCustomerController.$inject = ['$scope', '$location', 'customerService', 'filterFilter', 'companyInformationService', 'configrationService', '$filter', '$rootScope','constantsService'];

function viewCustomerController($scope, $location, customerService, filterFilter, companyInformationService, configrationService, $filter, $rootScope,constantsService) {
    var viewCustomerControllerScope = this;

    viewCustomerControllerScope.showCustomerList = true;
    viewCustomerControllerScope.customersearchKeyword = "";
    viewCustomerControllerScope.title = "Customers"
    getCustomerList();

    viewCustomerControllerScope.showCustomerDetail = showCustomerDetail;
    viewCustomerControllerScope.showEdit = showEdit;
    viewCustomerControllerScope.decline = decline;
    viewCustomerControllerScope.updateDetails = updateDetails;
    viewCustomerControllerScope.customerListView = customerListView;
    viewCustomerControllerScope.hideAlertPopup = hideAlertPopup;
    viewCustomerControllerScope.gotoCreateCustomer = gotoCreateCustomer;
    viewCustomerControllerScope.selectedCustomerType = selectedCustomerType;
    viewCustomerControllerScope.selectedCountryCodeType=selectedCountryCodeType;
    viewCustomerControllerScope.selectedCountry=selectedCountry;
    viewCustomerControllerScope.selectedState=selectedState;
    viewCustomerControllerScope.selectedStatus=selectedStatus;
    $rootScope.currentMenu = "View Customer";

    viewCustomerControllerScope.statusUpdate = [{
        status: constantsService.activeState,
    }, {
        status: constantsService.inactiveState,
    }]

    var customerDetailInfoBackup;

    function getCustomerList() {
        viewCustomerControllerScope.infoMsg = "";
        viewCustomerControllerScope.spinner = true;
        customerService.getCustomerList().then(function(response) {
            if (response.data.msg) {
                viewCustomerControllerScope.spinner = false;
                viewCustomerControllerScope.infoMsg = response.data.msg;
                viewCustomerControllerScope.customerList = '';
            } else {
                if (response.data) {
                    viewCustomerControllerScope.customerList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = viewCustomerControllerScope.customerList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(viewCustomerControllerScope.customerList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    viewCustomerControllerScope.spinner = false;

                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function showCustomerDetail(customerDetails) {
        viewCustomerControllerScope.customerInfo = "";
        customerDetailInfoBackup = "";

        viewCustomerControllerScope.showEditButton = true;
        viewCustomerControllerScope.nonEditableField = true;
        viewCustomerControllerScope.edititemtrue = true;
        viewCustomerControllerScope.edititem = false;
        viewCustomerControllerScope.showCustomerList = false;
        viewCustomerControllerScope.customersearchKeyword = "";
        viewCustomerControllerScope.title = "Customer Detail"
        customerDetailInfoBackup = angular.copy(customerDetails);
        viewCustomerControllerScope.customerInfo = angular.copy(customerDetails);
        viewCustomerControllerScope.selectCustomerType=customerDetails.customerType;
        viewCustomerControllerScope.selectCountryCodeType=customerDetails.countryCode;
        viewCustomerControllerScope.selectCountry=customerDetails.country[0];
        viewCustomerControllerScope.selectState=customerDetails.state;
        viewCustomerControllerScope.selectStatus=customerDetails.status;
        /*if(viewCustomerControllerScope.customerInfo.status == 'a') viewCustomerControllerScope.customerInfo.status = viewCustomerControllerScope.statusUpdate[0];
        else viewCustomerControllerScope.customerInfo.status = viewCustomerControllerScope.statusUpdate[1];*/
        viewCustomerControllerScope.fieldEditable = true;
    }

    function showEdit() {
        viewCustomerControllerScope.customerInfo.countryDetail = viewCustomerControllerScope.customerInfo.country[0];
        getCountryList();
        getCustomerType();
        viewCustomerControllerScope.spinner = true;
        viewCustomerControllerScope.nonEditableField = false;
        viewCustomerControllerScope.edititemtrue = true;
        viewCustomerControllerScope.edititem = true;
        viewCustomerControllerScope.showEditButton = false;
        viewCustomerControllerScope.fieldEditable = false;
        viewCustomerControllerScope.spinner = false;
    }

    function decline() {

        customerListView();


    }

    function getCountryList() {
        viewCustomerControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {
            if (response.data.msg) {

            } else {
                if (response.data) {
                    viewCustomerControllerScope.countryList = response.data;
                    viewCustomerControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getCustomerType() {
        viewCustomerControllerScope.spinner = true;
        configrationService.getCustomerType().then(function(response) {
            if (response.data.msg) {
                /*createCustomerControllerScope.spinner = false;
                createCustomerControllerScope.infoMsg = response.data.msg;
                createCustomerControllerScope.customerTypeList = '';*/

            } else {
                if (response.data.length > 0) {
                    var c = { status: 'a' };
                    var ss = $filter('filter')(response.data, { status: "a" });
                    viewCustomerControllerScope.customerTypeList = response.data;
                    viewCustomerControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function updateDetails() {
        viewCustomerControllerScope.spinner = true;
        if (viewCustomerControllerScope.customerInfo.status.status) {
            viewCustomerControllerScope.customerInfo.status = viewCustomerControllerScope.customerInfo.status.status
        }
        if (viewCustomerControllerScope.customerInfo.customerType.name) viewCustomerControllerScope.customerInfo.customerType = viewCustomerControllerScope.customerInfo.customerType.name;
        if (viewCustomerControllerScope.customerInfo.countryCode.name) viewCustomerControllerScope.customerInfo.countryCode = viewCustomerControllerScope.customerInfo.countryCode.name;
        viewCustomerControllerScope.customerInfo.customerType=viewCustomerControllerScope.selectCustomerType;
        viewCustomerControllerScope.customerInfo.countryCode=viewCustomerControllerScope.selectCountryCodeType;
        viewCustomerControllerScope.customerInfo.countryDetail=viewCustomerControllerScope.selectCountry;
        viewCustomerControllerScope.customerInfo.state=viewCustomerControllerScope.selectState;
        viewCustomerControllerScope.customerInfo.status=viewCustomerControllerScope.selectStatus;

        customerService.updateCustomerDetail(viewCustomerControllerScope.customerInfo).then(function(response) {
            if (response.data.msg) {
                viewCustomerControllerScope.spinner = false;
                viewCustomerControllerScope.infoMsg = response.data.msg;
                viewCustomerControllerScope.customerList = '';
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    viewCustomerControllerScope.alertMsg = "Customer Updated Successfully"
                    viewCustomerControllerScope.showCustomerList = true;
                    viewCustomerControllerScope.customersearchKeyword = "";
                    viewCustomerControllerScope.title = "Customers"
                    getCustomerList();
                    viewCustomerControllerScope.spinner = false;
                }
            }
        }, function(err) {
            console.log("errormsg", err);
        });
    }

    function customerListView() {
        viewCustomerControllerScope.spinner = true;
        viewCustomerControllerScope.showCustomerList = true;
        viewCustomerControllerScope.customersearchKeyword = "";
        viewCustomerControllerScope.title = "Customers"
        getCustomerList();
        viewCustomerControllerScope.spinner = false;
    }

    function selectedCustomerType(customer) {
        viewCustomerControllerScope.selectCustomerType = customer;
    }

function selectedCountryCodeType(countryCode){
        viewCustomerControllerScope.selectCountryCodeType=countryCode.callingCode;
}
function selectedCountry(country){
    viewCustomerControllerScope.selectCountry=country;
    viewCustomerControllerScope.stateList=country.state;
    viewCustomerControllerScope.selectState={name:"Select State"};
}
function selectedState(state){
    viewCustomerControllerScope.selectState=state;
}
function selectedStatus(status){
    viewCustomerControllerScope.selectStatus=status;

}

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function gotoCreateCustomer() {
        $location.url('/createCustomer')
    }

}
