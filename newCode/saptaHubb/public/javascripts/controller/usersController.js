angular
    .module('ePortal')
    .controller('usersController', usersController);

usersController.$inject = ['$scope', '$location', 'usersService', 'constantsService', 'employeeService', '$rootScope', '$cookies'];

function usersController($scope, $location, usersService, constantsService, employeeService, $rootScope, $cookies) {

    var usersControllerScope = this;
    usersControllerScope.sendInvite = sendInvite;
    usersControllerScope.hideAlertPopup = hideAlertPopup;
    usersControllerScope.openInviteUserForm = openInviteUserForm;
    usersControllerScope.activatePortal = activatePortal;
    usersControllerScope.onSelect = onSelect;
    usersControllerScope.logOut = logOut;
    getEmployeeRole();
    getUserInformation();
    getInviteInformation();
    usersControllerScope.showUl = false;
    usersControllerScope.showUls = false;
    usersControllerScope.selectedRoleFun = selectedRoleFun;
    usersControllerScope.selectedInformationFun = selectedInformationFun;
    usersControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;
    usersControllerScope.selectedUser = selectedUser;


    usersControllerScope.selectEmployeeRole = { name: '' };
    $rootScope.currentMenu = "User";

    function getEmployeeByEmailOrName(employee) {
        usersControllerScope.spinner = true;
        employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
            if (response.data.msg) {
                usersControllerScope.employeeList = '';
                usersControllerScope.spinner = false;
            } else {
                if (response.data) {
                    usersControllerScope.employeeList = response.data;
                }
            }
            if (usersControllerScope.employeeList.length > 0) {
                usersControllerScope.isEmployeeFound = true;
                usersControllerScope.employeeListNotFound = false;
            } else {
                usersControllerScope.isEmployeeFound = false;
                usersControllerScope.employeeListNotFound = true;
            }

        }, function(err) {
            console.log("Employee Error");
        });
        usersControllerScope.spinner = false;
    }

    function selectedUser(user) {
        usersControllerScope.spinner = true;
        usersControllerScope.inviteUser.email = user.email[0].companyEmail;
        usersControllerScope.isEmployeeFound = false;
        usersControllerScope.spinner = false;
    }

    function selectedRoleFun(empRole) {
        usersControllerScope.showUl = !usersControllerScope.showUl;
        usersControllerScope.selectEmployeeRole = empRole;
    }

    function selectedInformationFun(userInformation, empRole, showUls) {
        usersControllerScope.showUls = false;
        userInformation.role = empRole;
        usersControllerScope.onSelect(userInformation, empRole);
    }

    function sendInvite() {
        usersControllerScope.infoMsg = "";
        usersControllerScope.spinner = true;
        usersControllerScope.inviteUser.role = usersControllerScope.selectEmployeeRole;
        usersService.addUser(usersControllerScope.inviteUser).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                usersControllerScope.alertMsg = response.data.msg;
                usersControllerScope.spinner = false;
                usersControllerScope.inviteUser = "";
                $('#inviteUserModel').modal('hide');
                usersControllerScope.inviteUserForm.$setPristine();
                usersControllerScope.inviteUserForm.$setUntouched();
            } else {
                if (response.data.successMsg) {
                    usersControllerScope.inviteUser = "";
                    usersControllerScope.selectEmployeeRole = usersControllerScope.employeeRoleList[0];
                    $scope.modalShown = true;
                    $('#inviteUserModel').modal('hide');
                    getInviteInformation();
                    usersControllerScope.inviteUserForm.$setPristine();
                    usersControllerScope.inviteUserForm.$setUntouched();
                    usersControllerScope.alertMsg = "User Invited"
                    usersControllerScope.spinner = false;
                }
            }
        }, function(err) {
            console.log(err);
        });
    }



    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function openInviteUserForm() {
        usersControllerScope.infoMsg = "";
        usersControllerScope.spinner = true;
        $('#inviteUserModel').modal('show');
        usersControllerScope.inviteUser = { role: constantsService.defultInvitedUserRole };
        usersControllerScope.spinner = false;
    }

    function getEmployeeRole() {
        usersControllerScope.infoMsg = "";
        usersControllerScope.spinner = true;
        usersService.getEmployeeRole().then(function(response) {
            if (response.data.msg) {
                usersControllerScope.spinner = false;
                usersControllerScope.infoMsg = response.data.msg;
                usersControllerScope.employeeRoleList = '';
            } else {
                if (response.data.length > 0) {
                    usersControllerScope.employeeRoleList = response.data;
                    usersControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getUserInformation() {
        usersControllerScope.infoMsg = "";
        usersControllerScope.spinner = true;
        usersService.getUserInformation().then(function(response) {
            if (response.data.msg) {
                usersControllerScope.spinner = false;
                usersControllerScope.infoMsg = response.data.msg;
                usersControllerScope.userInformationList = '';
            } else {

                if (response.data.length > 0) {
                    usersControllerScope.userInformationList = response.data;
                    usersControllerScope.selectEmployeeRole = response.data[response.data.length - 1].role;
                    usersControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }


    function getInviteInformation() {
        usersControllerScope.infoMsg = "";
        usersService.getInviteInformation().then(function(response) {
            if (response.data.msg) {
                usersControllerScope.spinner = false;
                usersControllerScope.infoMsg = response.data.msg;
                usersControllerScope.inviteInformationList = '';
            } else {

                if (response.data.length > 0) {
                    usersControllerScope.inviteInformationList = response.data;
                    usersControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }



    function onSelect(userInformation, role) {
        usersControllerScope.infoMsg = "";
        usersControllerScope.spinner = true;
        var newStatus = {
            role: role,
            id: userInformation._id,
        }

        usersService.updateUserType(newStatus).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                usersControllerScope.alertMsg = response.data.msg;
                usersControllerScope.spinner = false;
                getUserInformation();
            } else {
                if (response.data.successMsg) {
                    usersControllerScope.spinner = false;
                    getUserInformation();
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });

    }

    function activatePortal(userDetails) {
        var newStatus = {
            role: usersControllerScope.employeeRoleList[0],
            id: userDetails._id,
        }
        usersService.activatePortal(newStatus).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                usersControllerScope.alertMsg = response.data.msg;
                usersControllerScope.spinner = false;
                getUserInformation();
            } else {
                if (response.data.successMsg) {
                    usersControllerScope.spinner = false;
                    $scope.logoutmodalShown = true;
                    //getUserInformation();
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });

    }

    function logOut() {
        $rootScope.currentMenu = "";
        usersService.userLogout().then(function(response) {
            if (response.data.successMsg) {
                $cookies.remove('saptaePortalCookies');
                $location.url('/')
            }
        }, function(err) {
            console.log(err);
        })
    }
}
