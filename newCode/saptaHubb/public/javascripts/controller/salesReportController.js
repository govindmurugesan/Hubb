angular
    .module('ePortal')
    .controller('salesReportController', salesReportController);

salesReportController.$inject = ['$scope', '$location', 'filterFilter', 'salesReportService', 'employeeService', '$rootScope'];

function salesReportController($scope, $location, filterFilter, salesReportService, employeeService, $rootScope) {
    var salesReportControllerScope = this;

    salesReportControllerScope.showSalesReportList = true;
    $scope.modalShown = false;
    showFincialYearStartCalen = false;
    showFincialYearEndCalen = false;
    salesReportControllerScope.isViewSalesReport = true;

    salesReportControllerScope.editButton = false;
    salesReportControllerScope.updateButton = false;
    salesReportControllerScope.yearEndIsOpen = false;
    salesReportControllerScope.yearStartIsOpen = false;
    salesReportControllerScope.title = "Sales Report";

    var currentYear = new Date();

    salesReportControllerScope.yearStart = moment(currentYear);
    salesReportControllerScope.yearEnd = moment(currentYear.setYear(currentYear.getFullYear() + 1));

    salesReportControllerScope.hideAlertPopup = hideAlertPopup;
    salesReportControllerScope.decline = decline;
    salesReportControllerScope.openSetTargetForm = openSetTargetForm;
    salesReportControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;
    salesReportControllerScope.selectedBDM = selectedBDM;
    salesReportControllerScope.setTarget = setTarget;
    salesReportControllerScope.getYearEnd = getYearEnd;
    salesReportControllerScope.getYearStart = getYearStart;
    salesReportControllerScope.showSalesReportDetail = showSalesReportDetail;
    salesReportControllerScope.updateSalesTargetDecline = updateSalesTargetDecline;
    salesReportControllerScope.updateSalesTarget = updateSalesTarget;
    salesReportControllerScope.showEdit = showEdit;

    $rootScope.currentMenu = "Sales Report";
    currentYear = currentYear.getFullYear();

    getSalesReport(currentYear - 1, currentYear);

    function showEdit() {
        salesReportControllerScope.spinner = true;
        salesReportControllerScope.nonEditableField = false;
        salesReportControllerScope.edititemtrue = true;
        salesReportControllerScope.edititem = true;
        salesReportControllerScope.showEditButton = false;
        salesReportControllerScope.showbillList = false;
        salesReportControllerScope.editButton = false;
        salesReportControllerScope.updateButton = true;
        salesReportControllerScope.spinner = false;
    }

    function setTarget(setTargetData) {
        salesReportControllerScope.spinner = true;
        salesReportControllerScope.salesReportInfo.financialYearStart = salesReportControllerScope.salesReportInfo.financialYearStart.format('YYYY');
        salesReportControllerScope.salesReportInfo.financialYearEnd = salesReportControllerScope.salesReportInfo.financialYearEnd.format('YYYY');
        salesReportControllerScope.salesReportInfo.bdmID = salesReportControllerScope.salesReportInfo.bdmID;

        if (salesReportControllerScope.salesReportInfo.financialYearStart < salesReportControllerScope.salesReportInfo.financialYearEnd && (salesReportControllerScope.salesReportInfo.financialYearEnd - salesReportControllerScope.salesReportInfo.financialYearStart) == 1) {
            var q1 = salesReportControllerScope.salesReportInfo.q1,
                q2 = salesReportControllerScope.salesReportInfo.q2,
                q3 = salesReportControllerScope.salesReportInfo.q3,
                q4 = salesReportControllerScope.salesReportInfo.q4;
            if ((q1 == undefined || q1 == "") && (q2 == undefined || q2 == "") && (q3 == undefined || q3 == "") && (q4 == undefined || q4 == "")) {
                $scope.modalShown = true;
                salesReportControllerScope.alertMsg = "Enter Amount For Any Of Qaurter";
            } else {
                salesReportService.setTarget(salesReportControllerScope.salesReportInfo).then(function(response) {
                    if (response.data.msg) {
                        salesReportControllerScope.infoMsg = response.data.msg;
                        salesReportControllerScope.spinner = false;
                    } else {
                        if (response.data.successmsg) {
                            $('#setTarget').modal('hide');
                            salesReportControllerScope.alertMsg = "Target is set for employee " + salesReportControllerScope.salesReportInfo.bdm + " successfully";
                            getSalesReport(moment(salesReportControllerScope.yearStart).format('YYYY'), moment(salesReportControllerScope.yearEnd).format('YYYY'));
                            decline();
                            $scope.modalShown = true;
                            salesReportControllerScope.spinner = false;
                        }
                    }
                }, function(error_response) {
                    console.log("error", error_response);
                });
            }
        } else {
            $scope.modalShown = true;
            salesReportControllerScope.alertMsg = "Financial Year is invalid";
        }
        salesReportControllerScope.spinner = false;
    }

    function updateSalesTarget() {
        salesReportControllerScope.spinner = true;
        salesReportService.updateSalesTarget(salesReportControllerScope.updateTargetInfo).then(function(response) {

            if (response.data.msg) {
                $scope.modalShown = true;
                salesReportControllerScope.alertMsg = response.data.msg;
                updateSalesTargetDecline();
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    salesReportControllerScope.alertMsg = "Sales Target Updated Successfully For " + salesReportControllerScope.updateTargetInfo.bdm;
                    salesReportControllerScope.isViewSalesReport = true;
                    salesReportControllerScope.editButton = false;
                    salesReportControllerScope.updateButton = false;
                    salesReportControllerScope.title = "Sales Report";
                    getSalesReport(currentYear - 1, currentYear);
                }

            }
        });
        salesReportControllerScope.spinner = false;
    }

    function getSalesReport(financialYearStart, financialYearEnd) {

        salesReportControllerScope.spinner = true;
        salesReportService.getSalesReport(financialYearStart, financialYearEnd).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                salesReportControllerScope.alertMsg = response.data.msg;
                salesReportControllerScope.salesReportList = '';
                salesReportControllerScope.spinner = false;
            } else {
                if (response.data) {
                    salesReportControllerScope.salesReportList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = salesReportControllerScope.salesReportList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                    $scope.valueToCheck;
                    $scope.$watch('search', function(newVal, oldVal) {
                        $scope.filtered = filterFilter(salesReportControllerScope.salesReportList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                }
                salesReportControllerScope.spinner = false;
            }

        }, function(err) {
            console.log("Sales Error");
        });
        salesReportControllerScope.spinner = false;
    }

    function getEmployeeByEmailOrName(employee) {
        salesReportControllerScope.spinner = true;
        employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
            if (response.data.msg) {
                salesReportControllerScope.employeeList = '';
                salesReportControllerScope.spinner = false;
            } else {
                if (response.data) {
                    salesReportControllerScope.employeeList = response.data;
                }
            }
            if (salesReportControllerScope.employeeList.length > 0) {
                salesReportControllerScope.isEmployeeFound = true;
                salesReportControllerScope.employeeListNotFound = false;
            } else {
                salesReportControllerScope.isEmployeeFound = false;
                salesReportControllerScope.employeeListNotFound = true;
            }

        }, function(err) {
            console.log("Sales Error");
        });
        salesReportControllerScope.spinner = false;
    }

    function selectedBDM(employee) {
        salesReportControllerScope.spinner = true;
        if (salesReportControllerScope.salesReportInfo.financialYearStart && salesReportControllerScope.salesReportInfo.financialYearEnd) {
            var yearStart = salesReportControllerScope.salesReportInfo.financialYearStart.format('YYYY'),
                yearEnd = salesReportControllerScope.salesReportInfo.financialYearEnd.format('YYYY');
            salesReportControllerScope.salesReportInfo.bdm = employee.firstName + ' ' + employee.middleName + ' ' + employee.lastName;
            salesReportControllerScope.salesReportInfo.bdmID = employee._id;
            salesReportService.checkBDMSalesRecord(yearStart, yearEnd, employee._id).then(function(response) {

                if (response.data.msg) {} else {
                    if (response.data) {
                        $scope.modalShown = true;
                        salesReportControllerScope.alertMsg = "Target has aleady set to " + salesReportControllerScope.salesReportInfo.bdm + ' for ' + yearStart + '-' + yearEnd + ' financial year';
                        decline();
                    }
                }
            });
            salesReportControllerScope.isEmployeeFound = false;
        } else {
            $scope.modalShown = true;
            salesReportControllerScope.alertMsg = "Select financial year";
            salesReportControllerScope.salesReportInfo.bdm = "";
            salesReportControllerScope.isEmployeeFound = false;
        }
        salesReportControllerScope.spinner = false;
    }

    function getYearStart(yearStart) {
        salesReportControllerScope.spinner = true;
        if (salesReportControllerScope.yearStart < salesReportControllerScope.yearEnd)
            getSalesReport(moment(yearStart).format('YYYY'), moment(salesReportControllerScope.yearEnd).format('YYYY'));
        else {
            $scope.modalShown = true;
            salesReportControllerScope.alertMsg = "Financial Year is invalid";
            salesReportControllerScope.salesReportList = '';
            salesReportControllerScope.spinner = false;
        }
    }

    function getYearEnd(yearEnd) {
        salesReportControllerScope.spinner = true;
        if (salesReportControllerScope.yearStart < salesReportControllerScope.yearEnd)
            getSalesReport(moment(salesReportControllerScope.yearStart).format('YYYY'), moment(yearEnd).format('YYYY'));
        else {
            $scope.modalShown = true;
            salesReportControllerScope.alertMsg = "Financial Year is invalid";
            salesReportControllerScope.salesReportList = '';
        }

        salesReportControllerScope.spinner = false;
    }

    function openSetTargetForm() {
        $('#setTarget').modal('show');
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function decline() {
        salesReportControllerScope.spinner = true;
        salesReportControllerScope.salesReportInfo = "";
        salesReportControllerScope.setTargetForm.$setPristine();
        salesReportControllerScope.setTargetForm.$setUntouched();
        $('#setTarget').modal('hide');
        salesReportControllerScope.spinner = false;
    }

    function updateSalesTargetDecline() {
        salesReportControllerScope.spinner = true;
        salesReportControllerScope.updateTargetInfo = "";
        salesReportControllerScope.updateTargetForm.$setPristine();
        salesReportControllerScope.updateTargetForm.$setUntouched();
        salesReportControllerScope.isViewSalesReport = true;
        salesReportControllerScope.title = "Sales Report";
        salesReportControllerScope.editButton = false;
        salesReportControllerScope.updateButton = false;
        salesReportControllerScope.spinner = false;
    }

    function showSalesReportDetail(salesReportInfo) {
        salesReportControllerScope.spinner = true;
        salesReportControllerScope.isViewSalesReport = false;
        salesReportControllerScope.updateTargetInfo = salesReportInfo;
        salesReportControllerScope.title = "Target Detail";
        salesReportControllerScope.showEditButton = true;
        salesReportControllerScope.showBillList = false;
        salesReportControllerScope.nonEditableField = true;
        salesReportControllerScope.edititemtrue = true;
        salesReportControllerScope.edititem = false;
        salesReportControllerScope.showbillList = false;
        salesReportControllerScope.editButton = true;
        salesReportControllerScope.updateButton = false;
        salesReportControllerScope.spinner = false;
    }

}
