angular
    .module('ePortal')
    .controller('projectCostController', projectCostController);

projectCostController.$inject = ['$scope', '$location', 'projectService', 'employeeService', 'filterFilter', 'configrationService', '$rootScope'];

function projectCostController($scope, $location, projectService, employeeService, filterFilter, configrationService, $rootScope) {
    var projectCostControllerScope = this;
    var totBackUp = 0;
    projectCostControllerScope.totalPaid = 0;
    projectCostControllerScope.spinner = false;
    projectCostControllerScope.isAddProjectCost = false;
    projectCostControllerScope.isEditable = true;
    projectCostControllerScope.isShowProjectCost = true;

    projectCostControllerScope.decline = decline;
    projectCostControllerScope.hideAlertPopup = hideAlertPopup;
    projectCostControllerScope.getProject = getProject;
    projectCostControllerScope.selectedProjectFun = selectedProjectFun;
    projectCostControllerScope.selectedBDMFun = selectedBDMFun;
    projectCostControllerScope.selectedProjectManagerFun = selectedProjectManagerFun;
    projectCostControllerScope.addNewPaymentTerm = addNewPaymentTerm;
    projectCostControllerScope.changePaymentTerms = changePaymentTerms;
    projectCostControllerScope.removePaymentTerm = removePaymentTerm;
    projectCostControllerScope.addProjectCost = addProjectCost;
    projectCostControllerScope.checkTotal = checkTotal;
    projectCostControllerScope.showAddProjectCost = showAddProjectCost;
    projectCostControllerScope.showProjectList = showProjectList;
    projectCostControllerScope.updateProjectCost = updateProjectCost;
    projectCostControllerScope.showProjectDetail = showProjectDetail;
    projectCostControllerScope.declineEdit = declineEdit;
    projectCostControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;

    $rootScope.currentMenu = "Project Cost Detail";
    projectCostControllerScope.title = "Project Cost";
    projectCostControllerScope.selectedProject = { projectName: "Select Project" };
    projectCostControllerScope.employeeList = '';
    projectCostControllerScope.projectCost = { BDMName: '' };
    // projectCostControllerScope.selectedBDM = { name: "Select BDM" };
    // projectCostControllerScope.selectedProjectManager = { name: "Select Project Manager" };
    projectCostControllerScope.projectCost = { paymentValueType: '' };

    getProject();
    viewEmployee();
    getPaymentTerm();

    $scope.paymentTermsElement = [{
        paymentTerm: 'Select Term',
        paymentTermValue: ''
    }];

    // function selectedRepoting(reportingTo) {
    //     projectCostControllerScope.spinner = true;
    //     projectCostControllerScope.empInfo.reportingTo = {
    //         reportingEmployeeId: reportingTo._id,
    //         reportingEmployeeName: reportingTo.firstName + ' ' + reportingTo.middleName + ' ' + reportingTo.lastName
    //     };
    //     projectCostControllerScope.empInfo.reporting = projectCostControllerScope.empInfo.reportingTo.reportingEmployeeName;

    //     projectCostControllerScope.isEmployeeFound = false;
    //     projectCostControllerScope.spinner = false;
    // }

    function getEmployeeByEmailOrName(employee, isBDM) {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.employeeList = '';
        employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.spinner = false;
                projectCostControllerScope.employeeList = '';
            } else {
                if (response.data) {
                    projectCostControllerScope.employeeList = response.data;
                }
            }
            if (projectCostControllerScope.employeeList.length > 0 && isBDM) {

                projectCostControllerScope.isEmployeeFoundForBdm = true;
                projectCostControllerScope.isEmployeeFoundForManager = false;
                projectCostControllerScope.employeeListNotFound = false;

            } else if (projectCostControllerScope.employeeList.length > 0 && !isBDM) {
                projectCostControllerScope.isEmployeeFoundForManager = true;
                projectCostControllerScope.isEmployeeFoundForBdm = false;
            }
        }, function(err) {});
        if (projectCostControllerScope.employeeList.length == 0 || projectCostControllerScope.employeeList == '') {
            projectCostControllerScope.isEmployeeFoundForManager = false;
            projectCostControllerScope.isEmployeeFoundForBdm = false;
            if (isBDM)
                projectCostControllerScope.employeeListNotFound = true;
            if (!isBDM)
                projectCostControllerScope.employeeListNotFoundForManager = true;
        }
        projectCostControllerScope.spinner = false;
    }

    function showProjectDetail(project) {

        projectCostControllerScope.spinner = true;
        projectCostControllerScope.projectInfo = '';
        projectCostControllerScope.isAddProjectCost = false;
        projectCostControllerScope.isShowProjectCost = false;
        projectCostControllerScope.isUpdateProjectCost = true;
        projectCostControllerScope.title = "Project Cost Detail";
        if (projectCostControllerScope.isUpdateProjectCost)
            projectCostControllerScope.projectInfo = project;
        if (project.bdm) {
            var projectManager = project.projectManager;
            var BDM = project.bdm;
            projectCostControllerScope.projectInfo.comment = project.projectCostComment[project.projectCostComment.length - 1].comment;
            projectCostControllerScope.projectInfo.projectManager = projectManager;
            projectCostControllerScope.projectInfo.projectManagerName = projectManager.firstName + ' ' + projectManager.middleName + ' ' + projectManager.lastName;
            projectCostControllerScope.projectInfo.bdm = BDM;
            projectCostControllerScope.projectInfo.BDMName = BDM.firstName + ' ' + BDM.middleName + ' ' + BDM.lastName;
            projectCostControllerScope.projectInfo.projectBDMName = project.bdm.firstName + ' ' + project.bdm.middleName + ' ' + project.bdm.lastName;
            projectCostControllerScope.projectInfo.projectManagerName = project.projectManager.firstName + ' ' + project.projectManager.middleName + ' ' + project.projectManager.lastName;
            $scope.paymentTermsElementData = project.payment;
        }
        projectCostControllerScope.spinner = false;
    }

    function declineEdit() {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.title = "Projects Cost";
        showProjectList();
        projectCostControllerScope.spinner = false;
    }

    function showProjectList() {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.isAddProjectCost = false;
        projectCostControllerScope.isShowProjectCost = true;
        projectCostControllerScope.isUpdateProjectCost = false;
        projectCostControllerScope.title = "Projects Cost";
        // decline();
        projectCostControllerScope.spinner = false;
    }

    function showAddProjectCost() {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.isAddProjectCost = true;
        projectCostControllerScope.isShowProjectCost = false;
        projectCostControllerScope.isUpdateProjectCost = false;
        projectCostControllerScope.isPaymentTermsElementData = false;
        projectCostControllerScope.totalPaid = 0;
        projectCostControllerScope.selectedProject = { projectName: "Select Project" };
        projectCostControllerScope.selectedBDM = { name: "Select BDM" };
        // projectCostControllerScope.selectedProjectManager = { name: "Select Project Manager" };
        projectCostControllerScope.projectCost = { paymentValueType: '' };
        projectCostControllerScope.projectCost = "";
        projectCostControllerScope.createProjectCostForm.$setPristine();
        projectCostControllerScope.createProjectCostForm.$setUntouched();
        projectCostControllerScope.title = "Project Cost Detail";
        projectCostControllerScope.spinner = false;
    }

    function getPaymentTerm(argument) {
        projectCostControllerScope.spinner = true;
        configrationService.getPaymentTerm().then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.infoMsg = response.data.msg;
                projectCostControllerScope.paymentTermList = '';
            } else {
                if (response.data.length > 0) {
                    projectCostControllerScope.paymentTermList = response.data;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
        projectCostControllerScope.spinner = false;
    }

    function checkTotal(val) {
        projectCostControllerScope.spinner = true;
        if (projectCostControllerScope.projectCost.projectCost == undefined || projectCostControllerScope.projectCost.projectCost == '') {
            $scope.modalShown = true;
            projectCostControllerScope.alertMsg = "Please Enter Project Cost";
            angular.forEach($scope.paymentTermsElement, function(ele) {
                ele.paymentTermValue = "";
            });
        } else {
            var tot = 0;
            if (val) {
                tot = totBackUp;
                angular.forEach($scope.paymentTermsElement, function(ele) {
                    if (ele.paymentTerm == "Select Term" && projectCostControllerScope.isAddProjectCost) {
                        ele.paymentTermValue = "";
                        $scope.modalShown = true;
                        projectCostControllerScope.alertMsg = "Select Payment Term";
                        if (projectCostControllerScope.isUpdateProjectCost)
                            projectCostControllerScope.updateProjectCostForm.$setDirty();
                        else
                            projectCostControllerScope.createProjectCostForm.$setDirty();
                    } else {
                        var projectCost;
                        if (projectCostControllerScope.isUpdateProjectCost)
                            projectCost = projectCostControllerScope.projectInfo;
                        else
                            projectCost = projectCostControllerScope.projectCost;
                        projectCostControllerScope.createProjectCostForm.$dirty = true;
                        if (projectCost) {
                            if (projectCost.paymentValueType == "INR") {
                                tot += ele.paymentTermValue;
                            } else if (projectCost.paymentValueType == "%") {
                                tot += (projectCost.projectCost * ele.paymentTermValue) / 100;
                            }
                        }
                        if (tot > projectCostControllerScope.projectCost.projectCost) {
                            $scope.modalShown = true;
                            ele.paymentTermValue = "";
                            projectCostControllerScope.alertMsg = "Amount Can not be greater than project cost";
                            projectCostControllerScope.createProjectCostForm.$setDirty();
                        }

                    }

                });
                projectCostControllerScope.totalPaid = "Total : " + tot * 1.0;
                if (tot > 0)
                    projectCostControllerScope.isPaymentTermsElementData = true;
            } else {
                if (tot == 0)
                    projectCostControllerScope.isPaymentTermsElementData = false;

            }
        }
        projectCostControllerScope.spinner = false;
    }

    function addNewPaymentTerm() {
        projectCostControllerScope.spinner = true;
        var status = true;
        angular.forEach($scope.paymentTermsElement, function(ele) {
            if (ele.paymentTermValue == "") {
                status = false;
            }
        });
        if (status)
            $scope.paymentTermsElement.push({ paymentTerm: 'Select Term', paymentTermValue: '' });
        else {
            $scope.modalShown = true;
            projectCostControllerScope.alertMsg = "Please Enter Payment Term Value";
        }

        projectCostControllerScope.spinner = false;
    }

    function removePaymentTerm(index, val) {
        projectCostControllerScope.spinner = true;
        if (projectCostControllerScope.isAddProjectCost) {
            $scope.paymentTermsElement.splice(index, 1);
            checkTotal(val);
        } else {
            $scope.paymentTermsElementData.splice(index, 1);
            // projectService.deletePaymentTermByIndex($scope.paymentTermsElementData[index]._id, projectCostControllerScope.selectedProject._id);
            // selectedProjectFun(projectCostControllerScope.selectedProject);
        }

        projectCostControllerScope.spinner = false;
    }

    function changePaymentTerms(value) {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.projectCost.paymentValueType = angular.copy(value);

        angular.forEach($scope.paymentTermsElement, function(ele) {
            ele.paymentTermValue = "";
        });
        totBackUp = 0;
        projectCostControllerScope.isPaymentTermsElementData = false;
        projectCostControllerScope.isEditable = false;
        projectCostControllerScope.spinner = false;
    }

    function selectedProjectFun(project) {
        projectCostControllerScope.spinner = true;
        // decline();
        projectCostControllerScope.selectedProject = project;
        if (!projectCostControllerScope.isAddProjectCost)
            projectCostControllerScope.projectCost.project = project;
        // projectService.getProjectByReference(project._id).then(function(response) {
        //     if (response.data.msg) {
        //         projectCostControllerScope.infoMsg = response.data.msg;
        //         projectCostControllerScope.projectList = '';
        //     } else {
        //         if (response.data.bdm) {
        //             $scope.paymentTermsElementData = [];
        //             selectedBDMFun(response.data.bdm);
        //             selectedProjectManagerFun(response.data.projectManager);
        //             projectCostControllerScope.projectCost.projectBDMName = response.data.bdm.firstName + ' ' + response.data.bdm.middleName + ' ' + response.data.bdm.lastName;
        //             projectCostControllerScope.projectCost.projectManagerName = response.data.projectManager.firstName + ' ' + response.data.projectManager.middleName + ' ' + response.data.projectManager.lastName;
        //             projectCostControllerScope.totalPaid = 0;
        //             projectCostControllerScope.projectCost = response.data;
        //             angular.forEach(response.data.payment, function(ele, key) {
        //                 $scope.paymentTermsElementData[key] = ele;
        //                 if (ele.paymentValueType == "%") {
        //                     projectCostControllerScope.totalPaid += (projectCostControllerScope.projectCost.projectCost * ele.paymentTermValue) / 100;
        //                 } else
        //                     projectCostControllerScope.totalPaid += ele.paymentTermValue;
        //             });
        //             totBackUp = angular.copy(projectCostControllerScope.totalPaid);
        //             projectCostControllerScope.totalPaid = " Total : " + projectCostControllerScope.totalPaid;
        //             projectCostControllerScope.projectCost.comment = response.data.projectCostComment[response.data.projectCostComment.length - 1].comment;
        //             projectCostControllerScope.isPaymentTermsElementData = true;
        //         } else
        //             projectCostControllerScope.isPaymentTermsElementData = false;

        //     }
        // });
        projectCostControllerScope.spinner = false;
    }

    function selectedBDMFun(BDM) {
        projectCostControllerScope.spinner = true;
        if (BDM) {

            if (projectCostControllerScope.isUpdateProjectCost) {
                projectCostControllerScope.projectInfo.bdm = BDM;
                projectCostControllerScope.projectInfo.BDMName = BDM.firstName + ' ' + BDM.middleName + ' ' + BDM.lastName;
            }
            if (projectCostControllerScope.isAddProjectCost) {
                projectCostControllerScope.projectCost.bdm = BDM;
                projectCostControllerScope.projectCost.BDMName = BDM.firstName + ' ' + BDM.middleName + ' ' + BDM.lastName;
            }
        }
        projectCostControllerScope.isEmployeeFoundForManager = false;
        projectCostControllerScope.isEmployeeFoundForBdm = false;
        projectCostControllerScope.employeeListNotFound = false;
        projectCostControllerScope.spinner = false;
    }

    function selectedProjectManagerFun(projectManager) {
        projectCostControllerScope.spinner = true;
        if (projectManager) {
            if (projectCostControllerScope.isUpdateProjectCost) {
                projectCostControllerScope.projectInfo.projectManager = projectManager;
                projectCostControllerScope.projectInfo.projectManagerName = projectManager.firstName + ' ' + projectManager.middleName + ' ' + projectManager.lastName;
            }
            if (projectCostControllerScope.isAddProjectCost) {
                projectCostControllerScope.projectCost.projectManager = projectManager;
                projectCostControllerScope.projectCost.projectManagerName = projectManager.firstName + ' ' + projectManager.middleName + ' ' + projectManager.lastName;
            }
        }
        projectCostControllerScope.isEmployeeFoundForManager = false;
        projectCostControllerScope.isEmployeeFoundForBdm = false;
        projectCostControllerScope.employeeListNotFoundForManager = false;
        projectCostControllerScope.spinner = false;
    }

    function addProjectCost() {
        projectCostControllerScope.spinner = true;
        projectCostControllerScope.projectCost.payment = $scope.paymentTermsElement;
        angular.forEach($scope.paymentTermsElement, function(ele, key) {
            projectCostControllerScope.projectCost.payment[key].paymentValueType = projectCostControllerScope.projectCost.paymentValueType;
        });
        projectCostControllerScope.projectCost.project = projectCostControllerScope.selectedProject;
        projectService.addProjectCost(projectCostControllerScope.projectCost).then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.infoMsg = response.data.msg;
                projectCostControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    projectCostControllerScope.alertMsg = "Project Cost added successfully";
                    totBackUp = 0;
                    projectCostControllerScope.isPaymentTermsElementData = false;
                    projectCostControllerScope.isEditable = false;
                    getProject();
                    decline();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });


        projectCostControllerScope.spinner = false;
    }

    function updateProjectCost() {
        projectCostControllerScope.spinner = true;

        projectCostControllerScope.projectCost.payment = $scope.paymentTermsElementData;
        // angular.forEach($scope.paymentTermsElementData, function(ele, key) {
        //     projectCostControllerScope.projectCost.payment[key].paymentValueType = projectCostControllerScope.projectCost.paymentValueType;
        // });
        projectCostControllerScope.projectCost.project = projectCostControllerScope.selectedProject;
        projectCostControllerScope.projectCost.bdm = projectCostControllerScope.selectedBDM;
        projectCostControllerScope.projectCost.projectManager = projectCostControllerScope.selectedProjectManager;
        projectCostControllerScope.projectCost.projectCostComment = projectCostControllerScope.comment;
        projectService.updateProjectCost(projectCostControllerScope.projectInfo).then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.infoMsg = response.data.msg;
                projectCostControllerScope.spinner = false;
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    projectCostControllerScope.alertMsg = "Project Updated added successfully";
                    getProject();
                    showProjectList();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
        projectCostControllerScope.spinner = false;
    }

    function viewEmployee() {
        projectCostControllerScope.spinner = true;
        employeeService.viewEmployee().then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.spinner = false;
                projectCostControllerScope.empList = '';
            } else {
                projectCostControllerScope.empList = response.data;
                projectCostControllerScope.spinner = false;
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
    }


    function getProject() {
        projectCostControllerScope.spinner = true;
        projectService.getProject().then(function(response) {
            if (response.data.msg) {
                projectCostControllerScope.infoMsg = response.data.msg;
                projectCostControllerScope.projectList = '';
            } else {
                projectCostControllerScope.projectList = response.data;
                $scope.currentPage = 1;
                $scope.totalItems = projectCostControllerScope.projectList.length;
                $scope.entryLimit = 10;
                $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                $scope.valueToCheck;
                $scope.$watch('search', function(newVal, oldVal) {
                    $scope.filtered = filterFilter(projectCostControllerScope.projectList, newVal);
                    $scope.totalItems = $scope.filtered.length;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                    $scope.currentPage = 1;
                    // $scope.$apply(function() {
                    //     $scope.valueToCheck = newVal;
                    // });
                }, true);
            }
        });
        projectCostControllerScope.spinner = false;
    }



    function decline() {

        projectCostControllerScope.spinner = true;
        // getProject();
        // $scope.paymentTermsElement = [{
        //     paymentTerm: 'Select Term',
        //     paymentTermValue: ''
        // }];
        // projectCostControllerScope.isPaymentTermsElementData = false;
        // projectCostControllerScope.totalPaid = 0;
        // $scope.paymentTermsElementData = [];
        // projectCostControllerScope.selectedProject = { projectName: "Select Project" };
        // projectCostControllerScope.selectedBDM = { name: "Select BDM" };
        // projectCostControllerScope.selectedProjectManager = { name: "Select Project Manager" };
        // projectCostControllerScope.projectCost = { paymentValueType: '' };
        // projectCostControllerScope.projectCost = "";

        // if (projectCostControllerScope.isAddProjectCost) {
        //     projectCostControllerScope.createProjectCostForm.$setPristine();
        //     projectCostControllerScope.createProjectCostForm.$setUntouched();
        // } else {
        //     projectCostControllerScope.updateProjectCostForm.$setPristine();
        //     projectCostControllerScope.updateProjectCostForm.$setUntouched();
        // }
        // showProjectList();

        projectCostControllerScope.isAddProjectCost = false;
        projectCostControllerScope.isShowProjectCost = true;
        projectCostControllerScope.isUpdateProjectCost = false;
        projectCostControllerScope.spinner = false;
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        // $location.path('/viewLead');
    }


}
