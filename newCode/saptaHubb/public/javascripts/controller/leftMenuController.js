angular
    .module('ePortal')
    .controller('leftMenuController', leftMenuController);

leftMenuController.$inject = ['$scope', '$location', 'configrationService', 'menusService', '$rootScope', '$cookies', 'usersService', 'constantsService'];

function leftMenuController($scope, $location, configrationService, menusService, $rootScope, $cookies, usersService, constantsService) {
    var leftMenuControllerScope = this;

    leftMenuControllerScope.showMenu = false;
    leftMenuControllerScope.superAdminMenus = false

    leftMenuControllerScope.leftMenuList = '';
    leftMenuControllerScope.getMenuList = getMenuList;
    leftMenuControllerScope.subMenuNavigation = subMenuNavigation;
    getMenuList();


    function getMenuList() {
        leftMenuControllerScope.errMsg = "";
        var authorizedUser = false;

        var n = $location.url().slice(1).indexOf('/');
        var browserUrl = $location.url().slice(1).substring(0, n != -1 ? n : $location.url().slice(1).length);

        menusService.getAssignedRoles($cookies.getObject('saptaePortalCookies').role._id).then(function(response) {
            if (response.data.msg) {
                if (constantsService.superAdmin == $cookies.getObject('saptaePortalCookies').role.name) {

                    menusService.getMenus().then(function(response) {
                        if (response.data.msg) {

                        } else {
                            if (response.data) {
                                leftMenuControllerScope.superAdminMenus = true;
                                leftMenuControllerScope.leftMenuList = response.data;
                                
                            }
                        }

                    }, function(err) {});


                } else {

                    leftMenuControllerScope.errMsg = response.data.msg;

                }

            } else {

                if (response.data) {
                    if (response.data[0].assignedMenus) {
                        leftMenuControllerScope.leftMenuList = response.data[0].assignedMenus;
                        leftMenuControllerScope.superAdminMenus = false;
                        outer_loop: for (var i = 0; i < leftMenuControllerScope.leftMenuList.length; i++) {

                            if (leftMenuControllerScope.leftMenuList[i].url == browserUrl) {
                                if (leftMenuControllerScope.leftMenuList[i].isChecked) {
                                    authorizedUser = true;
                                    break outer_loop;
                                } else {
                                    authorizedUser = false;
                                    break outer_loop;
                                }
                            }
                           if (leftMenuControllerScope.leftMenuList[i].submenu!=undefined) {
                                for (var j = 0; j < leftMenuControllerScope.leftMenuList[i].submenu.length; j++) {

                                    if (leftMenuControllerScope.leftMenuList[i].submenu[j].url == browserUrl) {
                                        if (leftMenuControllerScope.leftMenuList[i].submenu[j].isChecked) {
                                            authorizedUser = true;
                                            break outer_loop;
                                        } else {
                                            authorizedUser = false;
                                            break outer_loop;
                                        }

                                    }
                                }
                            }

                        }

                        if (!authorizedUser) {
                            $rootScope.currentMenu = "";
                            usersService.userLogout().then(function(response) {
                                if (response.data.successMsg) {
                                    $cookies.remove('saptaePortalCookies');
                                    $location.url('/')
                                }
                            }, function(err) {
                                console.log(err);
                            })
                        }
                    } else {}
                 // todo show if it isChecked and sending to dashboard controller
                 if(leftMenuControllerScope.leftMenuList[0].submenu!=undefined){
                 var SalesReport = leftMenuControllerScope.leftMenuList[0].submenu[0].isChecked;
                           if(SalesReport == true) {
                             $rootScope.$broadcast('SalesReport');
                            }
                             else {      
                            }
                 var FinancialReport = leftMenuControllerScope.leftMenuList[0].submenu[1].isChecked;
                           if(FinancialReport == true) {
                             $rootScope.$broadcast('FinancialReport');
                            }
                             else {      
                            }
                    
                    var todo = leftMenuControllerScope.leftMenuList[0].submenu[2].isChecked;
                            if(todo == true) {
                             $rootScope.$broadcast('todo');
                            }
                             else {      
                            }
                    var dashBoard = leftMenuControllerScope.leftMenuList[0].submenu;
                    dashBoard.splice(0,3); 
                    } 
                 // end                

                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });

    }

    if ($(window).width() < 768) {
        leftMenuControllerScope.showMenu = true;
    }


    function subMenuNavigation(menuType, menuName) {
        var sample = 'country';
        if (menuType) {
            if (menuType == 'configuration') {
                $location.path('/' + menuType + '/' + sample);
            } else {
                $location.path('/' + menuType);
            }
        } else {

        }

    }

    $scope.hoverIn = function() {
        this.hoverEdit = true;
    };

    $scope.hoverOut = function() {
        this.hoverEdit = false;
    };



    $rootScope.$on("CallLeftSideMenuMethod", function() {
        leftMenuControllerScope.showMenu = !leftMenuControllerScope.showMenu;

    });


}
