angular
    .module('ePortal')
    .controller('rolesController', rolesController);

rolesController.$inject = ['$scope', '$location', 'configrationService', 'menusService', '$cookies', '$rootScope'];

function rolesController($scope, $location, configrationService, menusService, $cookies, $rootScope) {
    var rolesControllerScope = this;

    rolesControllerScope.editRoleDetails = editRoleDetails;
    rolesControllerScope.moveToRoleType = moveToRoleType;
    rolesControllerScope.chageEventForRole = chageEventForRole;
    rolesControllerScope.changeEventForParentCheckbox = changeEventForParentCheckbox;
    rolesControllerScope.changeEventForChildCheckbox = changeEventForChildCheckbox;
    rolesControllerScope.hideAlertPopup = hideAlertPopup;
    rolesControllerScope.decline = decline;
    rolesControllerScope.selectedRoleFun = selectedRoleFun;


    rolesControllerScope.updateDetails = updateDetails;
    rolesControllerScope.editRole = true;
    rolesControllerScope.spinner = false;
    rolesControllerScope.showrole = false;
    $rootScope.currentMenu = "Roles";
    rolesControllerScope.selectedRole = { name: "Select Role" };

    getMenuList();
    getEmpRoleList();
    var menuBackup;

    function selectedRoleFun(roles) {
        rolesControllerScope.spinner = true;
        rolesControllerScope.showrole = !rolesControllerScope.showrole;
        rolesControllerScope.selectedRole = roles;
        getRoleDetails();
    }


    function chageEventForRole() {
        rolesControllerScope.spinner = true;
        getRoleDetails();
    }

    function changeEventForParentCheckbox(subMenuDetails, parentIndex, checkedStatus) {
        if (subMenuDetails) {
            for (var i = 0; i < subMenuDetails.length; i++) {
                rolesControllerScope.leftMenuList[parentIndex].submenu[i].isChecked = checkedStatus;
            }
        }
    }

    function changeEventForChildCheckbox(parentIndex, checkedStatus) {
        if (parentIndex) {
            var checkedMenus = false;
            for (var i = 0; i < rolesControllerScope.leftMenuList[parentIndex].submenu.length; i++) {
                if (rolesControllerScope.leftMenuList[parentIndex].submenu[i].isChecked) {
                    checkedMenus = true;
                }
            }
            if (checkedMenus) rolesControllerScope.leftMenuList[parentIndex].isChecked = true;
            else rolesControllerScope.leftMenuList[parentIndex].isChecked = false;
        }

    }

    function getMenuList() {
        rolesControllerScope.spinner = true;
        menusService.getMenus().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                rolesControllerScope.alertMsg = response.data.msg;
                rolesControllerScope.spinner = false;
            } else {
                if (response.data) {
                    menuBackup = angular.copy(response.data);
                    rolesControllerScope.leftMenuList = angular.copy(response.data);
                    rolesControllerScope.spinner = false;
                    var toDo = rolesControllerScope.leftMenuList[0].submenu[2];

                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function getEmpRoleList() {
        rolesControllerScope.spinner = true;
        menusService.getEmpRole().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                rolesControllerScope.alertMsg = response.data.msg;
                rolesControllerScope.spinner = false;
            } else {
                if (response.data) {
                    rolesControllerScope.empRoleList = response.data
                    for (var i = 0; i < rolesControllerScope.empRoleList.length; i++) {
                        if (rolesControllerScope.empRoleList[i]._id == $cookies.getObject('saptaePortalCookies').role._id) rolesControllerScope.selectedRole = rolesControllerScope.empRoleList[i];
                    }
                    getRoleDetails();
                    rolesControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function editRoleDetails() {
        rolesControllerScope.editRole = false;
    }

    function moveToRoleType() {
        $location.path('/configuration/employee_Role');
    }

    function updateDetails() {

        // updateInfo.roleName = rolesControllerScope.selectedRole
        rolesControllerScope.spinner = true;
        if (rolesControllerScope.leftMenuList.referenceId) {
            var updateInfo = {
                role_refid: rolesControllerScope.selectedRole._id,
                roleName: rolesControllerScope.selectedRole.name,
                assignedMenus: rolesControllerScope.leftMenuList,
                referenceId: rolesControllerScope.leftMenuList.referenceId,
                description: rolesControllerScope.leftMenuList.description
            }
        } else {
            var updateInfo = {
                role_refid: rolesControllerScope.selectedRole._id,
                roleName: rolesControllerScope.selectedRole.name,
                assignedMenus: rolesControllerScope.leftMenuList,
                description: rolesControllerScope.leftMenuList.description
            }
        }

        menusService.updateAssignedRoles(updateInfo).then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                rolesControllerScope.alertMsg = response.data.msg;
                rolesControllerScope.spinner = false;
            } else {
                rolesControllerScope.editRole = true;
                rolesControllerScope.spinner = false;
                getRoleDetails();
            }

        }, function(err) {

        });
    }

    function getRoleDetails() {
        rolesControllerScope.spinner = true;
        menusService.getAssignedRoles(rolesControllerScope.selectedRole._id).then(function(response) {
            if (response.data.msg) {
                rolesControllerScope.leftMenuList = "";
                rolesControllerScope.leftMenuList = angular.copy(menuBackup);
                rolesControllerScope.leftMenuList.roleName = rolesControllerScope.selectedRole.name;
                $scope.modalShown = true;
                rolesControllerScope.alertMsg = response.data.msg;
                rolesControllerScope.spinner = false;
            } else {
                if (response.data) {
                    rolesControllerScope.leftMenuList.referenceId = response.data[0]._id;
                    rolesControllerScope.leftMenuList.roleName = response.data[0].roleName;
                    rolesControllerScope.leftMenuList.description = response.data[0].description;

                    for (var i = 0; i < rolesControllerScope.leftMenuList.length; i++) {
                        if (response.data[0].assignedMenus[i]) {
                            if (response.data[0].assignedMenus[i].isChecked) rolesControllerScope.leftMenuList[i].isChecked = true;
                            else rolesControllerScope.leftMenuList[i].isChecked = false;
                            if (rolesControllerScope.leftMenuList[i].submenu!=undefined) {

                                for (var j = 0; j < rolesControllerScope.leftMenuList[i].submenu.length; j++) {
                                   if(response.data[0].assignedMenus[i].submenu!=undefined)
                                    if (response.data[0].assignedMenus[i].submenu[j]) {
                                        if (response.data[0].assignedMenus[i].submenu[j].isChecked) rolesControllerScope.leftMenuList[i].submenu[j].isChecked = true;
                                        else rolesControllerScope.leftMenuList[i].submenu[j].isChecked = false;
                                    }
                                }
                            }
                        }

                    }
                    rolesControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function decline() {
        rolesControllerScope.editRole = true;
        getRoleDetails();
    }

}
