angular
    .module('ePortal')
    .controller('viewProjectController', viewProjectController);

viewProjectController.$inject = ['$scope', '$location', 'configrationService', 'projectService', 'filterFilter', '$filter', '$rootScope', 'customerService', 'constantsService'];

function viewProjectController($scope, $location, configrationService, projectService, filterFilter, $filter, $rootScope, customerService, constantsService) {

    var viewProjectControllerScope = this;

    viewProjectControllerScope.spinner = false;
    viewProjectControllerScope.showProjectList = true;
    viewProjectControllerScope.startDateCalender = false;
    viewProjectControllerScope.endDateCalender = false;
    viewProjectControllerScope.showStatusUl = false;
    viewProjectControllerScope.showCustomerUl = false;

    viewProjectControllerScope.activeState = constantsService.activeState;
    viewProjectControllerScope.inactiveState = constantsService.inactiveState;

    viewProjectControllerScope.projectsearchKeyword = "";
    viewProjectControllerScope.title = "Projects"
    getProject();

    viewProjectControllerScope.gotoNewProject = gotoNewProject;
    viewProjectControllerScope.showProjectDetail = showProjectDetail;
    viewProjectControllerScope.projectListView = projectListView;
    viewProjectControllerScope.showEdit = showEdit;
    viewProjectControllerScope.getCustomerList = getCustomerList;
    viewProjectControllerScope.openStartingDateCalender = openStartingDateCalender;
    viewProjectControllerScope.openEndDateCalender = openEndDateCalender;
    viewProjectControllerScope.selectedStatus = selectedStatus;
    viewProjectControllerScope.selectedCustomer = selectedCustomer;
    viewProjectControllerScope.updateProject = updateProject;
    viewProjectControllerScope.decline = decline;
    viewProjectControllerScope.hideAlertPopup = hideAlertPopup;
    viewProjectControllerScope.callChangeStatus = callChangeStatus;



    viewProjectControllerScope.statusList = [{
        status: constantsService.activeState,

    }, {
        status: constantsService.inactiveState,

    }];

    function updateProject() {
        viewProjectControllerScope.spinner = true;

        if (viewProjectControllerScope.changeActive != "" && viewProjectControllerScope.changeActive != undefined) {
            viewProjectControllerScope.projectInfo.status = viewProjectControllerScope.changeActive;
        } else if (viewProjectControllerScope.projectInfo.statusSelected != "" && viewProjectControllerScope.projectInfo.statusSelected != undefined) {
            viewProjectControllerScope.projectInfo.status = viewProjectControllerScope.projectInfo.statusSelected;
            viewProjectControllerScope.projectInfo.customer = viewProjectControllerScope.projectInfo.customerName;
        }
        projectService.updateProjectDetail(viewProjectControllerScope.projectInfo).then(function(response) {
            if (response.data.msg) {
                viewProjectControllerScope.infoMsg = response.data.msg;
                viewProjectControllerScope.projectList = '';
            } else {
                if (response.data.successmsg) {
                    if (viewProjectControllerScope.changeActive != "") {} else {
                        $scope.modalShown = true;
                        viewProjectControllerScope.alertMsg = "Project Details updated successfully";
                        viewProjectControllerScope.projectSearchKeyword = "";
                        viewProjectControllerScope.title = "Projects";
                    }
                }
                projectListView();

            }
        });

        viewProjectControllerScope.spinner = false;
    }


    getProject();
    getCustomerList();

    function getProject() {
        viewProjectControllerScope.spinner = true;
        projectService.getProject().then(function(response) {
            if (response.data.msg) {
                viewProjectControllerScope.infoMsg = response.data.msg;
                viewProjectControllerScope.projectList = '';
            } else {
                viewProjectControllerScope.projectList = response.data;
                $scope.currentPage = 1;
                $scope.totalItems = viewProjectControllerScope.projectList.length;
                $scope.entryLimit = 10;
                $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                $scope.$watch('search', function(newVal, oldVal) {

                    $scope.filtered = filterFilter(viewProjectControllerScope.projectList, newVal);
                    $scope.totalItems = $scope.filtered.length;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                    $scope.currentPage = 1;
                }, true);
            }
        });
        viewProjectControllerScope.spinner = false;
    }

    function showProjectDetail(projectDetails) {
        viewProjectControllerScope.projectInfo = "";
        projectDetailInfoBackup = "";

        viewProjectControllerScope.showEditButton = true;
        viewProjectControllerScope.nonEditableField = true;
        viewProjectControllerScope.edititemtrue = true;
        viewProjectControllerScope.edititem = false;
        viewProjectControllerScope.showProjectList = false;
        viewProjectControllerScope.projectsearchKeyword = "";
        viewProjectControllerScope.title = "Project Detail"
        projectDetailInfoBackup = angular.copy(projectDetails);
        viewProjectControllerScope.projectInfo = angular.copy(projectDetails);
        viewProjectControllerScope.projectInfo.customerName = viewProjectControllerScope.projectInfo.customer;
        viewProjectControllerScope.projectInfo.statusSelected = viewProjectControllerScope.projectInfo.status;
        viewProjectControllerScope.fieldEditable = true;

    }


    function projectListView() {
        viewProjectControllerScope.spinner = true;
        getProject();
        viewProjectControllerScope.showProjectList = true;
        viewProjectControllerScope.customersearchKeyword = "";
        viewProjectControllerScope.title = "Projects";
        viewProjectControllerScope.spinner = false;
    }

    function getCustomerList() {
        viewProjectControllerScope.spinner = true;
        customerService.getCustomerList().then(function(response) {
            if (response.data.msg) {
                viewProjectControllerScope.infoMsg = response.data.msg;
                viewProjectControllerScope.customerList = '';
            } else {
                if (response.data) {
                    viewProjectControllerScope.customerList = response.data;
                }
            }

        });

        viewProjectControllerScope.spinner = false;
    }

    function showEdit() {

        viewProjectControllerScope.spinner = true;
        viewProjectControllerScope.nonEditableField = false;
        viewProjectControllerScope.edititemtrue = true;
        viewProjectControllerScope.edititem = true;
        viewProjectControllerScope.showEditButton = false;
        viewProjectControllerScope.fieldEditable = false;
        viewProjectControllerScope.spinner = false;
    }

    function openStartingDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewProjectControllerScope.startDateCalender = !viewProjectControllerScope.startDateCalender;
    }

    function openEndDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewProjectControllerScope.endDateCalender = !viewProjectControllerScope.endDateCalender;
    }

    function selectedStatus(status) {
        viewProjectControllerScope.showStatusUl = !viewProjectControllerScope.showStatusUl;
        viewProjectControllerScope.changeActive = "";
        viewProjectControllerScope.projectInfo.statusSelected = status;
    }

    function selectedCustomer(customer) {
        viewProjectControllerScope.showCustomerUl = !viewProjectControllerScope.showCustomerUl;
        viewProjectControllerScope.projectInfo.customerName = customer;
    }

    function gotoNewProject() {
        $location.url('/createProject')
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }


    function decline() {
        projectListView();
    }

    function callChangeStatus(status, projectDetails) {
        viewProjectControllerScope.spinner = true;
        viewProjectControllerScope.projectInfo = angular.copy(projectDetails);
        viewProjectControllerScope.changeActive = status;
        updateProject();
        viewProjectControllerScope.spinner = false;
    }
}
