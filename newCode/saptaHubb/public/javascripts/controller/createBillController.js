angular
    .module('ePortal')
    .controller('createBillController', createBillController);

createBillController.$inject = ['$scope', '$location', 'vendorService', 'configrationService', 'billServices', '$rootScope'];

function createBillController($scope, $location, vendorService, configrationService, billServices, $rootScope) {

    var createBillControllerScope = this;

    createBillControllerScope.spinner = false;
    createBillControllerScope.vendorList = '';
    createBillControllerScope.newbill = "";
    createBillControllerScope.currencyTypeList = '';
    createBillControllerScope.selectVendorType = "Select Vendor";
    createBillControllerScope.selectCurrencyType = "Select Currency Type";

    createBillControllerScope.dueDateCalender = false;
    createBillControllerScope.opendueDateCalender = opendueDateCalender;
    createBillControllerScope.billingDateCalender = false;
    createBillControllerScope.openbillingDateCalender = openbillingDateCalender;
    createBillControllerScope.hideAlertPopup = hideAlertPopup;
    createBillControllerScope.decline = decline;
    createBillControllerScope.selectedVendorTypeFun = selectedVendorTypeFun;
    createBillControllerScope.selectedCurrencyTypeFun = selectedCurrencyTypeFun;

    createBillControllerScope.addBill = addbill;
    $rootScope.currentMenu = "Create Bill";

    getVndorList();
    getCurrencyType();
    getBillNumber();



    function addbill() {
        createBillControllerScope.spinner = true;
        createBillControllerScope.newbill.vendor = createBillControllerScope.selectVendorType;
        createBillControllerScope.newbill.currencytype = createBillControllerScope.selectCurrencyType;

        billServices.addBill(createBillControllerScope.newbill).then(function(response) {
            if (response.data.msg) {
                createBillControllerScope.infoMsg = response.data.msg;
                createBillControllerScope.spinner = false;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    createBillControllerScope.alertMsg = "Bill Created successfully";
                    createBillControllerScope.newbill = "";
                    createBillControllerScope.selectVendorType = "Select Vendor";
                    createBillControllerScope.selectCurrencyType = "Select Currency Type";
                    createBillControllerScope.newBillForm.$setPristine();
                    createBillControllerScope.newBillForm.$setUntouched();
                    createBillControllerScope.spinner = false;
                    getBillNumber();
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
        createBillControllerScope.spinner = false;
    }

    function getVndorList() {
        createBillControllerScope.spinner = true;
        vendorService.getVendorList().then(function(response) {
            if (response.data.msg) {
                createBillControllerScope.spinner = false;
                createBillControllerScope.infoMsg = response.data.msg;
                createBillControllerScope.vendorList = '';
            } else {
                createBillControllerScope.spinner = false;
                createBillControllerScope.vendorList = response.data;
            }
        });
    }

    function getCurrencyType() {
        createBillControllerScope.spinner = true;
        configrationService.getCurrencyType().then(function(response) {
            if (response.data.msg) {
                createBillControllerScope.spinner = false;
                createBillControllerScope.infoMsg = response.data.msg;
                createBillControllerScope.currencyTypeList = '';
            } else {
                createBillControllerScope.spinner = false;
                createBillControllerScope.currencyTypeList = response.data;
            }
        });
    }

    function getBillNumber() {
        createBillControllerScope.spinner = true;
        billServices.newBillNumber().then(function(response) {
            createBillControllerScope.newbill = { billno: response.data };
        });
        createBillControllerScope.spinner = false;
    }

    function opendueDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createBillControllerScope.dueDateCalender = !createBillControllerScope.dueDateCalender;
    }

    function openbillingDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createBillControllerScope.billingDateCalender = !createBillControllerScope.billingDateCalender;
    }

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    $scope.initDate = new Date();
    $scope.formats = ['dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewBill');
    }


    function decline() {
        createBillControllerScope.spinner = true;
        createBillControllerScope.newbill = "";
        createBillControllerScope.newBillForm.$setPristine();
        createBillControllerScope.newBillForm.$setUntouched();
        getBillNumber();
        createBillControllerScope.spinner = false;
    }

    function selectedVendorTypeFun(vendor) {
        createBillControllerScope.spinner = true;
        createBillControllerScope.selectVendorType = vendor;
        createBillControllerScope.spinner = false;
    }

    function selectedCurrencyTypeFun(currency) {
        createBillControllerScope.spinner = true;
        createBillControllerScope.selectCurrencyType = currency;
        createBillControllerScope.spinner = false;
    }
}
