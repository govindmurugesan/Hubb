angular
    .module('ePortal')
    .controller('companyInformationController', companyInformationController);

companyInformationController.$inject = ['$scope', '$location', 'companyInformationService', 'configrationService', '$rootScope'];

function companyInformationController($scope, $location, companyInformationService, configrationService, $rootScope) {
    var companyInformationControllerScope = this;

    companyInformationControllerScope.hideAlertPopup = hideAlertPopup;
    companyInformationControllerScope.editDetails = editDetails;
    companyInformationControllerScope.decline = decline;
    companyInformationControllerScope.updateCompanyInformation = updateCompanyInformation;
    companyInformationControllerScope.countrySelectChange = countrySelectChange;
    companyInformationControllerScope.selectedStateFun = selectedStateFun;
    companyInformationControllerScope.selectedCountryFun = selectedCountryFun;
    companyInformationControllerScope.selectedCountryCodeType = selectedCountryCodeType;
    companyInformationControllerScope.showEditButton = true;
    companyInformationControllerScope.nonEditableField = true;
    companyInformationControllerScope.edititemtrue = true;
    companyInformationControllerScope.edititem = false;

    $rootScope.currentMenu = "Company Information";
    companyInformationControllerScope.SelectedCountry = { name: "Select Country" };
    companyInformationControllerScope.selectedState = { name: "Select State" };
    companyInformationControllerScope.selectCountryCodeType = { countryCode: "Code" };

    getCompanyInformation();

    function getCompanyInformation() {


        companyInformationService.getCompanyInformation().then(function(response) {
            if (response.data.msg) {
                $scope.modalShown = true;
                companyInformationControllerScope.alertMsg = response.data.msg;
                companyInformationControllerScope.companyName = 'Company Details'
                companyInformationControllerScope.spinner = false;
            } else {

                if (response.data) {
                    companyInformationControllerScope.companyInfo = response.data[0];
                    companyInformationControllerScope.SelectedCountry = { name: companyInformationControllerScope.companyInfo.country };
                    companyInformationControllerScope.selectedState = { name: companyInformationControllerScope.companyInfo.state };
                    companyInformationControllerScope.selectCountryCodeType = { countryCode: companyInformationControllerScope.companyInfo.callingCode };
                }
            }

        }, function(err) {
            console.log("company eerroorr");
        });
    }

    function hideAlertPopup() {
        $scope.modalShown = false;
    }

    function editDetails() {
        companyInformationControllerScope.spinner = true;
        companyInformationControllerScope.nonEditableField = false;
        companyInformationControllerScope.edititemtrue = true;
        companyInformationControllerScope.edititem = true;
        companyInformationControllerScope.showEditButton = false;
        companyInformationControllerScope.fieldEditable = false;
        companyInformationControllerScope.spinner = false;
        getCountryList();
    }

    function decline() {
        companyInformationControllerScope.showEditButton = true;
        companyInformationControllerScope.nonEditableField = true;
        companyInformationControllerScope.edititemtrue = true;
        companyInformationControllerScope.edititem = false;
        companyInformationControllerScope.companyInfo = "";
        companyInformationControllerScope.companyDetailsForm.$setPristine();
        companyInformationControllerScope.companyDetailsForm.$setUntouched();
        getCompanyInformation();
    }

    function updateCompanyInformation() {
        companyInformationControllerScope.spinner = true;

        companyInformationControllerScope.companyInfo.callingCode = companyInformationControllerScope.selectCountryCodeType.countryCode;
        companyInformationControllerScope.companyInfo.country = companyInformationControllerScope.SelectedCountry
        companyInformationControllerScope.companyInfo.state = companyInformationControllerScope.selectedState
        if (companyInformationControllerScope.companyInfo.country.name) companyInformationControllerScope.companyInfo.country = companyInformationControllerScope.companyInfo.country.name;
        if (companyInformationControllerScope.companyInfo.state.name) companyInformationControllerScope.companyInfo.state = companyInformationControllerScope.companyInfo.state.name;

        companyInformationService.updateCompanyInformation(companyInformationControllerScope.companyInfo).then(function(response) {

            if (response.data.msg) {

            } else {
                if (response.data.successMsg) {
                    companyInformationControllerScope.companyInfo = "";
                    companyInformationControllerScope.companyDetailsForm.$setPristine();
                    companyInformationControllerScope.companyDetailsForm.$setUntouched();
                    getCompanyInformation();
                    companyInformationControllerScope.showEditButton = true;
                    companyInformationControllerScope.nonEditableField = true;
                    companyInformationControllerScope.edititemtrue = true;
                    companyInformationControllerScope.edititem = false;
                    companyInformationControllerScope.spinner = false;
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });


    }

    function getCountryList() {
        companyInformationControllerScope.spinner = true;
        configrationService.getCountryDetals().then(function(response) {

            if (response.data.msg) {
                $scope.modalShown = true;
                companyInformationControllerScope.alertMsg = response.data.msg;
                companyInformationControllerScope.spinner = false;
            } else {
                if (response.data) {
                    companyInformationControllerScope.countryList = response.data;
                    companyInformationControllerScope.spinner = false;
                }
            }

        }, function(err) {
            // $scope.mainControllerScope.templateSection = 'errormsg';
        });
    }

    function countrySelectChange(countryDetail) {
        companyInformationControllerScope.companyInfo.callingCode = countryDetail.callingCode + '(' + countryDetail.name + ')'
    }

    function selectedStateFun(state) {
        companyInformationControllerScope.selectedState = state;
    }

    function selectedCountryFun(country) {
        companyInformationControllerScope.SelectedCountry = country;
        companyInformationControllerScope.stateList = country.state;
        companyInformationControllerScope.selectedState = { name: "Select State" };

    }

    // function selectedCountryCodeType(country) {
    //      companyInformationControllerScope.showCountryCodeUl=!companyInformationControllerScope.showCountryCodeUl;
    //     companyInformationControllerScope.selectCountryCodeType.countryCode = country;
    // }

    function selectedCountryCodeType(countryCode) {

        companyInformationControllerScope.selectCountryCodeType = { countryCode: countryCode.callingCode + ' (' + countryCode.countryCode + ')' };

    }



}
