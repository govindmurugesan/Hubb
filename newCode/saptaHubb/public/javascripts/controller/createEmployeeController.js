angular
    .module('ePortal')
    .controller('createEmployeeController', createEmployeeController);

createEmployeeController.$inject = ['$scope', '$location', 'employeeService', 'filterFilter', 'configrationService', '$rootScope'];

function createEmployeeController($scope, $location, employeeService, filterFilter, configrationService, $rootScope) {
    var createEmployeeControllerScope = this;
    createEmployeeControllerScope.spinner = false;
    createEmployeeControllerScope.isSelectedEmployee = false;

    createEmployeeControllerScope.decline = decline;
    createEmployeeControllerScope.createEmployee = createEmployee;
    createEmployeeControllerScope.moveToWorkLocation = moveToWorkLocation;
    createEmployeeControllerScope.moveToEmplyType = moveToEmplyType;
    createEmployeeControllerScope.moveToDepartment = moveToDepartment;
    createEmployeeControllerScope.openjoiningDateCalender = openjoiningDateCalender;
    createEmployeeControllerScope.joiningDateCalender = false;
    createEmployeeControllerScope.dateOfBirthCalender = false;
    createEmployeeControllerScope.opendateOfBirthCalender = opendateOfBirthCalender;
    createEmployeeControllerScope.hideAlertPopup = hideAlertPopup;
    createEmployeeControllerScope.viewEmployee = viewEmployee;
    createEmployeeControllerScope.selectedWorkFun = selectedWorkFun;
    createEmployeeControllerScope.selectedEmployeeFun = selectedEmployeeFun;
    // createEmployeeControllerScope.selectedReportingFun = selectedReportingFun;
    createEmployeeControllerScope.selectedDepartmentFun = selectedDepartmentFun;
    createEmployeeControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;
    createEmployeeControllerScope.selectedRepoting = selectedRepoting;

    createEmployeeControllerScope.selectedWork = { worklocation: "Select Work location" };
    createEmployeeControllerScope.selectedEmployeeType = { name: "Select Employee Type" };
    // createEmployeeControllerScope.selectedReportingType = { firstName: "Select Reporting Type" };
    createEmployeeControllerScope.selectedDepartmentType = { name: "Select Department Type" };

    $rootScope.currentMenu = "New Employee";
    $scope.showLabel = false;

    function selectedRepoting(reportingTo) {
        createEmployeeControllerScope.spinner = true;
        createEmployeeControllerScope.employee.reportingTo = {
            reportingEmployeeId: reportingTo._id,
            reportingEmployeeName: reportingTo.firstName + ' ' + reportingTo.middleName + ' ' + reportingTo.lastName
        };
        createEmployeeControllerScope.employee.reporting = createEmployeeControllerScope.employee.reportingTo.reportingEmployeeName;

        createEmployeeControllerScope.isEmployeeFound = false;
        createEmployeeControllerScope.spinner = false;
    }

    function getEmployeeByEmailOrName(employee) {
        createEmployeeControllerScope.spinner = true;
        employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.employeeList = '';
                createEmployeeControllerScope.spinner = false;
            } else {
                if (response.data) {
                    createEmployeeControllerScope.employeeList = response.data;
                    createEmployeeControllerScope.employee.reportingTo = {
                        reportingEmployeeId: '',
                        reportingEmployeeName: ''
                    };
                }
            }
            if (createEmployeeControllerScope.employeeList.length > 0) {
                createEmployeeControllerScope.isEmployeeFound = true;
                createEmployeeControllerScope.employeeListNotFound = false;
            } else {
                createEmployeeControllerScope.isEmployeeFound = false;
                createEmployeeControllerScope.employeeListNotFound = true;
            }

        }, function(err) {
            console.log("Sales Error");
        });
        createEmployeeControllerScope.spinner = false;
    }

    function opendateOfBirthCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createEmployeeControllerScope.dateOfBirthCalender = !createEmployeeControllerScope.dateOfBirthCalender
    };

    function openjoiningDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        createEmployeeControllerScope.joiningDateCalender = !createEmployeeControllerScope.joiningDateCalender
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    $scope.initDate = new Date();
    $scope.formats = ['dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    getWorkLocationDetails();
    viewEmployee();


    function createEmployee() {
        createEmployeeControllerScope.spinner = true;

        createEmployeeControllerScope.employee.workLocation = createEmployeeControllerScope.selectedWork;
        createEmployeeControllerScope.employee.employeeType = createEmployeeControllerScope.selectedEmployeeType;
        createEmployeeControllerScope.employee.department = createEmployeeControllerScope.selectedDepartmentType;
        if (createEmployeeControllerScope.employee.reporting == '' || createEmployeeControllerScope.employee.reporting == undefined) {
            createEmployeeControllerScope.employee.reportingTo = { reportingEmployeeId: "", reportingEmployeeName: "" };
        }

        employeeService.createEmployee(createEmployeeControllerScope.employee).then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.spinner = false;
                createEmployeeControllerScope.infoMsg = response.data.msg;
                createEmployeeControllerScope.worklocationListType = '';
            } else {
                if (response.data.successMsg) {
                    $scope.modalShown = true;
                    createEmployeeControllerScope.alertMsg = "Employee created successfully";
                    createEmployeeControllerScope.employee = "";
                    viewEmployee();
                    createEmployeeControllerScope.createEmployeeForm.$setPristine();
                    createEmployeeControllerScope.createEmployeeForm.$setUntouched();

                    createEmployeeControllerScope.selectedWork = { worklocation: "Select Work location" };
                    createEmployeeControllerScope.selectedEmployeeType = { name: "Select Employee Type" };
                    createEmployeeControllerScope.selectedReportingType = { firstName: "Select Reporting Type" };
                    createEmployeeControllerScope.selectedDepartmentType = { name: "Select Department Type" };
                }
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
        createEmployeeControllerScope.spinner = false;
    }

    function viewEmployee() {
        createEmployeeControllerScope.spinner = true;
        employeeService.viewEmployee().then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.spinner = false;
                createEmployeeControllerScope.viewEmployeeList = '';
            } else {
                createEmployeeControllerScope.viewEmployeeList = response.data;
                createEmployeeControllerScope.spinner = false;
            }
        }, function(error_response) {
            console.log("error", error_response);
        });
    }

    function moveToWorkLocation() {
        $location.path('/configuration/work_Location');
    }

    function moveToEmplyType() {
        $location.path('/configuration/employee_Types');
    }

    function moveToDepartment() {
        $location.path('/configuration/department');
    }


    $('#date1').click(function() {
        $("#datepicker").datepicker();
    });


    function getWorkLocationDetails() {
        createEmployeeControllerScope.infoMsg = "";
        createEmployeeControllerScope.spinner = true;
        configrationService.getWorkLocationDetails().then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.spinner = false;
                createEmployeeControllerScope.infoMsg = response.data.msg;
                createEmployeeControllerScope.worklocationListType = '';
            } else {

                if (response.data.length > 0) {
                    createEmployeeControllerScope.worklocationListType = response.data;
                    createEmployeeControllerScope.spinner = false;
                    getEmployeeType();
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getEmployeeType() {
        createEmployeeControllerScope.infoMsg = "";
        createEmployeeControllerScope.spinner = true;
        configrationService.getEmployeeType().then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.spinner = false;
                createEmployeeControllerScope.infoMsg = response.data.msg;
                createEmployeeControllerScope.employeeTypeList = '';
            } else {

                if (response.data.length > 0) {
                    createEmployeeControllerScope.employeeTypeList = response.data;
                    createEmployeeControllerScope.spinner = false;
                    getDepartment();
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getDepartment() {
        createEmployeeControllerScope.infoMsg = "";
        createEmployeeControllerScope.spinner = true;
        configrationService.getDepartment().then(function(response) {
            if (response.data.msg) {
                createEmployeeControllerScope.spinner = false;
                createEmployeeControllerScope.infoMsg = response.data.msg;
                createEmployeeControllerScope.departmentList = '';
            } else {

                if (response.data.length > 0) {
                    createEmployeeControllerScope.departmentList = response.data;

                    createEmployeeControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function decline() {
        createEmployeeControllerScope.employee = "";
        createEmployeeControllerScope.createEmployeeForm.$setPristine();
        createEmployeeControllerScope.createEmployeeForm.$setUntouched();
        createEmployeeControllerScope.selectedWork = { worklocation: "Select Work location" };
        createEmployeeControllerScope.selectedEmployeeType = { name: "Select Employee Type" };
        createEmployeeControllerScope.selectedReportingType = { firstName: "Select Reporting Type" };
        createEmployeeControllerScope.selectedDepartmentType = { name: "Select Department Type" };

    }

    function hideAlertPopup() {
        $scope.modalShown = false;
        $location.path('/viewEmployee');
    }

    function selectedWorkFun(workPlace) {
        createEmployeeControllerScope.selectedWork = workPlace;
        createEmployeeControllerScope.keywordworkLocation = "";
        createEmployeeControllerScope.isSelectedEmployee = true;
    }

    function selectedEmployeeFun(emptypelist) {
        createEmployeeControllerScope.selectedEmployeeType = emptypelist;
        createEmployeeControllerScope.keywordemployeeType="";

    }

    // function selectedReportingFun(emplist) {
    //     createEmployeeControllerScope.selectedReportingType = emplist;

    // }

    function selectedDepartmentFun(deplist) {
        createEmployeeControllerScope.selectedDepartmentType = deplist;
        createEmployeeControllerScope.keywordDepartment="";
    }

}
