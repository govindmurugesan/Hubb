angular
    .module('ePortal')
    .controller('dashBoardController', dashBoardController);

dashBoardController.$inject = ['$scope', '$location', '$rootScope', '$cookies', 'employeeService', 'leadService', 'salesReportService', 'projectService', 'usersService', '$filter'];

function dashBoardController($scope, $location, $rootScope, $cookies, employeeService, leadService, salesReportService, projectService, usersService, $filter) {
    var dashBoardControllerScope = this;
    dashBoardControllerScope.spinner = false;
    dashBoardControllerScope.getLead = getLead;
    dashBoardControllerScope.previouss = previouss;
    dashBoardControllerScope.nexts = nexts;
    dashBoardControllerScope.nextSalesReport = nextSalesReport;
    dashBoardControllerScope.previousSalesReport = previousSalesReport;
    dashBoardControllerScope.selectedEmpFunc = selectedEmpFunc;
    dashBoardControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;

    $rootScope.currentMenu = "Dashboard";
    dashBoardControllerScope.q1 = 0;
    dashBoardControllerScope.q2 = 0;
    dashBoardControllerScope.q3 = 0;
    dashBoardControllerScope.q4 = 0;
    dashBoardControllerScope.q1Total = dashBoardControllerScope.q2Total = dashBoardControllerScope.q3Total = dashBoardControllerScope.q4Total = 0;

    dashBoardControllerScope.currencyVal = ['L', 'L', 'L', 'L'];
    dashBoardControllerScope.salesReportMsg = "";
    dashBoardControllerScope.selectedEmp = { name: "Select BDM" };

    currentYear = dashBoardControllerScope.today = new Date();
    nextYear = dashBoardControllerScope.nextDate = new Date();
    $scope.financialYearBeg = new Date();
    $scope.financialYearEnd = new Date();
    // var financialYearBeg, financialYearEnd;

    $scope.financialYearEnd.setFullYear($scope.financialYearEnd.getFullYear() + 1);
    dashBoardControllerScope.achived = [];
    // achived = [];
    target = new Array();

    viewEmployee();
    getUserInformation();
    getLoginUserInfo();
    getLead();

    $scope.$on('SalesReport', function() {
        dashBoardControllerScope.SalesReport = true;
    });
    $scope.$on('FinancialReport', function() {
        dashBoardControllerScope.FinancialReport = true;
    });
    $scope.$on('todo', function() {
        dashBoardControllerScope.todo = true;
    });

    function getEmployeeByEmailOrName(employee) {
        dashBoardControllerScope.spinner = true;
        dashBoardControllerScope.employeeList = '';
        if (employee == '' || employee == null) {
            dashBoardControllerScope.bdm = '';
            getProject(moment(currentYear).format('YYYY'), moment(currentYear).format('MM'));
        } else {
            employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
                if (response.data.msg) {
                    dashBoardControllerScope.spinner = false;
                    dashBoardControllerScope.employeeList = '';
                } else {
                    if (response.data) {
                        dashBoardControllerScope.employeeList = response.data;
                    }
                }
                if (dashBoardControllerScope.employeeList.length > 0) {
                    dashBoardControllerScope.isEmployeeFound = true;
                }
            }, function(err) {});
            if (dashBoardControllerScope.employeeList.length == 0 || dashBoardControllerScope.employeeList == '') {
                dashBoardControllerScope.isEmployeeFound = false;
            }
        }
        dashBoardControllerScope.spinner = false;
    }

    function previouss() {
        dashBoardControllerScope.spinner = true;
        dashBoardControllerScope.nextDate.setDate(dashBoardControllerScope.nextDate.getDate() - 1);
        getToDoByDate(dashBoardControllerScope.todoList);
        dashBoardControllerScope.spinner = false;
    }

    function nexts() {
        dashBoardControllerScope.spinner = true;
        dashBoardControllerScope.nextDate.setDate(dashBoardControllerScope.nextDate.getDate() + 1);
        getToDoByDate(dashBoardControllerScope.todoList);
        dashBoardControllerScope.spinner = false;
    }


    function nextSalesReport() {
        dashBoardControllerScope.spinner = true;
        $scope.financialYearEnd.setFullYear($scope.financialYearBeg.getFullYear() + 2);
        $scope.financialYearBeg.setFullYear($scope.financialYearBeg.getFullYear() + 1);
        getProject(moment($scope.financialYearBeg).format('YYYY'), moment(currentYear).format('MM'));
        dashBoardControllerScope.spinner = false;
    }

    function previousSalesReport() {
        dashBoardControllerScope.spinner = true;
        $scope.financialYearEnd.setFullYear($scope.financialYearBeg.getFullYear());
        $scope.financialYearBeg.setFullYear($scope.financialYearBeg.getFullYear() - 1);
        getProject(moment($scope.financialYearBeg).format('YYYY'), moment(currentYear).format('MM'));
        dashBoardControllerScope.spinner = false;
    }

    function selectedEmpFunc(emp) {
        dashBoardControllerScope.spinner = true;
        dashBoardControllerScope.bdm = emp._id;
        dashBoardControllerScope.emp = emp.firstName + ' ' + emp.middleName + ' ' + emp.lastName;
        dashBoardControllerScope.isEmployeeFound = false;
        getProject(moment(currentYear).format('YYYY'), moment(currentYear).format('MM'));
        dashBoardControllerScope.spinner = false;
    }

    function getLead() {
        dashBoardControllerScope.spinner = true;
        leadService.getLead().then(function(response) {
            if (response.data.msg) {
                dashBoardControllerScope.infoViewMsg = response.data.msg;
                dashBoardControllerScope.todoList = '';

            } else {
                if (response.data) {
                    dashBoardControllerScope.infoViewMsg = "";
                    dashBoardControllerScope.todoList = response.data;
                    getToDoByDate(response.data);
                }
            }
            dashBoardControllerScope.currentDate = dashBoardControllerScope.nextDate;
        });
        dashBoardControllerScope.spinner = false;
    }

    function getToDoByDate(data) {
        dashBoardControllerScope.spinner = true;
        count = 0;
        dashBoardControllerScope.dateList = [];
        dashBoardControllerScope.infoViewMsg = "";
        angular.forEach(data, function(lead) {
            angular.forEach(lead.followup, function(followup, key) {
                if ($filter('date')(dashBoardControllerScope.nextDate, 'dd MMMM yyyy') == $filter('date')(followup.followupDate, 'dd MMMM yyyy')) {
                    if (dashBoardControllerScope.isAdmin) {
                        dashBoardControllerScope.dateList.push(followup);
                        dashBoardControllerScope.dateList[count].leadName = lead.leadName;
                        count++;
                    } else if ((followup.addedEmployeeID == dashBoardControllerScope.bdm || followup.addedEmployee === dashBoardControllerScope.emp)&&(dashBoardControllerScope.bdm!=undefined||dashBoardControllerScope.emp!=undefined)) {
                        dashBoardControllerScope.dateList.push(followup);
                        dashBoardControllerScope.dateList[count].leadName = lead.leadName;
                        count++;
                    }
                }
            });
        });
        if (count == 0)
            dashBoardControllerScope.infoViewMsg = "Follow-up not available";
        dashBoardControllerScope.spinner = false;
    }

    function getUserInformation() {
        dashBoardControllerScope.infoMsg = "";
        dashBoardControllerScope.spinner = true;
        usersService.getUserInformation().then(function(response) {
            if (response.data.msg) {
                dashBoardControllerScope.spinner = false;
                dashBoardControllerScope.infoMsg = response.data.msg;
            } else {
                if (response.data.length > 0) {
                    angular.forEach(response.data, function(ele) {
                        if ($cookies.getObject('saptaePortalCookies').email == ele.email[0].address) {
                           
                            if (ele.role.name == "Super Admin")
                                dashBoardControllerScope.isAdmin = true;
                            else {
                                dashBoardControllerScope.isAdmin = false;
                                dashBoardControllerScope.salesReportMsg = "Employee information is not present";
                            }
                        }
                    });
                    dashBoardControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getLoginUserInfo() {
        dashBoardControllerScope.spinner = true;
        employeeService.getEmployeeDetailsByEmail($cookies.getObject('saptaePortalCookies').email).then(function(response) {
            if (response.data.msg) { dashBoardControllerScope.salesReportMsg = "Employee information not present"; } else {
                if (response.data) {
                    if (!dashBoardControllerScope.isAdmin) {
                        dashBoardControllerScope.bdm = response.data._id;
                        dashBoardControllerScope.emp = response.data.firstName + ' ' + response.data.middleName + ' ' + response.data.lastName;
                    }
                    getProject(moment(currentYear).format('YYYY'), moment(currentYear).format('MM'));
                }
            }
        }, function(err) {
            console.log(err);
        });
        dashBoardControllerScope.spinner = false;
    }


    function getProject(presentYear, presentMonth) {
        dashBoardControllerScope.spinner = true;
        if (dashBoardControllerScope.bdm == undefined || dashBoardControllerScope.bdm == '') {
            dashBoardControllerScope.isSearchEmpty = true;
        } else {
            dashBoardControllerScope.isSearchEmpty = false;
        }
        projectService.getProject().then(function(response) {
            if (response.data.msg) {
                dashBoardControllerScope.salesReportMsg = " ";
                dashBoardControllerScope.projectList = '';
                dashBoardControllerScope.achived = [0, 0, 0, 0];
                dashBoardControllerScope.achievedTarget = 0;
            } else {
                dashBoardControllerScope.salesReportMsg = '';
                dashBoardControllerScope.q1 = dashBoardControllerScope.q2 = dashBoardControllerScope.q3 = dashBoardControllerScope.q4 = dashBoardControllerScope.achievedTarget = 0;
                angular.forEach(response.data, function(bdm) {
                    if (bdm.bdm) {
                        var year = $filter('date')(bdm.startDate, 'yyyy');
                        var month = $filter('date')(bdm.startDate, 'MM');
                        if (presentMonth < 3)
                            year--;
                        if (bdm.bdm._id == dashBoardControllerScope.bdm && !dashBoardControllerScope.isSearchEmpty) {

                            if ((year == presentYear - 1) && month <= 3) {
                                dashBoardControllerScope.q4 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 06) {
                                dashBoardControllerScope.q1 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 9) {
                                dashBoardControllerScope.q2 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 12) {
                                dashBoardControllerScope.q3 += bdm.projectCost;
                            }
                        } else if (dashBoardControllerScope.isSearchEmpty) {
                            if ((year == presentYear - 1) && month <= 3) {
                                dashBoardControllerScope.q4 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 06) {
                                dashBoardControllerScope.q1 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 9) {
                                dashBoardControllerScope.q2 += bdm.projectCost;
                            } else
                            if ((year == presentYear) && month <= 12) {
                                dashBoardControllerScope.q3 += bdm.projectCost;
                            }
                        }
                    }
                });
                dashBoardControllerScope.q1Total = dashBoardControllerScope.q1;
                dashBoardControllerScope.q2Total = dashBoardControllerScope.q2;
                dashBoardControllerScope.q3Total = dashBoardControllerScope.q3;
                dashBoardControllerScope.q4Total = dashBoardControllerScope.q4;
                if (dashBoardControllerScope.q1Total < 100000) {
                    dashBoardControllerScope.q1Total /= 100000;
                } else {
                    dashBoardControllerScope.q1Total /= 10000000;
                }
                if (dashBoardControllerScope.q2Total < 100000) {
                    dashBoardControllerScope.q2Total /= 100000;
                } else {
                    dashBoardControllerScope.q2Total /= 10000000;
                }
                if (dashBoardControllerScope.q3Total < 100000) {
                    dashBoardControllerScope.q3Total /= 100000;
                } else {
                    dashBoardControllerScope.q3Total /= 10000000;
                }
                if (dashBoardControllerScope.q4Total < 100000) {
                    dashBoardControllerScope.q4Total /= 100000;
                } else {
                    dashBoardControllerScope.q4Total /= 10000000;
                }
                dashBoardControllerScope.achievedTarget = dashBoardControllerScope.q1 + dashBoardControllerScope.q2 + dashBoardControllerScope.q3 + dashBoardControllerScope.q4;
                dashBoardControllerScope.achived = [dashBoardControllerScope.q1Total, dashBoardControllerScope.q2Total, dashBoardControllerScope.q3Total, dashBoardControllerScope.q4Total];
            }
        });

        getSalesReport(moment(presentYear), moment(presentYear).get('year') + 1, dashBoardControllerScope.bdm);
        dashBoardControllerScope.spinner = false;
    }

    function getSalesReport(financialYearStart, financialYearEnd, bdm) {
        dashBoardControllerScope.target = [];
        target = [];
        dashBoardControllerScope.spinner = true;
        var yearStart = financialYearStart.format('YYYY'),
            yearEnd = financialYearEnd;
        if (dashBoardControllerScope.emp == '' || dashBoardControllerScope.emp == undefined || bdm == undefined) {
            bdm = '';
        }
        salesReportService.checkBDMSalesRecord(yearStart, yearEnd, bdm).then(function(response) {

            if (response.data.msg) {
                dashBoardControllerScope.salesReportMsg = "Target has not set  for financial year " + yearStart + '-' + yearEnd;
            } else {
                if (response.data) {
                    dashBoardControllerScope.salesReportMsg = '';
                    dashBoardControllerScope.totalTarget = 0;
                    angular.forEach(response.data, function(val) {
                        dashBoardControllerScope.target.push(val.q1);
                        dashBoardControllerScope.target.push(val.q2);
                        dashBoardControllerScope.target.push(val.q3);
                        dashBoardControllerScope.target.push(val.q4);
                        dashBoardControllerScope.totalTarget += val.q1 + val.q2 + val.q3 + val.q4;
                    });
                    angular.forEach(dashBoardControllerScope.target, function(value, key) {
                        if (value < 100000 || value < 10000000) {
                            value /= 100000.0;
                            dashBoardControllerScope.currencyVal[key] = 'L';
                        } else {
                            value /= 10000000.0;
                            dashBoardControllerScope.currencyVal[key] = 'Cr';
                        }

                        if (key >= 4) {
                            target[key % 4.0] += value;
                        } else
                            target.push(value);
                    });
                    $scope.salesReportTableData = {
                        type: 'vbullet',
                        scaleX: {
                            labels: [target[0] + ' ' + dashBoardControllerScope.currencyVal[0] + ' <br><strong>Q1</strong><br>Apr-Jun',
                                target[1] + ' ' + dashBoardControllerScope.currencyVal[1] + ' <br><strong>Q2</strong><br>Jul-Sep',
                                target[2] + ' ' + dashBoardControllerScope.currencyVal[2] + ' <br><strong>Q3</strong><br>Oct-Dec',
                                target[3] + ' ' + dashBoardControllerScope.currencyVal[3] + ' <br><strong>Q4</strong><br>Jan-Mar'
                            ]
                        },
                        tooltip: {
                            fontSize: 14,
                            borderRadius: 3,
                            borderWidth: 0
                        },
                        plot: {
                            valueBox: [{
                                type: 'all',
                                color: '#000',
                                placement: 'goal',
                                text: '[%node-value / %node-goal-value]'
                            }]
                        },
                        series: [{
                            dataDragging: true,
                            values: dashBoardControllerScope.achived,
                            goals: target,
                            goal: {
                                backgroundColor: '#64b5f6',
                                borderWidth: 0,
                            },
                            rules: [{
                                rule: '%v == %g',
                                backgroundColor: '#81c784'
                            }, {
                                rule: '%v < %g/2',
                                backgroundColor: '#ef5350'
                            }, {
                                rule: '%v >= %g/2 && %v < %g',
                                backgroundColor: '#ffca28'
                            }, {
                                rule: '%v > %g', // if in between
                                backgroundColor: 'green'
                            }]
                        }]
                    };
                }
            }
        });
        dashBoardControllerScope.spinner = false;
    }

    function viewEmployee() {
        dashBoardControllerScope.spinner = true;
        employeeService.viewEmployee().then(function(response) {
            if (response.data.msg) {
                dashBoardControllerScope.spinner = false;
                dashBoardControllerScope.infoMsg = response.data.msg;
                dashBoardControllerScope.empList = '';
            } else {
                dashBoardControllerScope.empList = response.data;
                dashBoardControllerScope.spinner = false;
            }
        }, function(error_response) {
            console.log("error", error_response);
        });

    }

}
