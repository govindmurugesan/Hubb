angular
    .module('ePortal')
    .controller('headerController', headerController);

headerController.$inject = ['$scope', '$rootScope', '$location', 'usersService', '$cookies', 'employeeService', '$rootScope'];

function headerController($scope, $rootScope, $location, usersService, $cookies, employeeService, $rootScope) {
    var headerControllerScope = this;

    headerControllerScope.sideMenuToggle = sideMenuToggle;
    headerControllerScope.logout = logout;
    headerControllerScope.getLoginUserInfo=getLoginUserInfo;

    getLoginUserInfo();

    $scope.$watch(function() {
            return $rootScope.currentMenu },
        function() {
            headerControllerScope.selectedMenu = $rootScope.currentMenu;
        })


    function sideMenuToggle() {
        $rootScope.$emit("CallLeftSideMenuMethod", {});
    }

    function getLoginUserInfo() {
        employeeService.getEmployeeDetailsByEmail($cookies.getObject('saptaePortalCookies').email).then(function(response) {
            if (response.data.msg) {
                headerControllerScope.userFirstName = "";
                headerControllerScope.userLastName = ""
            } else {
                if (response.data) {
                    headerControllerScope.userFirstName = response.data.firstName;
                    headerControllerScope.userLastName = response.data.lastName;
                }
            }
        }, function(err) {
            console.log(err);
        })
    }


    function logout() {
        $rootScope.currentMenu = "";
        usersService.userLogout().then(function(response) {
            if (response.data.successMsg) {
                $cookies.remove('saptaePortalCookies');
                $location.url('/')
            }
        }, function(err) {
            console.log(err);
        })
    }

}
