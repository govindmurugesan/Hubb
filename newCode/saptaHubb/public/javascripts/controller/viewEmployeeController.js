angular
    .module('ePortal')
    .controller('viewEmployeeController', viewEmployeeController);

viewEmployeeController.$inject = ['$scope', '$rootScope', '$location', 'employeeService', 'configrationService', 'filterFilter', 'constantsService'];

function viewEmployeeController($scope, $rootScope, $location, employeeService, configrationService, filterFilter, constantsService) {
    var viewEmployeeControllerScope = this;

    getEmployeeList();
    getDepartment();
    getEmployeeType();
    getWorkLocationDetails();
    viewEmployee();

    viewEmployeeControllerScope.showEmployeeList = true;
    viewEmployeeControllerScope.showworkLocation = false;
    viewEmployeeControllerScope.showEmployee = false;
    viewEmployeeControllerScope.showDepartment = false;
    viewEmployeeControllerScope.showStatus = false;
    viewEmployeeControllerScope.showReport = false;
    viewEmployeeControllerScope.joiningDateCalender = false;

    viewEmployeeControllerScope.title = "Employees";
    var employeeInfoBackup;

    viewEmployeeControllerScope.showEmployeeDetail = showEmployeeDetail;
    viewEmployeeControllerScope.showEdit = showEdit;
    viewEmployeeControllerScope.employeeListView = employeeListView;
    viewEmployeeControllerScope.updateDetails = updateDetails;
    viewEmployeeControllerScope.gotoCreateCustomer = gotoCreateCustomer;
    viewEmployeeControllerScope.viewEmployee = viewEmployee;
    viewEmployeeControllerScope.openbirthdayCalender = openbirthdayCalender;
    viewEmployeeControllerScope.openJoiningDateCalender = openJoiningDateCalender;
    viewEmployeeControllerScope.hideAlertPopup = hideAlertPopup;
    viewEmployeeControllerScope.decline = decline;
    viewEmployeeControllerScope.selectedworkLocationFun = selectedworkLocationFun;
    viewEmployeeControllerScope.selectedEmployeeFun = selectedEmployeeFun;
    viewEmployeeControllerScope.selectedDepartmentFun = selectedDepartmentFun;
    viewEmployeeControllerScope.selectedStatusFun = selectedStatusFun;
    // viewEmployeeControllerScope.selectedReportFun = selectedReportFun;
    viewEmployeeControllerScope.getEmployeeByEmailOrName = getEmployeeByEmailOrName;
    viewEmployeeControllerScope.selectedRepoting = selectedRepoting;

    $rootScope.currentMenu = "View Employee";

    viewEmployeeControllerScope.statusList = [{
        status: constantsService.activeState,

    }, {
        status: constantsService.inactiveState,

    }];

    function selectedRepoting(reportingTo) {
        viewEmployeeControllerScope.spinner = true;
        viewEmployeeControllerScope.empInfo.reportingTo = {
            reportingEmployeeId: reportingTo._id,
            reportingEmployeeName: reportingTo.firstName + ' ' + reportingTo.middleName + ' ' + reportingTo.lastName
        };
        viewEmployeeControllerScope.empInfo.reporting = viewEmployeeControllerScope.empInfo.reportingTo.reportingEmployeeName;
        viewEmployeeControllerScope.keywordDepartment = "";
        viewEmployeeControllerScope.isEmployeeFound = false;
        viewEmployeeControllerScope.spinner = false;
    }

    function getEmployeeByEmailOrName(employee) {
        viewEmployeeControllerScope.spinner = true;
        employeeService.getEmployeeByEmailOrName(employee).then(function(response) {
            if (response.data.msg) {
                viewEmployeeControllerScope.employeeList = '';
                viewEmployeeControllerScope.spinner = false;
            } else {
                if (response.data) {
                    viewEmployeeControllerScope.employeeList = response.data;
                    viewEmployeeControllerScope.empInfo.reportingTo = {
                        reportingEmployeeId: '',
                        reportingEmployeeName: ''
                    };
                }
            }
            if (viewEmployeeControllerScope.employeeList.length > 0) {
                viewEmployeeControllerScope.isEmployeeFound = true;
                viewEmployeeControllerScope.employeeListNotFound = false;
            } else {
                viewEmployeeControllerScope.isEmployeeFound = false;
                viewEmployeeControllerScope.employeeListNotFound = true;
            }

        }, function(err) {
            console.log("Employee Error");
        });
        viewEmployeeControllerScope.spinner = false;
    }

    function updateDetails() {
        var s = angular.copy(viewEmployeeControllerScope.empInfo);
        viewEmployeeControllerScope.spinner = true;


        viewEmployeeControllerScope.empInfo.status = viewEmployeeControllerScope.selectedStatus;
        viewEmployeeControllerScope.empInfo.workLocation = viewEmployeeControllerScope.selectedWork;
        viewEmployeeControllerScope.empInfo.department = viewEmployeeControllerScope.selectedDepartment;

        viewEmployeeControllerScope.empInfo.employeeType = viewEmployeeControllerScope.selectedEmployeType;

        if (viewEmployeeControllerScope.empInfo.reporting == "" || viewEmployeeControllerScope.empInfo.reporting == undefined) {
            viewEmployeeControllerScope.empInfo.reportingTo = { reportingEmployeeName: '', reportingEmployeeId: '' };
        }
        employeeService.updateEmployeeDetails(viewEmployeeControllerScope.empInfo).then(function(response) {

            if (response.data.msg) {
                viewEmployeeControllerScope.infoMsg = response.data.msg;
            } else {
                if (response.data.successmsg) {
                    $scope.modalShown = true;
                    viewEmployeeControllerScope.alertMsg = "Employee Details updated successfully";
                    viewEmployeeControllerScope.showEmployeeList = true;
                    viewEmployeeControllerScope.employeeSearchKeyword = "";
                    viewEmployeeControllerScope.title = "Employees";
                    getEmployeeList();
                }

            }
        });
        viewEmployeeControllerScope.spinner = false;

    }

    function viewEmployee() {
        viewEmployeeControllerScope.spinner = true;
        employeeService.viewEmployee().then(function(response) {
            if (response.data.msg) {
                viewEmployeeControllerScope.spinner = false;
                viewEmployeeControllerScope.infoMsg = response.data.msg;
                viewEmployeeControllerScope.viewEmployeeList = '';
            } else {
                viewEmployeeControllerScope.viewEmployeeList = response.data;
                viewEmployeeControllerScope.spinner = false;
            }
        }, function(error_response) {
            console.log("error", error_response);
        });

    }

    function getEmployeeList() {
        viewEmployeeControllerScope.spinner = true;
        employeeService.viewEmployee().then(function(response) {

            if (response.data.msg) {
                viewEmployeeControllerScope.spinner = false;
                viewEmployeeControllerScope.infoMsg = response.data.msg;
                viewEmployeeControllerScope.employeeList = '';
            } else {
                if (response.data) {
                    viewEmployeeControllerScope.employeeList = response.data;
                    $scope.currentPage = 1;
                    $scope.totalItems = viewEmployeeControllerScope.employeeList.length;
                    $scope.entryLimit = 10;
                    $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

                    $scope.$watch('search', function(newVal, oldVal) {

                        $scope.filtered = filterFilter(viewEmployeeControllerScope.employeeList, newVal);
                        $scope.totalItems = $scope.filtered.length;
                        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
                        $scope.currentPage = 1;
                    }, true);

                    viewEmployeeControllerScope.spinner = false;

                }
            }

        }, function(error_response) {
            console.log("error", error_response);
        });
    }

    function showEmployeeDetail(employeeDetails) {

        viewEmployeeControllerScope.empInfo = "";
        employeeInfoBackup = "";

        viewEmployeeControllerScope.showEditButton = true;
        viewEmployeeControllerScope.nonEditableField = true;
        viewEmployeeControllerScope.edititemtrue = true;
        viewEmployeeControllerScope.edititem = false;
        viewEmployeeControllerScope.showEmployeeList = false;
        viewEmployeeControllerScope.employeeSearchKeyword = "";
        viewEmployeeControllerScope.title = "Employee Detail"
        employeeInfoBackup = angular.copy(employeeDetails);
        viewEmployeeControllerScope.empInfo = angular.copy(employeeDetails);
        viewEmployeeControllerScope.selectedStatus = { status: employeeDetails.status };
        viewEmployeeControllerScope.selectedWork = employeeDetails.workLocation;
        viewEmployeeControllerScope.selectedEmployeType = employeeDetails.employeeType;
        viewEmployeeControllerScope.selectedDepartment = employeeDetails.department;
        // viewEmployeeControllerScope.selectedReport = { firstName: employeeDetails.reportingTo[0].reportingEmployeeName };
        viewEmployeeControllerScope.empInfo.reporting = employeeDetails.reportingTo[0].reportingEmployeeName;
        viewEmployeeControllerScope.fieldEditable = true;
    }




    function getWorkLocationDetails() {
        viewEmployeeControllerScope.infoMsg = "";
        viewEmployeeControllerScope.spinner = true;
        configrationService.getWorkLocationDetails().then(function(response) {
            if (response.data.msg) {
                viewEmployeeControllerScope.spinner = false;
                viewEmployeeControllerScope.infoMsg = response.data.msg;
                viewEmployeeControllerScope.worklocationListType = '';
            } else {

                if (response.data.length > 0) {
                    viewEmployeeControllerScope.worklocationListType = response.data;
                    viewEmployeeControllerScope.spinner = false;
                    getEmployeeType();
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getEmployeeType() {
        viewEmployeeControllerScope.infoMsg = "";
        viewEmployeeControllerScope.spinner = true;
        configrationService.getEmployeeType().then(function(response) {
            if (response.data.msg) {
                viewEmployeeControllerScope.spinner = false;
                viewEmployeeControllerScope.infoMsg = response.data.msg;
                viewEmployeeControllerScope.employeeTypeList = '';
            } else {

                if (response.data.length > 0) {
                    viewEmployeeControllerScope.employeeTypeList = response.data;
                    viewEmployeeControllerScope.spinner = false;
                    getDepartment();
                }
            }
        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function getDepartment() {
        viewEmployeeControllerScope.infoMsg = "";
        viewEmployeeControllerScope.spinner = true;
        configrationService.getDepartment().then(function(response) {
            if (response.data.msg) {
                viewEmployeeControllerScope.spinner = false;
                viewEmployeeControllerScope.infoMsg = response.data.msg;
                viewEmployeeControllerScope.departmentList = '';
            } else {

                if (response.data.length > 0) {
                    viewEmployeeControllerScope.departmentList = response.data;

                    viewEmployeeControllerScope.spinner = false;
                }
            }

        }, function(error_response) {
            console.log("error_response", error_response);
        });
    }

    function showEdit() {
        getDepartment();
        viewEmployeeControllerScope.spinner = true;
        viewEmployeeControllerScope.nonEditableField = false;
        viewEmployeeControllerScope.edititemtrue = true;
        viewEmployeeControllerScope.edititem = true;
        viewEmployeeControllerScope.showEditButton = false;
        // if (viewEmployeeControllerScope.selectedReport.firstName == "") { viewEmployeeControllerScope.selectedReport = { firstName: "Select Reporting" }; }
        viewEmployeeControllerScope.spinner = false;
    }

    function employeeListView() {
        viewEmployeeControllerScope.spinner = true;
        viewEmployeeControllerScope.showEmployeeList = true;
        viewEmployeeControllerScope.employeeSearchKeyword = "";
        viewEmployeeControllerScope.title = "Employees";
        getEmployeeList();
        viewEmployeeControllerScope.spinner = false;
    }

    function gotoCreateCustomer() {
        $location.path('/createEmployee');
    }


    function hideAlertPopup() {
        $scope.modalShown = false;
    }


    function decline() {
        employeeListView();
    }

    function openbirthdayCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewEmployeeControllerScope.birthdayCalender = !viewEmployeeControllerScope.birthdayCalender;
    }

    function openJoiningDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        viewEmployeeControllerScope.joiningDateCalender = !viewEmployeeControllerScope.joiningDateCalender;
    }

    function selectedworkLocationFun(workPlace) {
        viewEmployeeControllerScope.showworkLocation = !viewEmployeeControllerScope.showworkLocation;
        viewEmployeeControllerScope.selectedWork = workPlace.worklocation;
        viewEmployeeControllerScope.keywordworklocation="";
    }

    function selectedEmployeeFun(emptypelist) {
        viewEmployeeControllerScope.showEmployee = !viewEmployeeControllerScope.showEmployee;
        viewEmployeeControllerScope.selectedEmployeType = emptypelist.name;
        viewEmployeeControllerScope.keywordemployeeType="";
    }

    function selectedDepartmentFun(deplist) {
        viewEmployeeControllerScope.showDepartment = !viewEmployeeControllerScope.showDepartment;
        viewEmployeeControllerScope.selectedDepartment = deplist.name;
    }

    function selectedStatusFun(status) {
        viewEmployeeControllerScope.showStatus = !viewEmployeeControllerScope.showStatus;
        viewEmployeeControllerScope.selectedStatus = status;

    }

    // function selectedReportFun(emplist) {
    //     viewEmployeeControllerScope.showReport = !viewEmployeeControllerScope.showReport;
    //     viewEmployeeControllerScope.selectedReport = emplist;
    // }

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd MMMM yyyy', 'MMMM d, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}
