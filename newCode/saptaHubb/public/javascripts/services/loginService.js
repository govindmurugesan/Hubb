angular
    .module('ePortal')
    .factory('loginService', loginService);

loginService.$inject = ['$q', '$http', 'apiRouterService'];

function loginService($q, $http, apiRouterService) {
    var service = {
        userlogin: userlogin,
    };

    return service;

    function userlogin(loginDetail) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.userLogin + '?email=' + loginDetail.email + '&password=' + loginDetail.password
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }



}
