angular
    .module('ePortal')
    .factory('offerService', offerService);

offerService.$inject = ['$q', '$http', 'apiRouterService'];

function offerService($q, $http, apiRouterService) {
    var service = {
        addOffer: addOffer,
        getOffer: getOffer,
        updateOffer: updateOffer

    };

    return service;

    function addOffer(offerDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.offer,
            data: offerDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getOffer() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.offer
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }


    function updateOffer(offerDetail) {

        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.offer,
            data: offerDetail
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;

    }



}
