angular
    .module('ePortal')
    .factory('requirementService', requirementService);

requirementService.$inject = ['$q', '$http', 'apiRouterService'];

function requirementService($q, $http, apiRouterService) {
    var service = {
        addRequirement: addRequirementDetail,
        getRequirement:getRequirement,
        updateRequirementDetail:updateRequirementDetail
    };

    return service;

    function addRequirementDetail(requirementDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.requirement,
            data: requirementDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

     function getRequirement() {

        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.requirement
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }


 function updateRequirementDetail(requirementDetail) {
    console.log("service",requirementDetail)
    var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.requirement,
            data: requirementDetail
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;

 }

 
 // function updateProjectDetail(updateProjectDetail) {
    
 //    var deferred = $q.defer();
 //        $http({
 //            method: apiRouterService.METHOD_PUT,
 //            url: apiRouterService.project,
 //            data: updateProjectDetail
 //        }).then(function(dataReturns) {
 //            deferred.resolve(dataReturns);
 //        }, function(error) {
 //            console.log('error', error);
 //            deferred.reject(error);
 //        });
 //        return deferred.promise;

 // }




}
