angular
    .module('ePortal')
    .factory('vendorService', vendorService);

vendorService.$inject = ['$q', '$http', 'apiRouterService'];

function vendorService($q, $http, apiRouterService) {
    var service = {
        addVendor: addVendor,
        getVendorList: getVendorList,
        updateVendorDetail: updateVendorDetail,

    };

    return service;

    function addVendor(vendorDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.vendor,
            data: vendorDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getVendorList() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.vendor
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateVendorDetail(vendorDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.vendor,
            data: vendorDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }



}
