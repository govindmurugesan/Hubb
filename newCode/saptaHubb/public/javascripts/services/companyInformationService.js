angular
    .module('ePortal')
    .factory('companyInformationService', companyInformationService);

companyInformationService.$inject = ['$q', '$http', 'apiRouterService'];

function companyInformationService($q, $http, apiRouterService) {
    var service = {

        getCompanyInformation: getCompanyInformation,
        updateCompanyInformation: updateCompanyInformation,

    };
    return service;

    function getCompanyInformation() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.companyInformation
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateCompanyInformation(updateCompanyInformation) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.companyInformation,
            data: updateCompanyInformation
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
}
