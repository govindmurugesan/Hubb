angular
    .module('ePortal')
    .factory('customerService', customerService);

customerService.$inject = ['$q', '$http', 'apiRouterService'];

function customerService($q, $http, apiRouterService) {
    var service = {
        addCustomer: addCustomer,
        getCustomerList: getCustomerList,
        updateCustomerDetail: updateCustomerDetail,

    };

    return service;

    function addCustomer(customerDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.customer,
            data: customerDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getCustomerList() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.customer
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateCustomerDetail(customerDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.customer,
            data: customerDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }



}
