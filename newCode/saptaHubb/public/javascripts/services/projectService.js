angular
    .module('ePortal')
    .factory('projectService', projectService);

projectService.$inject = ['$q', '$http', 'apiRouterService'];

function projectService($q, $http, apiRouterService) {
    var service = {
        addProject: addProject,
        getProject: getProject,
        updateProjectDetail: updateProjectDetail,
        addProjectCost: addProjectCost,
        getProjectByReference: getProjectByReference,
        updateProjectCost: updateProjectCost,
        deletePaymentTermByIndex: deletePaymentTermByIndex

    };

    return service;

    function addProject(projectDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.project,
            data: projectDetails
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getProject() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.project
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    function updateProjectDetail(updateProjectDetail) {

        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.project,
            data: updateProjectDetail
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;

    }


    function addProjectCost(projectCostDetail) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.projectCost,
            data: projectCostDetail
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateProjectCost(project) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.projectCost,
            data: project
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getProjectByReference(project) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getProjectByReference,
            params: {
                project: project
            }
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function deletePaymentTermByIndex(paymentID,projectID) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.deletePaymentTermByIndex,
            params: {
                paymentID: paymentID,
                projectID: projectID
            }
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }



}
