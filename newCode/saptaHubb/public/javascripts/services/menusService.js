angular
    .module('ePortal')
    .factory('menusService', menusService);

menusService.$inject = ['$q', '$http', 'apiRouterService'];

function menusService($q, $http, apiRouterService) {
    var service = {
        getMenus: getMenus,
        getEmpRole: getEmpRole,
        /*saveRole : saveRole,*/
        updateAssignedRoles: updateAssignedRoles,
        getAssignedRoles: getAssignedRoles,

    };

    return service;

    function getMenus(loginDetail) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getMenus
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getEmpRole() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getEmpRole
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    /*function saveRole(chethan){
        console.log("shdfvjsdvf");
        var deferred = $q.defer();
        var saveRole1 = { 'selectRole' : chethan.selectRole,
                            'roleText' : chethan.roleText,
                            'roleCheck' : chethan.roleCheck
                        };
        console.log("friends",saveRole1);
        
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.saveRole,
            data : saveRole1
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }*/

    function updateAssignedRoles(detailsToUpdate) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.menuAssignment,
            data: detailsToUpdate
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getAssignedRoles(roleId) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.menuAssignment + '?roleId=' + roleId
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

}
