angular
    .module('ePortal')
    .factory('usersService', usersService);

usersService.$inject = ['$q', '$http', 'apiRouterService'];

function usersService($q, $http, apiRouterService) {
    var service = {
        userlogin: userlogin,
        forgotPassword: forgotPassword,
        userLogout: userLogout,
        addUser: addUser,
        getUser: getUser,
        getEmployeeRole: getEmployeeRole,
        getUserInformation: getUserInformation,
        getInviteInformation: getInviteInformation,
        updateUserType: updateUserType,
        activatePortal: activatePortal
    };

    return service;

    function userlogin(loginDetail) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.userLogin + '?email=' + loginDetail.email + '&password=' + loginDetail.password
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function forgotPassword(mailId) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.forgotPassword + '?email=' + mailId
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function userLogout() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.userLogout
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function addUser(invite) {
        var deferred = $q.defer();
        var userInvite = {
            email: invite.email,
            role: invite.role
        }
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.addUser,
            data: invite
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getUser() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getUser
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getEmployeeRole() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.employeeRole
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getUserInformation() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getUserInformation
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getInviteInformation() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getInviteInformation
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateUserType(updateUserType) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.updateUserType,
            data: updateUserType
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function activatePortal(detailsToUpdate){
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.activatePortal,
            data: detailsToUpdate
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

}
