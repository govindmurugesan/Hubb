angular
    .module('ePortal')
    .factory('salesReportService', salesReportService);

salesReportService.$inject = ['$q', '$http', 'apiRouterService'];

function salesReportService($q, $http, apiRouterService) {

    var salesReportService = {
        setTarget: setTarget,
        updateSalesTarget:updateSalesTarget,
        getSalesReport: getSalesReport,
        checkBDMSalesRecord: checkBDMSalesRecord
    };

    return salesReportService;


    function setTarget(salesReportDetail) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.salesReport,
            data: salesReportDetail
        }).then(function(response) {

            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateSalesTarget(targetDetail) {

        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.salesReport,
            data: targetDetail
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;

    }

    function getSalesReport(financialYearStart, financialYearEnd) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.salesReport,
            params: {
                financialYearStart: financialYearStart,
                financialYearEnd: financialYearEnd
            }
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function checkBDMSalesRecord(financialYearStart, financialYearEnd, bdm) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.checkBDMSalesRecord,
            params: {
                financialYearStart: financialYearStart,
                financialYearEnd: financialYearEnd,
                bdm: bdm
            }
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
}
