angular
    .module('ePortal')
    .factory('employeeService', employeeService);

employeeService.$inject = ['$q', '$http', 'apiRouterService'];

function employeeService($q, $http, apiRouterService) {
    var service = {
        createEmployee: createEmployee,
        viewEmployee: viewEmployee,
        getEmployeeDetailsByEmail: getEmployeeDetailsByEmail,
        updateEmployeeDetails: updateEmployeeDetails,
        getEmployeeByEmailOrName: getEmployeeByEmailOrName
    };
    return service;

    function createEmployee(employeeDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.employee,
            data: employeeDetails
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function updateEmployeeDetails(employeeDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.employee,
            data: employeeDetails
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        })
        return deferred.promise;

    }

    function viewEmployee() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.employee,
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function getEmployeeDetailsByEmail(email) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getEmployeeByReference + '?email=' + email,
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function getEmployeeByEmailOrName(employee) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.getEmployeeByEmailOrName + '?emp=' + employee,
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

}
