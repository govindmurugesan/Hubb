angular
    .module('ePortal')
    .factory('billServices', billServices);

billServices.$inject = ['$q', '$http', 'apiRouterService'];

function billServices($q, $http, apiRouterService) {

     var billService = {
        getBill: getBill,
        addBill: addBill,
        updateBill: updateBill,
        newBillNumber: newBillNumber
    };

    return billService;

    function newBillNumber() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.newBillNumber
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            console.log('bill error ', error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

    function updateBill(updatedBillDetails) {
        
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_PUT,
            url: apiRouterService.bills,
            data: updatedBillDetails
        }).then(function(response) {

            deferred.resolve(response);
        }, function(error) {
            console.log('updating bill service error ', error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

    function getBill() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.bills
        }).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            console.log('bill error ', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function addBill(billDetails) {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_POST,
            url: apiRouterService.bills,
            data: billDetails
        }).then(function(response) {

            deferred.resolve(response);
        }, function(error) {
            console.log('adding bill service error ', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
}
