angular
.module('ePortal')
.factory('configrationService', configrationService);

configrationService.$inject = ['$q', '$http', 'apiRouterService'];

function configrationService($q, $http, apiRouterService) {
    var service = {

        addCustomerType: addCustomerType,
        addEmployeeType: addEmployeeType,
        addDepartment: addDepartment,
        addTimeOffType: addTimeOffType,
        addInvoiceType: addInvoiceType,
        addPaymentTerm: addPaymentTerm,
        addVendorType: addVendorType,
        addInvoicePaymentTerm: addInvoicePaymentTerm,
        addEmployeeRole: addEmployeeRole,
        addRole : addRole, 
        getCustomerType: getCustomerType,
        getVendorType: getVendorType,
        getDepartment: getDepartment,            
        addCountry : addCountry,
        addState : addState,
        addWorkLocation : addWorkLocation,         
        getEmployeeType: getEmployeeType,
        getTimeOffType: getTimeOffType,
        getInvoiceType: getInvoiceType,
        getPaymentTerm: getPaymentTerm,
        getInvoicePaymentTerm: getInvoicePaymentTerm,
        getEmployeeRole: getEmployeeRole,
        getRoles : getRoles,
        getCountryDetals : getCountryDetals,
       /* getStateDetals : getStateDetals,*/
        getWorkLocationDetails : getWorkLocationDetails,
        updateCustomerType: updateCustomerType,
        updateEmployeeType : updateEmployeeType,
        updateTimeOffType: updateTimeOffType,
        updateInvoiceType: updateInvoiceType,
        updatePaymentTerm: updatePaymentTerm,
        updateInvoicePaymentTerm: updateInvoicePaymentTerm,
        updateEmployeeRole: updateEmployeeRole,
         updateVendorType: updateVendorType,
        updateDepartment: updateDepartment,
        deleteCustomerType : deleteCustomerType,
        deleteEmployeeType : deleteEmployeeType,
        deleteTimeOffType : deleteTimeOffType,
         deleteVendorType: deleteVendorType,
        deleteInvoiceType : deleteInvoiceType,
        deletePaymentTerm : deletePaymentTerm,
        deleteInvoicePaymentTerm : deleteInvoicePaymentTerm,
        deleteEmployeeRoleType : deleteEmployeeRoleType,
        deleteDepartment : deleteDepartment,
        deleteCountry : deleteCountry,
        deleteState : deleteState,
        deleteWorkLocation : deleteWorkLocation,
        updateCountryStatus : updateCountryStatus,
        updateWorkLocationStatus : updateWorkLocationStatus,
        updateStateStatus : updateStateStatus,
        getCurrencyType: getCurrencyType
    };

    return service;


    function addCustomerType(customerType){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.customerType,
            data: customerType
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function addEmployeeType(emplyeeType){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url :  apiRouterService.employeeType,
            data: emplyeeType
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }


    function addVendorType(ventorType){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.ventorType,
            data: ventorType
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function addTimeOffType(timeOffType){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url :  apiRouterService.timeOffType,
            data: timeOffType
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

     function addInvoiceType(invoiceType){
         var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.invoiceType,
            data : invoiceType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

function addPaymentTerm(paymentTerm){
         var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.paymentTerm,
            data : paymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
 
function addInvoicePaymentTerm(invoicePaymentTerm){
         var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.invoicePaymentTerm,
            data : invoicePaymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function addEmployeeRole(employeeRole){
         var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.employeeRole,
            data : employeeRole
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function addDepartment(department){
         var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.department,
            data : department
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    function addRole(role){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_POST,
            url :  apiRouterService.roles,
            data : role,
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }


    function getCurrencyType() {
        var deferred = $q.defer();
        $http({
            method: apiRouterService.METHOD_GET,
            url: apiRouterService.country
        }).then(function(dataReturns) {
            deferred.resolve(dataReturns);
        }, function(error) {
            console.log('error', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }


    function getRoles(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.roles
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getCustomerType(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.customerType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

      function getVendorType(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.ventorType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

    function getEmployeeType(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.employeeType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getTimeOffType(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.timeOffType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getInvoiceType(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.invoiceType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
   
   function getPaymentTerm(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.paymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    function getInvoicePaymentTerm(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.invoicePaymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getEmployeeRole(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.employeeRole
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getDepartment(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.department
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    function updateEmployeeType(newEmployeeType){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.employeeType,
            data: newEmployeeType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateCustomerType(newCustomerType){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.customerType,
            data: newCustomerType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateTimeOffType(newTimeOffType){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.timeOffType,
            data: newTimeOffType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function updateInvoiceType(newInvoiceType){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.invoiceType,
            data: newInvoiceType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

function updatePaymentTerm(newPaymentTerm){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.paymentTerm,
            data: newPaymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

function updateInvoicePaymentTerm(newInvoicePaymentTerm){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.invoicePaymentTerm,
            data: newInvoicePaymentTerm
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

function updateEmployeeRole(newEmployeeRole){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.employeeRole,
            data: newEmployeeRole
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    function updateDepartment(newDepartment){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.department,
            data: newDepartment
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    function deleteEmployeeType(employeeTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.employeeType+'?employeeTypeId='+employeeTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;

    }

    function deleteCustomerType(customerTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.customerType+'?customerTypeId='+customerTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function deleteTimeOffType(timeOffTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.timeOffType+'?timeOffTypeId='+timeOffTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }

      function deleteInvoiceType(invoiceTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.invoiceType+'?invoiceTypeId='+invoiceTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    function deletePaymentTerm(paymentTermId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.paymentTerm+'?paymentTermId='+paymentTermId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    function deleteInvoicePaymentTerm(invoicePaymentTermId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.invoicePaymentTerm+'?invoicePaymentTermId='+invoicePaymentTermId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    function deleteEmployeeRoleType(deleteEmployeeRoleTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.employeeRole+'?deleteEmployeeRoleTypeId='+deleteEmployeeRoleTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function addCountry(countryDetails){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.country,
            data: countryDetails
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

     function addState(stateDetails){
        console.log("stateDetails",stateDetails);
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.state,
            data: stateDetails
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

    function addWorkLocation(workLocationDetails){
        var deferred = $q.defer();
        $http({
            method : apiRouterService.METHOD_POST,
            url : apiRouterService.workLocation,
            data: workLocationDetails
        }).then(function(response){
            deferred.resolve(response);
        },function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }



     function getCountryDetals(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.country
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

/*    function getStateDetals(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.state
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;

    }*/

    function getWorkLocationDetails(){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_GET,
            url :  apiRouterService.workLocation
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;

    }

    
     function deleteCountry(countryReferenceId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.country+'?countryReferenceId='+countryReferenceId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }

     function deleteState(stateReferenceId,countryReferenceId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.state+'?stateReferenceId='+stateReferenceId+'&countryReferenceId='+countryReferenceId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }

      function deleteWorkLocation(workLocationReferenceId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.workLocation+'?workLocationReferenceId='+workLocationReferenceId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    
     function deleteDepartment(departmentId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.department+'?departmentId='+departmentId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        })
        return deferred.promise;
    }

        function deleteVendorType(vendorTypeId){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_DELETE,
            url :  apiRouterService.ventorType+'?vendorTypeId='+vendorTypeId
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }


    
     function updateVendorType(newVendorType){ 
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.ventorType,
            data: newVendorType
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
     function updateCountryStatus(countryDetails){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.country,
            data: countryDetails
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

     function updateStateStatus(stateDetails){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.state,
            data: stateDetails
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

     function updateWorkLocationStatus(workLocationDetails){
        var deferred = $q.defer();
        $http({
            method :  apiRouterService.METHOD_PUT,
            url :  apiRouterService.workLocation,
            data: workLocationDetails
        }).then(function(dataReturns){
            deferred.resolve(dataReturns);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }
}