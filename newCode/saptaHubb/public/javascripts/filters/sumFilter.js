angular.module('ePortal').filter('sumOfValue', function() {

        console.log('called sum of value');
    return function(data, key1,key2,key3,key4) {
        if (angular.isUndefined(data) || angular.isUndefined(key1) || angular.isUndefined(key2)|| angular.isUndefined(key3)|| angular.isUndefined(key4))
            return 0;
        var sum = 0;

        angular.forEach(data, function(v, k) {
            sum = sum + (parseInt(v[key1])+parseInt(v[key2])+parseInt(v[key3])+parseInt(v[key4]));
        });
        console.log('sum',sum);
        return sum;
    }
})