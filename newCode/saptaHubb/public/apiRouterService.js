(function() {
    'use strict';

    var apiRoutesUrl = {
        METHOD_GET: 'GET',
        METHOD_POST: 'POST',
        METHOD_PUT: 'PUT',
        METHOD_DELETE: 'DELETE',

        //invite user
        addUser: 'usersService/inviteUser',


        //userService
        userLogin: 'usersService/login',
        forgotPassword: 'usersService/forgotpassword',
        userLogout: 'usersService/logout',

        // menus
        getMenus: 'menus/getMenus',
        getEmpRole: 'menus/getEmpRole',
        saveRole: 'menus/saveRole',

        // users
        updateUserType: 'users/updateUserType',
        activatePortal: 'users/activatePortal',
        getUserInformation: 'users/getUserInformation',
        getInviteInformation: 'users/getInviteInformation',

        //Configaration
        roles: 'configaration/roles',
        employeeType: 'configaration/employeetype',
        customerType: 'configaration/customertype',
        ventorType: 'configaration/ventortype',
        timeOffType: 'configaration/timeofftype',
        invoiceType: 'configaration/invoicetype',
        paymentTerm: 'configaration/paymentterm',
        invoicePaymentTerm: 'configaration/invoicepaymentterm',
        country: 'configaration/country',
        state: 'configaration/state',
        workLocation: 'configaration/worklocation',
        employeeRole: 'configaration/employeerole',
        department: 'configaration/Department',

        // bills
        bills: 'bills/bills',
        newBillNumber: 'bills/newbillnumber',

        //employee
        employee: 'employee/employee',
        getEmployeeByReference: 'employee/getemployee',
        getEmployeeByEmailOrName: 'employee/getEmployeeByEmailOrName',

        // addViewEmployee: 'viewEmployee/viewEmployee',

        //companyInformation

        /*addNewCompanyInfo: 'companyInformation/addNewCompanyInfo',

        getCountryList: 'companyInformation/getCountryList',

        getStateList: 'companyInformation/getStateList',

        getCityList: 'companyInformation/getCityList',*/


        //updateCompanyInformation: 'companyInformation/updateCompanyInformation',

        // companyInformation
        companyInformation: 'companyInformation/CompanyInformation',

        //customer
        customer: 'customer/customerdetails',

        //vendor
        vendor: 'vendor/vendordetails',

        // menuAssignment
        menuAssignment: 'menus/roleprivileges',

        // project
        project: 'project/project',
        projectCost: 'project/projectCost',
        getProjectByReference: 'project/getProjectByReference',
        deletePaymentTermByIndex:'project/deletePaymentTermByIndex',

        // requirement
        requirement: 'requirement/requirement',

        // offer
        offer: 'offer/offer',

        // lead
        lead: 'lead/lead',
        followup: 'lead/followup',
        todo: 'lead/todo',
        followupContact: 'lead/followupContact',

        // salesReport
        salesReport: 'salesReport/salesReport',
        checkBDMSalesRecord: 'salesReport/checkBDMSalesRecord'

    };

    angular.module('ePortal').constant('apiRouterService', apiRoutesUrl);

})();
