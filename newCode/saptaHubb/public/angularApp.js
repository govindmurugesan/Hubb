var app = angular.module('ePortal', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'moment-picker','zingchart-angularjs'])
    .config(['momentPickerProvider', function(momentPickerProvider) {
        momentPickerProvider.options({
            leftArrow: '&lt;',
            rightArrow: '&gt;',
            secondsEnd: 0
        });
    }])

//var app = angular.module('saptahubb', ['ngRoute','ngCookies','checklist-model']);
