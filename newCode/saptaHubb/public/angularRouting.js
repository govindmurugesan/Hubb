angular.module('ePortal')
    .config(function config($routeProvider, $httpProvider) {
        $routeProvider.
        when('/', {
            resolve: {
                load: function($cookies) {
                    $cookies.remove('saptaePortalCookies');
                }
            },
            templateUrl: 'templates/login.html',
            controller: 'loginController as loginControllerScope'
        }).
        when('/dashboard', {
            templateUrl: 'templates/dashboard.html',
            controller: 'dashBoardController as dashBoardControllerScope'
        }).
        when('/roles', {
            templateUrl: 'templates/roles.html',
            controller: 'rolesController as rolesControllerScope'
        }).
        when('/users', {
            templateUrl: 'templates/users.html',
            controller: 'usersController as usersControllerScope'
        }).
        when('/configuration/:getText', {
            // url: '/configuration/:getText',
            templateUrl: 'templates/configuration.html',
            controller: 'configurationController as configurationControllerScope'
        }).
        when('/createEmployee', {
            templateUrl: 'templates/createEmployee.html',
            controller: 'createEmployeeController as createEmployeeControllerScope'
        }).
        when('/viewEmployee', {
            templateUrl: 'templates/viewEmployee.html',
            controller: 'viewEmployeeController as viewEmployeeControllerScope'
        }).
        when('/companyInformation', {
            templateUrl: 'templates/companyInformation.html',
            controller: 'companyInformationController as companyInformationControllerScope'
        }).
        when('/createCustomer', {
            templateUrl: 'templates/createCustomer.html',
            controller: 'createCustomerController as createCustomerControllerScope'
        }).
        when('/viewCustomer', {
            templateUrl: 'templates/viewCustomer.html',
            controller: 'viewCustomerController as viewCustomerControllerScope'
        }).
        when('/createVendor', {
            templateUrl: 'templates/createVendor.html',
            controller: 'createVendorController as createVendorControllerScope'
        }).
        when('/viewVendor', {
            templateUrl: 'templates/viewVendor.html',
            controller: 'viewVendorController as viewVendorControllerScope'
        }).
        when('/newBill', {
            templateUrl: 'templates/createBill.html',
            controller: 'createBillController as createBillControllerScope'
        }).
        when('/viewBill', {
            templateUrl: 'templates/viewBill.html',
            controller: 'viewBillController as viewBillControllerScope'
        }).
        when('/createProject', {
            templateUrl: 'templates/createProject.html',
            controller: 'createProjectController as createProjectControllerScope'
        }).
        when('/viewProject', {
            templateUrl: 'templates/viewProject.html',
            controller: 'viewProjectController as viewProjectControllerScope'
        }).
        when('/projectCost', {
            templateUrl: 'templates/createProjectCost.html',
            controller: 'projectCostController as projectCostControllerScope'
        }).
        when('/createRequirement', {
            templateUrl: 'templates/createRequirement.html',
            controller: 'requirementController as requirementControllerScope'
        }).
        when('/viewRequirement', {
            templateUrl: 'templates/viewRequirement.html',
            controller: 'requirementController as requirementControllerScope'
        }).
        when('/viewOffers', {
            templateUrl: 'templates/viewOffers.html',
            controller: 'offersController as offersControllerScope'
        }).
        when('/createOffers', {
            templateUrl: 'templates/createOffers.html',
            controller: 'offersController as offersControllerScope'
        }).
        when('/newLead', {
            templateUrl: 'templates/newLead.html',
            controller: 'leadController as leadControllerScope'
        }).
        when('/viewLead', {
            templateUrl: 'templates/viewLead.html',
            controller: 'leadController as leadControllerScope'
        }).
        when('/viewSalesReport', {
            templateUrl: 'templates/salesReport.html',
            controller: 'salesReportController as salesReportControllerScope'
        }).
        otherwise({
            redirectTo: '/'
        });
        $httpProvider.interceptors.push('TokenInterceptor');
    });

angular.module('ePortal')
    .factory('TokenInterceptor',
        function($q, $location, $cookies) {
            return {
                'request': function(config) {
                    config.headers = config.headers || {};
                    if ($cookies.getObject('saptaePortalCookies')) {
                        if ($cookies.getObject('saptaePortalCookies').token) {
                            config.headers.Authorization = $cookies.getObject('saptaePortalCookies').token;
                            config.headers.Accept = '*';
                        }

                    }
                    return config || $q.when(config);;
                },
                'responseError': function(response) {
                    if (response.status === 401 || response.status === 403) {
                        $cookies.remove('saptaePortalCookies');
                        $location.path('/');
                    }
                    return $q.reject(response);
                }
            };
        });
