(function() {
    'use strict';

    var constantsService_prod = {

    };

    var constantsService_dev = {
        //'url': 'http://localhost:3000',
        activeState: 'active',
        inactiveState: 'inactive',
        superAdmin: 'Super Admin',
        pending: 'Pending',
        cleared: 'Cleared',
        leadStatus: ['Qualified', 'Rejected', 'In Progress']

        //defultInvitedUserRole: 'Standard User'
        /*MEDIAINACTIVECODE: 'in',
        VERIFICATIONPENDING: 'vp',
        NEWSPAPER : 'newspaper'*/


    };

    angular.module("ePortal").constant('constantsService', constantsService_dev);

})();
