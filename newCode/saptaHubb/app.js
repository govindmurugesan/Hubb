var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var index = require('./routes/index');
var passwordReset = require('./routes/passwordReset');

var app = express();
 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var db = require('./dbModels/dbConnection');


app.use('/passwordReset', passwordReset);
app.use('/', index);

// Routes
var companyInformation = require('./server/companyInformation');
var authentication = require('./server/authentication');
var configaration = require('./server/configaration');
var employee = require('./server/employee');
//var viewEmployee = require('./server/viewEmployee');
var menus = require('./server/menus');
var customer = require('./server/customer');
var vendor = require('./server/vendor');
var users = require('./server/users');
var bills = require('./server/bills');
var project = require('./server/project');
var requirement = require('./server/requirement');
var offer = require('./server/offer');
var lead = require('./server/lead');
var salesReport= require('./server/salesReport');

app.use('/companyInformation',companyInformation);
app.use('/usersService', authentication);
app.use('/configaration',configaration);
app.use('/employee',employee);
//app.use('/viewEmployee',viewEmployee);
app.use('/menus',menus);
app.use('/customer',customer);
app.use('/vendor',vendor);
app.use('/users',users);
app.use('/bills',bills);
app.use('/project',project);
app.use('/requirement',requirement);
app.use('/offer',offer);
app.use('/lead',lead);
app.use('/salesReport',salesReport);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
