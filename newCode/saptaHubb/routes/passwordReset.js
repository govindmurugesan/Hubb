var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('passwordReset', { token: req.query.token ,email: req.query.email, temp: req.query.tokenId});
});

module.exports = router;
