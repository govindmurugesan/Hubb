var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var users = new Schema({
    name: {
        first: String,
        last: String
    },

    password: {type: String},

    email: [{
        address: String,
    }],

    phone: [{
        mobilenumber: String,
    }],

    gender : {type: String},

    photos:  {type: String},

    status: {type: String},

    role: {},
    autonticationInfo: [{
        token: {type: String},
        status: {type: String},
        temppassword:{type: String}
    }],

    joinDate: Date,
    createdOn: Date,
    updatedon: {type: Date, default: Date.now}

},{strict: false});


module.exports = mongoose.model('users', users);