var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var countryDetails = new Schema({
	name: {type: String},
	state: [{
		name : {type: String},
		status: {type: String}
	}],
	currencyType: {type: String},
	countryCode: {type: String},
	callingCode: {type: String},
	creationDate:Date,
	updatedon: Date,
	status: {type: String}

	
},{strict: false})

module.exports = mongoose.model('country',countryDetails);
