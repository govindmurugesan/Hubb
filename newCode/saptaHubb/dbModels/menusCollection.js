var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var menus = new Schema({
   	menuname:  {type: String},
    status:  {type: String},
    submenu: {},
    updatedon: {type: Date, default: Date.now}

},{strict: false});

module.exports = mongoose.model('menus', menus);