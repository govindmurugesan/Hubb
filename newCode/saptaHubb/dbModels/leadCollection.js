var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var leadCollection = new Schema({
    leadName: { type: String },
    mobileNumber: { type: String },
    phoneNumber: { type: String },
    email: { type: String },
    address: { type: String },
    followup: [{
        contactPerson: { type: String },
        designation: { type: String },
        im: { type: String },
        mobileNumber: { type: String },
        phoneNumber: { type: String },
        email: { type: String },
        dateOfContact: { type: Date },
        dateOfContactTime: { type: Date },
        followupDate: { type: Date },
        followupTime: { type: Date },
        remarks: { type: String },
        addedEmployee: { type: String },
        addedEmployeeID: { type: String },
        updatedOn: { type: Date },
        scopeOfWork: { type: String }
    }],
    followupContact: [{
        contactPerson: { type: String },
        designation: { type: String },
        im: { type: String },
        mobileNumber: { type: String },
        phoneNumber: { type: String },
        email: { type: String }
    }],
    comment: { type: String },
    createdEmployee: {},
    status: { type: String }
}, { strict: false })

module.exports = mongoose.model('leadCollection', leadCollection);
// {
//             hour: { type: String},
//             minute: { type: String},
//             period: { type: String}
//         },
