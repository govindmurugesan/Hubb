var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var vendorDetails = new Schema({
	name: {type: String},
	vendorCode: {type: String},
	state: {type: String},
	country:{type: String},
	city: {type: String},
	contactNo: {type: Number},
	countryCode: {type: String},
	venderType: {type: String},
	zip: {type: String},
	addressline1: {type: String},
	addressline2: {type: String},
	email: {type: String},
	website: {type: String},
	creationDate:Date,
	updatedon: Date,
	status: {type: String},

	
},{strict: false})

module.exports = mongoose.model('vendor',vendorDetails);
