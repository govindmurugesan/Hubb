var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bills = new Schema({
    vendor: { type: String, required: true },
    billno: { type: String, required: true,unique: true },
    billdate: { type: Date, required: true },
    duedate: { type: Date, required: true },
    amount: { type: String, required: true },
    currencytype: { type: String, required: true },
    comment: { type: String },
    status: { type: String, default: 'active' }
}, { strict: false });

module.exports = mongoose.model('bills', bills);
