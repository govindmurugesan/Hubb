var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var employee = new Schema({
    workLocation: { type: String },
    employeeId: { type: String },
    email: [{
        emergencyEmail: String,
        companyEmail: { type: String,default:''  }
    }],
    contact: [{
        emergencyNumber: String,
        primaryNumber: { type: String,default:''  }
    }],
    adharId: { type: String,default:''  },
    panNo: { type: String,default:''  },
    birthday: { type: Date },
    startDate: { type: Date },
    firstName: { type: String },
    middleName: { type: String,default:'' },
    lastName: { type: String },
    gender: { type: String },
    employeeType: { type: String },
    department: { type: String },
    title: { type: String },
    jobDescription: { type: String },
    reportingTo: [{
        reportingEmployeeId: String,
        reportingEmployeeName: String
    }],
    status: { type: String, default:'active' },
    creationDate: { type: Date, default:Date.now },
    updatedon: { type: Date, default:Date.now }

}, { strict: false })

module.exports = mongoose.model('employee', employee);
