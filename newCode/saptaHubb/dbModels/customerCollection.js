var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var customerDetails = new Schema({
	name: {type: String},
	customerCode: {type: String},
	state: {},
	country: [{}],
	city: {type: String},
	contactNo: {type: Number},
	countryCode: {type: String},
	customerType: {type: String},
	zip: {type: String},
	addressline1: {type: String},
	addressline2: {type: String},
	email: {type: String},
	website: {type: String},
	creationDate:Date,
	updatedon: Date,
	status: {type: String},

	
},{strict: false})

module.exports = mongoose.model('customer',customerDetails);
