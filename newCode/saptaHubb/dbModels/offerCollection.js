var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var offerCollection = new Schema({
    projectName: {},
    offerDate: { type: String, required: true },
    candidateName: { type: String, required: true },
    designation: { type: String, required: true },
    contact: { type: String, required: true },
    ctc: { type: String},
    joiningDate: { type: String },
    comment : { type: String}
}, { strict: false });

module.exports = mongoose.model('offer', offerCollection);
