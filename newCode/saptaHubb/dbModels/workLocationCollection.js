var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var workLocation = new Schema({
	country: {type: String},
	state: {type: String},
	worklocation: {type: String},
	creationDate:Date,
	updatedon: Date,
	status: {type: String}

	
},{strict: false})

module.exports = mongoose.model('worklocation',workLocation);
