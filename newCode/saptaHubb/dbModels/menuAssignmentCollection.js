var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var menuAssignment = new Schema({
	role_refid: {type: String, ref: 'roles'},
   roleName:  {type: String},
   assignedMenus:  [{}],
   numberOfUser: {type: String, ref: 'users'},
   description: {type: String},
   createdOn: {type: Date},
   updatedOn: {type: Date, default: Date.now}

},{strict: false});

module.exports = mongoose.model('menuAccess', menuAssignment);