var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var requirementCollection = new Schema({
    customer: {},
    projectName: {},
    minExp: { type: String, required: true },
    maxExp: { type: String, required: true },
    cost: { type: String },
    jobDescription: { type: String},
    comment: { type: String }
}, { strict: false });

module.exports = mongoose.model('requirement', requirementCollection);
