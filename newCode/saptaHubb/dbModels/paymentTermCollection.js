var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var paymentTerm = new Schema({
	name: {type: String},
	numberofdays: {type: String},
	creationDate:Date,
	updatedon: Date,
	status: {type: String}

	
},{strict: false})

module.exports = mongoose.model('payment_term',paymentTerm);
