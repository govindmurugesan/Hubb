var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var projectCollection = new Schema({
    customer: {},
    projectName: { type: String, required: true },
    displayName: { type: String, required: true },
    projectCode: { type: String, required: true },
    startDate: { type: Date, required: true },
    endDate: { type: Date, required: true },
    status: { type: String, default: 'active' },
    projectDuration: { type: String, required: true },
    comment: { type: String },
    bdm: {  },
    projectManager: {  },
    projectCost:{type:Number},
    payment: [{
        paymentTerm: { type: String },
        paymentTermValue: { type: Number },
        paymentValueType: { type: String }
    }],
    projectCostComment: [{
        comment: { type: String }
    }]
}, { strict: false });

module.exports = mongoose.model('project', projectCollection);
