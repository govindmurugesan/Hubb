var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var roles = new Schema({
    name:  {type: String},
    discription: {type: String},
    menusassigned: {},
    submenusassigned: {},
	creationDate: Date,
    updatedon: {type: Date, default: Date.now}

},{strict: false});

module.exports = mongoose.model('roles', roles);