var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var salesReportCollection = new Schema({
    bdm: { type: String, required: true },
    bdmID: { type: String },
    q1: { type: Number },
    q2: { type: Number },
    q3: { type: Number },
    q4: { type: Number },
    financialYearStart: { type: Number, required: true },
    financialYearEnd: { type: Number, required: true }
}, { strict: false });

module.exports = mongoose.model('sales_report_collection', salesReportCollection);
