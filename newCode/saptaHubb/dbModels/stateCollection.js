var mongoose      = require('mongoose');
var Schema        = mongoose.Schema;

var stateDetails = new Schema({
	name: {type: String},
	state: [{}],
	statename: {type: String},	
	creationDate:Date,
	updatedon: Date,
	status: {type: String}

	
},{strict: false})

module.exports = mongoose.model('state',stateDetails);
