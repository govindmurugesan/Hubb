var mongoose  = require('mongoose');
var Schema   = mongoose.Schema;

var companyInformation = new Schema({
	/*hubbid: {type: Number},*/
	companyname: {type: String},
	tanNo: {type: String},
	panNo: {type: String},
	address1: {type: String},
	address2: {type: String},
	city: {type: String},
	state: {type: String},
	country:{type: String},
	callingCode:{type: String},
	zip: {type: String},
	mainPhone: {type: Number},
	industry: {type: String},
	website: {type: String},
	creationDate:Date,
	updatedOn: Date,
	/*note:{type: String}*/
		
},{strict: false})

module.exports = mongoose.model('company_Information',companyInformation);