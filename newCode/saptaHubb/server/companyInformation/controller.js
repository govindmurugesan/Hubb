companyInformationCollection= require('../../dbModels/companyInformationCollection.js'),
countryCollection= require('../../dbModels/countryCollection.js'),
stateCollection= require('../../dbModels/stateCollection.js'),
workLocationCollection= require('../../dbModels/workLocationCollection.js'),


httpStatusCode = require('../../utils/httpStatusCodeConstant'),
constant = require('../../utils/constantList');

module.exports.getCompanyInformation = getCompanyInformation;
module.exports.updateCompanyInformation = updateCompanyInformation;


function getCompanyInformation(verifiedUser,req,res,next){
	if(verifiedUser.verified){
		companyInformationCollection.find()
		 .exec(function(err, collection) {
			if(err) { return res.status(httpStatusCode.DBERROR).send(err);}
			if(collection.length > 0) res.json(collection);
				else res.json({msg: 'Company detail not available contact admin'})
			});
	}else{
		res.sendStatus(httpStatusCode.Unauthorized);
	}

}

function updateCompanyInformation(verifiedUser,req,res,next){
	if(verifiedUser.verified){
		
		if(req.body._id){
			companyInformationCollection.findOne({ '_id': req.body._id})
			.exec(function(err, companyDetails) {
				if(err) return res.status(httpStatusCode.DBERROR).send(err);
				if(companyDetails != null ){
					companyDetails.companyname = req.body.companyname,
					companyDetails.tanNo = req.body.tanNo,
					companyDetails.panNo = req.body.panNo,
					companyDetails.address1 = req.body.address1,
					companyDetails.address2 = req.body.address2,
					companyDetails.country = req.body.country,
					companyDetails.state = req.body.state,
					companyDetails.city = req.body.city,
					companyDetails.zip = req.body.zip,
					companyDetails.mainPhone = req.body.mainPhone,
					companyDetails.callingCode = req.body.callingCode,
					companyDetails.industry = req.body.industry,
					companyDetails.website = req.body.website,
					companyDetails.updatedOn = Date.now()

					companyDetails.save(function(err){
						if (err) { return res.status(httpStatusCode.DBERROR).send(err) ;}
						else{
							res.json({successMsg: 'true'})
						}
					});

				}else{
					res.json({msg: 'Record not available to update'})
				}
			});
		}else{
			newcompanyInformation = new companyInformationCollection({
				companyname: req.body.companyname,
				tanNo: req.body.tanNo,
				panNo: req.body.panNo,
				address1: req.body.address1,
				address2: req.body.address2,
				country: req.body.country,
				state: req.body.state,
				city: req.body.city,
				zip: req.body.zip,
				mainPhone: req.body.mainPhone,
				callingCode: req.body.callingCode,
				industry: req.body.industry,
				website: req.body.website,
				creationDate: Date.now(),
				updatedOn: Date.now(),
				
			});

			newcompanyInformation.save(function(err,data){
				if(err) return res.status(httpStatusCode.DBERROR).send(err);
				res.json({successMsg:'true'});
			});
		}
	}else{
		res.sendStatus(httpStatusCode.Unauthorized);
	}
}