var express = require('express'),
    router = express.Router(),
    companyInformationController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
  .route('/CompanyInformation')
  .get(tokenVerifier.verifyToken, companyInformationController.getCompanyInformation)
  .put(tokenVerifier.verifyToken, companyInformationController.updateCompanyInformation);


/*
   router
  .route('/addNewCompanyInfo')

   .post(tokenVerifier.verifyToken, companyInformationController.addNewCompanyInfo);
   
    router
  .route('/getCountryList')

   .get(tokenVerifier.verifyToken, companyInformationController.getCountryList);
  

  router
 .route('/getStateList')

   .get(tokenVerifier.verifyToken, companyInformationController.getStateList);


   router
 .route('/getCityList')

   .get(tokenVerifier.verifyToken, companyInformationController.getCityList);


   router
 .route('/getCompanyInformation')

     .get(tokenVerifier.verifyToken, companyInformationController.getCompanyInformation);

  router
 .route('/updateCompanyInformation')

     .put(tokenVerifier.verifyToken, companyInformationController.updateCompanyInformation);*/

    module.exports = router;
