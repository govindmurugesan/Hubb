var express = require('express'),
    router = express.Router(),
    viewEmployeeController = require('./controller'),

     	tokenVerifier = require('../../utils/tokenVerifier');

   router
  .route('/addViewEmployee')
  .post(tokenVerifier.verifyToken, viewEmployeeController.viewEmployee);



     	 module.exports = router;
