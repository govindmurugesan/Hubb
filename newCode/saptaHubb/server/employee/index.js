var express = require('express'),
    router = express.Router(),
    employeeController = require('./controller'),

    tokenVerifier = require('../../utils/tokenVerifier');

router
    .route('/employee')
    .post(tokenVerifier.verifyToken, employeeController.createEmployee)
    .get(tokenVerifier.verifyToken, employeeController.getEmployeeDetails)
    .put(tokenVerifier.verifyToken, employeeController.updateEmployeeDetails);

router
    .route('/getemployee')
    .get(tokenVerifier.verifyToken, employeeController.getEmployeeDetailsByReference);

router
    .route('/getEmployeeByEmailOrName')
    .get(tokenVerifier.verifyToken, employeeController.getEmployeeByEmailOrName);


module.exports = router;
