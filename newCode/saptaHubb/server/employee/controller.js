employeeCollection = require('../../dbModels/employeeCollection.js'),


    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.createEmployee = createEmployee;
module.exports.getEmployeeDetails = getEmployeeDetails;
module.exports.getEmployeeDetailsByReference = getEmployeeDetailsByReference;
module.exports.updateEmployeeDetails = updateEmployeeDetails;
module.exports.getEmployeeByEmailOrName = getEmployeeByEmailOrName;

function updateEmployeeDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            employeeCollection.findOne({ '_id': req.body._id })
                .exec(function(err, employeeDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (employeeDetail != null) {

                        // updation goes here
                        employeeDetail.workLocation = req.body.workLocation;
                        employeeDetail.employeeId = req.body.employeeId;
                        employeeDetail.email = [{
                            emergencyEmail: req.body.email[0].emergencyEmail,
                            companyEmail: req.body.email[0].companyEmail
                        }];
                        employeeDetail.contact = [{
                            emergencyNumber: req.body.contact[0].emergencyNumber,
                            primaryNumber: req.body.contact[0].primaryNumber
                        }];
                        employeeDetail.adharId = req.body.adharId;
                        employeeDetail.panNo = req.body.panNo;
                        employeeDetail.birthday = req.body.birthday;
                        employeeDetail.startDate = req.body.startDate;
                        employeeDetail.firstName = req.body.firstName;
                        employeeDetail.middleName = req.body.middleName;
                        employeeDetail.lastName = req.body.lastName;
                        employeeDetail.gender = req.body.gender;
                        employeeDetail.employeeType = req.body.employeeType;
                        employeeDetail.department = req.body.department;
                        employeeDetail.title = req.body.title;
                        employeeDetail.jobDescription = req.body.jobDescription;
                        employeeDetail.reportingTo = [{
                            reportingEmployeeId: req.body.reportingTo.reportingEmployeeId,
                            reportingEmployeeName: req.body.reportingTo.reportingEmployeeName
                        }];
                        employeeDetail.status = req.body.status.status;
                        employeeDetail.creationDate = req.body.creationDate;
                        employeeDetail.updatedon = Date.now();

                        employeeDetail.save(function(err, data) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });

                    } else {
                        res.json({ msg: 'Bill not found' });
                    }
                });
        } else {
            res.json({ msg: 'Bill id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function createEmployee(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.empid != null || req.body.empid != undefine) {

            newCreateEmployee = new employeeCollection({
                workLocation: req.body.workLocation.worklocation,
                employeeId: req.body.empid,
                firstName: req.body.fname,
                middleName: req.body.mname,
                lastName: req.body.lname,
                email: [{
                    emergencyEmail: req.body.email.emergencyEmail,
                    companyEmail: req.body.email.companyEmail
                }],
                contact: [{
                    emergencyNumber: req.body.conatct.emergencyNumber,
                    primaryNumber: req.body.conatct.primaryNumber
                }],
                gender: req.body.gender,
                adharId: req.body.adharno,
                panNo: req.body.panno,
                birthday: req.body.dateOfBirth,
                employeeType: req.body.employeeType.name,
                department: req.body.department.name,
                title: req.body.emptitle,
                status: constant.activeStatus,
                jobDescription: req.body.jobdescription,
                startDate: req.body.joiningDate,
                reportingTo: [{
                    reportingEmployeeId: req.body.reportingTo.reportingEmployeeId,
                    reportingEmployeeName: req.body.reportingTo.reportingEmployeeName
                }],
                creationDate: Date.now(),
                updatedon: Date.now()
            });


            newCreateEmployee.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successMsg: 'true' });
            });
        } else {
            res.json({ errormsg: "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getEmployeeDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        employeeCollection.find().exec(function(err, employeeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (employeeList.length > 0) {
                res.json(employeeList)
            } else {
                res.json({ "msg": 'EmployeeList is not available' })
            }

        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getEmployeeDetailsByReference(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.email) {
            employeeCollection.findOne({ 'email.companyEmail': req.query.email }).exec(function(err, employeeDetail) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                if (employeeDetail) {
                    res.json(employeeDetail)
                } else {
                    res.json({ "msg": 'Employee detail is not available' })
                }

            });

        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getEmployeeByEmailOrName(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.emp) {

            req.query.emp = new RegExp(req.query.emp.toLowerCase(), "i");

            employeeCollection.find({
                $or: [
                    { 'email.companyEmail': req.query.emp },
                    { 'firstName': req.query.emp },
                    { 'middleName': req.query.emp },
                    { 'lastName': req.query.emp }
                ],
                $and: [{ 'status': 'active' }]
            }).exec(function(err, employeeDetail) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                if (employeeDetail.length > 0) {
                    res.json(employeeDetail)
                } else {
                    res.json({ "msg": 'Employee detail is not available' })
                }

            });
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
