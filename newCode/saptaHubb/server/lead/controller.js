var leadCollection = require('../../dbModels/leadCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.createLead = createLead;
module.exports.getLeadDetails = getLeadDetails;
module.exports.updateLeadDetails = updateLeadDetails;
module.exports.createFollowup = createFollowup;
module.exports.getLeadByReference = getLeadByReference;
module.exports.updateFollowUp = updateFollowUp;
module.exports.getLeadByDate = getLeadByDate;
module.exports.newFollowupContact = newFollowupContact;
module.exports.updateContact = updateContact;


function createLead(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body != null || req.body != undefined) {

            newCreateLead = new leadCollection({
                leadName: req.body.leadName,
                mobileNumber: req.body.mobileNumber,
                phoneNumber: req.body.phoneNumber,
                email: req.body.email,
                address: req.body.address,
                createdEmployee: req.body.createdEmployee,
                comment: req.body.comment,
                status: constant.newStatus
            });

            newCreateLead.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successMsg: 'true' });
            });
        } else {
            res.json({ errormsg: "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function updateLeadDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {

            leadCollection.findOne({ '_id': req.body._id })
                .exec(function(err, leadDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (leadDetail != null) {

                        // updation goes here
                        leadDetail.leadName = req.body.leadName;
                        leadDetail.mobileNumber = req.body.mobileNumber;
                        leadDetail.phoneNumber = req.body.phoneNumber;
                        leadDetail.email = req.body.email;
                        leadDetail.address = req.body.address;
                        leadDetail.dateOfContact = req.body.dateOfContact;
                        leadDetail.status = req.body.status;
                        leadDetail.comment = req.body.comment;
                        leadDetail.status = req.body.status;

                        leadDetail.save(function(err, data) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });

                    } else {
                        res.json({ msg: 'Lead not found' });
                    }
                });
        } else {
            res.json({ msg: 'Lead ID not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getLeadDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        leadCollection.find().exec(function(err, leadList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (leadList.length > 0) {
                res.json(leadList)
            } else {
                res.json({ "msg": 'Lead List is not available' })
            }

        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getLeadByDate(verifiedUser, req, res, next) {

    if (verifiedUser.verified) {
        if (req.query.date) {
            var checkDate = new RegExp(req.query.date);
            leadCollection.find({
                    '$or': [
                        { 'followup.followupDate': { "$lte": new Date(checkDate) } },
                        { 'followup.followupDate': { "$eq": new Date(checkDate) } },
                        { 'followup.followupDate': { "$gte": new Date(checkDate) } }
                    ]
                })
                .exec(function(err, followupInfo) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (followupInfo.length > 0) {
                        res.json(followupInfo);
                    } else {
                        res.json({ "msg": 'Followup is not available' })
                    }

                });
        } else {
            res.json({ msg: 'Followup Date not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function createFollowup(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.leadID) {
            leadCollection.findOne({ '_id': req.body.leadID })
                .exec(function(err, leadDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err);
                    }
                    if (leadDetail != null) {
                        if (leadDetail.status == "New") {
                            leadDetail.status = constant.inProgressStatus;
                        }
                        followupDetail = {
                            contactPerson: req.body.contactPerson,
                            designation: req.body.designation,
                            im: req.body.im,
                            mobileNumber: req.body.mobileNumber,
                            phoneNumber: req.body.phoneNumber,
                            email: req.body.email,
                            dateOfContact: req.body.dateOfContact,
                            dateOfContactTime: req.body.dateOfContactTime,
                            followupDate: req.body.followupDate,
                            followupTime: req.body.followupTime,
                            remarks: req.body.remarks,
                            scopeOfWork: req.body.scopeOfWork,
                            addedEmployee: req.body.addedEmployee,
                            addedEmployeeID: req.body.addedEmployeeID,
                            updatedOn: Date.now()
                        }
                        leadDetail.followup.push(followupDetail);
                        leadDetail.save(function(err, result) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });
                    } else {
                        res.sendStatus(httpStatusCode.BADREQUEST);
                    }
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getLeadByReference(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.leadId) {
            leadCollection.find({ '_id': req.query.leadId })
                .exec(function(err, leadInfo) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (leadInfo.length > 0) {
                        res.json(leadInfo);
                    } else {
                        res.json({ "msg": 'Lead is not available' })
                    }

                });
        } else {
            res.json({ msg: 'Lead ID not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateFollowUp(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {

        if (req.body._id) {

            leadCollection.findOne({ 'followup._id': req.body._id })
                .exec(function(err, followupDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (followupDetail != null) {
                        if (followupDetail.followup.length > 0) {
                            for (var i = 0; i < followupDetail.followup.length; i++) {
                                if (followupDetail.followup[i]._id == req.body._id) {
                                    followupDetail.followup[i] = {
                                        contactPerson: req.body.contactPerson,
                                        designation: req.body.designation,
                                        im: req.body.im,
                                        mobileNumber: req.body.mobileNumber,
                                        phoneNumber: req.body.phoneNumber,
                                        email: req.body.email,
                                        dateOfContact: req.body.dateOfContact,
                                        dateOfContactTime: req.body.dateOfContactTime,
                                        followupDate: req.body.followupDate,
                                        followupTime: req.body.followupTime,
                                        remarks: req.body.remarks,
                                        scopeOfWork: req.body.scopeOfWork,
                                        addedEmployee: req.body.addedEmployee,
                                        addedEmployeeID: req.body.addedEmployeeID,
                                        updatedOn: Date.now()
                                    };
                                }
                            }

                            followupDetail.save(function(err, data) {
                                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                                res.json({ successmsg: 'success' });
                            });

                        } else {
                            res.json({ msg: 'Followup Detail not found' });
                        }
                    };
                });

        } else {
            res.json({ msg: 'Followup ID not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function newFollowupContact(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.leadID) {
            leadCollection.findOne({ '_id': req.body.leadID })
                .exec(function(err, leadDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err);
                    }
                    if (leadDetail != null) {

                        followupContactDetail = {
                            contactPerson: req.body.contactPerson,
                            designation: req.body.designation,
                            im: req.body.im,
                            mobileNumber: req.body.mobileNumber,
                            phoneNumber: req.body.phoneNumber,
                            email: req.body.email
                        };

                        leadDetail.followupContact.push(followupContactDetail);
                        leadDetail.save(function(err, result) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });
                    } else {
                        res.sendStatus(httpStatusCode.BADREQUEST);
                    }
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateContact(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        console.log('req.body._id', req.body._id);
        if (req.body._id) {
            leadCollection.findOne({ 'followupContact._id': req.body._id })
                .exec(function(err, followupContactDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (followupContactDetail != null) {
                        if (followupContactDetail.followupContact.length > 0) {
                            for (var i = 0; i < followupContactDetail.followupContact.length; i++) {
                                if (followupContactDetail.followupContact[i]._id == req.body._id) {
                                    followupContactDetail.followupContact[i] = {
                                        contactPerson: req.body.contactPerson,
                                        designation: req.body.designation,
                                        mobileNumber: req.body.mobileNumber,
                                        phoneNumber: req.body.phoneNumber,
                                        im: req.body.im,
                                        email: req.body.email
                                    };
                                }
                            }

                            followupContactDetail.save(function(err, data) {
                                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                                res.json({ successmsg: 'success' });
                            });

                        } else {
                            res.json({ msg: 'Followup Contact Person not found' });
                        }
                    }
                });
        } else {
            res.json({ msg: 'Followup Contact ID not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}