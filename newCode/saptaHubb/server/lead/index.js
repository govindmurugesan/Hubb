var express = require('express'),
    router = express.Router(),
    leadController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/lead')
    .post(tokenVerifier.verifyToken, leadController.createLead)
    .get(tokenVerifier.verifyToken, leadController.getLeadDetails)
    .put(tokenVerifier.verifyToken, leadController.updateLeadDetails);

router
    .route('/followup')
    .post(tokenVerifier.verifyToken, leadController.createFollowup)
    .get(tokenVerifier.verifyToken, leadController.getLeadByReference)
    .put(tokenVerifier.verifyToken, leadController.updateFollowUp);

router
    .route('/todo')
    .get(tokenVerifier.verifyToken, leadController.getLeadByDate);

router
    .route('/followupContact')
    .post(tokenVerifier.verifyToken, leadController.newFollowupContact)
    .put(tokenVerifier.verifyToken, leadController.updateContact);

module.exports = router;
