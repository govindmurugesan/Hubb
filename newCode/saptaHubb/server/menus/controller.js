var userModel = require('../../dbModels/usersCollection.js'),
	//rolesCollection = require('../../dbModels/rolesCollection.js'),
	menusCollection = require('../../dbModels/menusCollection.js'),
	employeeRoleCollection = require('../../dbModels/employeeRoleCollection.js'),
	menuPrivilegesCollection = require('../../dbModels/menuAssignmentCollection.js'),
	//passwordGenerator = require('../../utils/passwordGenerator.js'),
	//resetRedis = require('../../utils/resetRedis'),
	//mailSender = require('../../utils/mailSender.js'),
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	constant = require('../../utils/constantList');
	
	module.exports.getMenus = getMenus;
	module.exports.getEmpRole = getEmpRole;
	module.exports.updateRolePrivileges = updateRolePrivileges;
	module.exports.getRolePrivileges =getRolePrivileges;
	//module.exports.saveRole = saveRole;
	//module.exports.getUserInformation = getUserInformation;
    /*module.exports.getInviteInformation = getInviteInformation;*/
   /* module.exports.updateUserType = updateUserType;*/


function getMenus(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			menusCollection.find()
			.exec(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}

function getEmpRole(verifiedUser,req,res,next){

		if (verifiedUser.verified){
			employeeRoleCollection.find()
			.exec(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}

function updateRolePrivileges(verifiedUser,req,res,next){
	if (verifiedUser.verified){

		if(req.body.referenceId){
			menuPrivilegesCollection.findOne({'_id':req.body.referenceId})
			.exec(function(err, Details){
				if(err) return res.status(httpStatusCode.DBERROR).send(err);
				else 
					Details.assignedMenus = req.body.assignedMenus,
					Details.description  = req.body.description,

					Details.save(function(err){
						if (err) { return res.status(httpStatusCode.DBERROR).send(err) ;}
						else{
							res.json({successMsg: 'true'})
						}
					});


			})
			



		}else{
			var newAssignment = new menuPrivilegesCollection({
				role_refid: req.body.role_refid,
			  	roleName:  req.body.roleName,
			    assignedMenus:  req.body.assignedMenus,
			    //numberOfUser: {type: String, ref: 'users'},
			    description: req.body.description,
			    createdOn: Date.now(),
			    updatedOn: Date.now()
			});

			newAssignment.save(function(err, data){
				if(err) return res.status(httpStatusCode.DBERROR).send(err);
				else res.json({successMsg: 'true'})
			})
		}
		
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}

function getRolePrivileges(verifiedUser,req,res,next){
	if (verifiedUser.verified){
		menuPrivilegesCollection.find({'role_refid':req.query.roleId}, function(err, roleDetails){
			if(err) return res.status(httpStatusCode.DBERROR).send(err);
			if(roleDetails.length >0){
				res.json(roleDetails)
			}else{
				res.json({msg: 'Role donot have any menu access'})
			}

		});
	}else{
			res.sendStatus(httpStatusCode.Unauthorized);
	}
}

/*function getUserInformation(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			userModel.find({ 'status': constant.activeStatus},function(err,collection){
			
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'User List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}*/

/*function getInviteInformation(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			userModel.find({ 'status': constant.invited},function(err,collection){
			
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}*/

/*function saveRole(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			console.log("req",req);
			nathan = new saveRoleCollection({
				selectRole:req.body.selectRole,
				roleText:req.body.roleText,
				roleCheck:req.body.roleCheck
			});
			console.log("nathan",req.body);
			nathan.save(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}*/



/*function updateUserType(verifiedUser,req,res,next){

	if(verifiedUser.verified){
		if(req.body.id != null || req.body.role != undefine){
			userModel.findOne({ 'role': req.body.role[0].primary})
			.exec(function(err, data){
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(data){

					data.role[0].primary = data.role[0].primary

					data.save(function(err){
						if (err) { return res.status(httpStatusCode.DBERROR).send(err) ;}
						else{
							res.json({successMsg: 'true'})
							
								data.role[0].primary = data.role[0].primary
						}
					});
				}else{
					res.json({msg: 'Record not available to update'})
				}
			})
		}else{
			res.sendStatus(httpStatusCode.BADREQUEST);
		}
	}else{
		res.sendStatus(httpStatusCode.Unauthorized);
	}
}
*/
