var express = require('express'),
    router = express.Router(),
  	configarationController = require('./controller');
  	tokenVerifier = require('../../utils/tokenVerifier');

router
  .route('/getMenus')
  .get(tokenVerifier.verifyToken, configarationController.getMenus);

router
  .route('/getEmpRole')
  .get(tokenVerifier.verifyToken, configarationController.getEmpRole); 

  

router
  .route('/roleprivileges')
  .put(tokenVerifier.verifyToken, configarationController.updateRolePrivileges)
  .get(tokenVerifier.verifyToken, configarationController.getRolePrivileges); 

 /* router
  .route('/getUserInformation')
  .get(tokenVerifier.verifyToken, configarationController.getUserInformation); */

  

  /*router
  .route('/saveRole')
  .post(tokenVerifier.verifyToken, configarationController.saveRole); 
*/
 /* router
  .route('/updateUserType')
  .put(tokenVerifier.verifyToken, configarationController.updateUserType); */

   module.exports = router;