var express = require('express'),
    router = express.Router(),
  	vendorController = require('./controller'),
   
  	tokenVerifier = require('../../utils/tokenVerifier');


  router
  .route('/vendordetails')
  .post(tokenVerifier.verifyToken, vendorController.addVendorDetails)
  .get(tokenVerifier.verifyToken, vendorController.getVendorDetails)
  .put(tokenVerifier.verifyToken, vendorController.updateVendorDetail);
  /*.delete(tokenVerifier.verifyToken, customerController.deleteEmployeeType);*/

  module.exports = router;