var vendorCollection = require('../../dbModels/vendorCollection.js'),
	passwordGenerator = require('../../utils/passwordGenerator.js'),
	resetRedis = require('../../utils/resetRedis'),
	mailSender = require('../../utils/mailSender.js');
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	constant = require('../../utils/constantList');
	
	module.exports.addVendorDetails = addVendorDetails;
	module.exports.getVendorDetails = getVendorDetails;
	module.exports.updateVendorDetail = updateVendorDetail;

	function addVendorDetails(verifiedUser, req, res, next) {
		if (verifiedUser.verified){
			if(req.body.country.name && req.body.name){
				vendorCollection.count({}, function(err , count){
					var collectionCount;
					if(count == 0){
					  	collectionCount = 1;
					}else{
						collectionCount = count+1;
					}
				    var vendorCode = req.body.country.name.substring(0, 2)+req.body.name.substring(0,3)+collectionCount;
					vendor = new vendorCollection({
						name: req.body.name,
						vendorCode: vendorCode.toUpperCase(),
						state: req.body.state.name,
						city: req.body.city,
						contactNo: req.body.contactno,
						countryCode:  req.body.countryCode,
						country:  req.body.country.name,
						venderType: req.body.vendorType,
						zip: req.body.zip,
						addressline1: req.body.address1,
						addressline2: req.body.address2,
						email: req.body.email,
						website: req.body.website,
						creationDate:Date.now(),
						updatedon: Date.now(),
						status: constant.activeStatus
					});

					vendor.save(function(err,data){
						if(err)return res.status(httpStatusCode.DBERROR).send(err);
						res.json({successmsg:'true'});
					});

				});

			}else{
				res.json({"msg":"Bad Input"})
			}
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}

	function getVendorDetails(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			vendorCollection.find(function(err,docs){
				if (err) return res.status(httpStatusCode.DBERROR).send(err);
				if(docs.length > 0) res.json(docs);
				else res.json({msg:'Vendor list not available'});
			});
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}

	function updateVendorDetail(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			console.log("req",req.body.name);
			if(req.body._id){
				vendorCollection.findOne({ '_id': req.body._id})
				.exec(function(err, vendorDetail) {
					if(err) return res.status(httpStatusCode.DBERROR).send(err);
					console.log("customerDetail",vendorDetail);
					if(vendorDetail != null ){
						console.log("det",vendorDetail);
						vendorDetail.name = req.body.name,
						vendorDetail.vendorCode = req.body.vendorCode.toUpperCase(),
						vendorDetail.state = req.body.state,
						vendorDetail.city = req.body.city,
						vendorDetail.contactNo = req.body.contactNo,
						vendorDetail.countryCode =  req.body.countryCode,
						vendorDetail.country =  req.body.country,
						vendorDetail.venderType = req.body.venderType,
						vendorDetail.zip = req.body.zip,
						vendorDetail.addressline1 = req.body.addressline1,
						vendorDetail.addressline2 = req.body.addressline2,
						vendorDetail.email = req.body.email,
						vendorDetail.website = req.body.website,
						vendorDetail.updatedon = Date.now(),
						vendorDetail.status = req.body.status;

			
						
						console.log("gng to updae",vendorDetail);
						vendorDetail.save(function(err,data){
							if (err) { return res.status(httpStatusCode.DBERROR).send(err) ;}
							else{
								console.log("33333333333",data);
								res.json({successMsg: 'true'})
							}
						});
					}else{
						 res.json({msg:'Customer not found'});
					}
				});
			}else{
				 res.json({msg:'Customer id not available'});
			}
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}