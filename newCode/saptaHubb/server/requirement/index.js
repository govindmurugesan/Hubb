var express = require('express'),
    router = express.Router(),
    requirementController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/requirement')
    .post(tokenVerifier.verifyToken, requirementController.addRequirementDetail)
    .get(tokenVerifier.verifyToken, requirementController.getRequirementDetail)
    .put(tokenVerifier.verifyToken, requirementController.updateRequirementDetail);



module.exports = router;
