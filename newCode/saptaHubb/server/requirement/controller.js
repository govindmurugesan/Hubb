var requirementCollection = require('../../dbModels/requirementCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.addRequirementDetail = addRequirementDetail;
module.exports.getRequirementDetail = getRequirementDetail;
module.exports.updateRequirementDetail = updateRequirementDetail;

function addRequirementDetail(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {


        if (req.body != null || req.body != undefined) {

            newRequirement = new requirementCollection({

                customer: req.body.customer,
                projectName: req.body.projectName,
                minExp: req.body.minExp,
                maxExp: req.body.maxExp,
                cost: req.body.cost,
                jobDescription: req.body.jobDescription,
                comment: req.body.comment

            });


            newRequirement.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'Success' });
            });

        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getRequirementDetail(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        requirementCollection.find(function(err, docs) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (docs.length > 0) res.json(docs);
            else res.json({ msg: 'Project list not available' });
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateRequirementDetail(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            requirementCollection.findOne({ '_id': req.body._id })
                .exec(function(err, requirementDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (requirementDetail != null) {

                        requirementDetail.customer = req.body.customer;
                        requirementDetail.projectName = req.body.projectName;
                        requirementDetail.minExp = req.body.minExp;
                        requirementDetail.maxExp = req.body.maxExp;
                        requirementDetail.cost = req.body.cost;
                        requirementDetail.jobDescription = req.body.jobDescription;
                        requirementDetail.comment = req.body.comment;

                        requirementDetail.save(function(err, data) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err); } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Requirement Detail not found' });
                    }
                });
        } else {
            res.json({ msg: 'Requirement Detail id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
