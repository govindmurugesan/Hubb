var projectCollection = require('../../dbModels/projectCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.addProject = addProject;
module.exports.getProject = getProject;
module.exports.updateProjectDetail = updateProjectDetail;
module.exports.addProjectCost = addProjectCost;
module.exports.updateProjectCost = updateProjectCost;
module.exports.getProjectByReference = getProjectByReference;
module.exports.deletePaymentTermByIndex = deletePaymentTermByIndex;

function addProject(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {

        if (req.body != null || req.body != undefined) {

            newProject = new projectCollection({
                customer: req.body.customer,
                projectName: req.body.projectName,
                displayName: req.body.displayName,
                projectCode: req.body.projectCode,
                startDate: req.body.startDate,
                endDate: req.body.endDate,
                projectDuration: req.body.duration,
                comment: req.body.comment
            });

            newProject.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });

        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getProject(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        projectCollection.find(function(err, docs) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (docs.length > 0) res.json(docs);
            else res.json({ msg: 'Project list not available' });
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateProjectDetail(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            projectCollection.findOne({ '_id': req.body._id })
                .exec(function(err, projectDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (projectDetail != null) {

                        projectDetail.customer = req.body.customer;
                        projectDetail.projectName = req.body.projectName;
                        projectDetail.displayName = req.body.displayName;
                        projectDetail.projectCode = req.body.projectCode;
                        projectDetail.startDate = req.body.startDate;
                        projectDetail.endDate = req.body.endDate;
                        projectDetail.status = req.body.status;
                        projectDetail.projectDuration = req.body.projectDuration;
                        projectDetail.comment = req.body.comment;

                        projectDetail.save(function(err, data) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err);
                            } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Project not found' });
                    }
                });
        } else {
            res.json({ msg: 'Project id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addProjectCost(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.project._id) {
            projectCollection.findOne({ '_id': req.body.project._id })
                .exec(function(err, projectDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err);
                    }
                    if (projectDetail != null) {

                        projectDetail.bdm = req.body.bdm;
                        projectDetail.projectManager = req.body.projectManager;
                        projectDetail.projectCost = req.body.projectCost;
                        var newComment = {
                            comment: req.body.comment
                        }
                        projectDetail.projectCostComment.push(newComment);

                        var projectPaymentDetail = [];
                        for (var i = 0; i < req.body.payment.length; i++) {
                            projectPaymentDetail[i] = {
                                paymentTerm: req.body.payment[i].paymentTerm,
                                paymentTermValue: req.body.payment[i].paymentTermValue,
                                paymentValueType: req.body.payment[i].paymentValueType
                            }
                            projectDetail.payment.push(projectPaymentDetail[i]);
                        }
                        projectDetail.save(function(err, data) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);

                            res.json({ successmsg: 'success' });


                        });

                    }
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateProjectCost(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            projectCollection.findOne({ '_id': req.body._id })
                .exec(function(err, projectDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (projectDetail != null) {

                        projectDetail.bdm = req.body.bdm;
                        projectDetail.projectManager = req.body.projectManager;
                        projectDetail.projectCost = req.body.projectCost;
                        projectDetail.projectCostComment[projectDetail.projectCostComment.length - 1] = req.body.projectCostComment;
                        projectDetail.payment.splice(0, projectDetail.payment.length - 1);

                        var projectPaymentDetail = [];
                        for (var i = 0; i < req.body.payment.length; i++) {
                            projectPaymentDetail[i] = {
                                paymentTerm: req.body.payment[i].paymentTerm,
                                paymentTermValue: req.body.payment[i].paymentTermValue,
                                paymentValueType: req.body.payment[i].paymentValueType
                            }
                            projectDetail.payment.push(projectPaymentDetail[i]);
                        }

                        projectDetail.save(function(err, data) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err);
                            } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Project not found' });
                    }
                });
        } else {
            res.json({ msg: 'Project id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deletePaymentTermByIndex(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.paymentID && req.query.projectID) {
            projectCollection.findOne({ '_id': req.query.projectID })
                .exec(function(err, projectDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err);
                    }
                    if (projectDetail) {
                        if (projectDetail.payment.length > 0) {
                            for (var i = 0; i < projectDetail.payment.length; i++) {
                                // if (projectDetail.payment[i]._id == req.query.paymentID) {
                                //     projectDetail.payment.splice(i, 1);
                                // }
                            }
                            projectDetail.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err);
                                } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });

                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    } else {
                        res.json({ msg: 'Record not available to update' })
                    }

                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getProjectByReference(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.project) {
            projectCollection.findOne({ '_id': req.query.project })
                .exec(function(err, targetDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (targetDetail) res.json(targetDetail);
                    else res.json({ msg: 'Project not available' });
                });
        } else {
            res.json({ msg: 'Project ID not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
