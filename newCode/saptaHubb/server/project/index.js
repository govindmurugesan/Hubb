var express = require('express'),
    router = express.Router(),
    projectController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/project')
    .post(tokenVerifier.verifyToken, projectController.addProject)
    .get(tokenVerifier.verifyToken, projectController.getProject)
    .put(tokenVerifier.verifyToken, projectController.updateProjectDetail);

router
    .route('/projectCost')
    .post(tokenVerifier.verifyToken, projectController.addProjectCost)
    .put(tokenVerifier.verifyToken, projectController.updateProjectCost);

router
    .route('/getProjectByReference')
    .get(tokenVerifier.verifyToken, projectController.getProjectByReference);
router
    .route('/deletePaymentTermByIndex')
    .put(tokenVerifier.verifyToken, projectController.deletePaymentTermByIndex);



module.exports = router;