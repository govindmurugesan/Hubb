var offerCollection = require('../../dbModels/offerCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.addOfferDetail = addOfferDetail;
module.exports.getOffer = getOffer;
module.exports.updateOffer = updateOffer;

function addOfferDetail(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {

        if (req.body != null || req.body != undefined) {

            newOffer = new offerCollection({

                projectName: req.body.projectName,
                offerDate: req.body.offerDate,
                candidateName: req.body.candidateName,
                designation: req.body.designation,
                contact: req.body.contact,
                ctc: req.body.ctc,
                joiningDate: req.body.joiningDate,
                comment: req.body.comment

            });


            newOffer.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'Success' });
            });

        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getOffer(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        offerCollection.find(function(err, docs) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (docs.length > 0) res.json(docs);
            else res.json({ msg: 'Offer list not available' });
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateOffer(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            offerCollection.findOne({ '_id': req.body._id })
                .exec(function(err, offerDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (offerDetail != null) {
                        offerDetail.projectName = req.body.projectName;
                        offerDetail.offerDate = req.body.offerDate;
                        offerDetail.candidateName = req.body.candidateName;
                        offerDetail.designation = req.body.designation;
                        offerDetail.contact = req.body.contact;
                        offerDetail.ctc = req.body.ctc;
                        offerDetail.joiningDate = req.body.joiningDate;
                        offerDetail.comment = req.body.comment;

                        offerDetail.save(function(err, data) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err);
                            } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Offer Detail not found' });
                    }
                });
        } else {
            res.json({ msg: 'Offer Detail id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
