var express = require('express'),
    router = express.Router(),
    offerController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/offer')
    .post(tokenVerifier.verifyToken, offerController.addOfferDetail)
    .get(tokenVerifier.verifyToken, offerController.getOffer)
    .put(tokenVerifier.verifyToken, offerController.updateOffer);



module.exports = router;
