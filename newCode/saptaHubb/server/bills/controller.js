var billCollection = require('../../dbModels/billsCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.addbill = addBill;
module.exports.getbill = getBill;
module.exports.updateBill = updateBill;
module.exports.newBillNumber = newBillNumber;

function newBillNumber(verifiedUser, req, res, next) {

    if (verifiedUser.verified) {

        var newbillno = 'SL-BILL-' + getFinancialYear();
        var financialYear = new RegExp(newbillno);

        var query = billCollection.find({ billno: financialYear });
        query.exec(function(err, docs) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (docs.length > 0) {
                newbillno += '-' + (docs.length + 1);
            } else {
                newbillno += '-1';
            }
            res.json(newbillno);
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getFinancialYear() {
    var currentYear = new Date();
    var currentMonth = currentYear.getMonth();
    var last = String(currentYear.getYear()).slice(-2);

    if (currentMonth >= 4)
        return (last + (parseInt(last) + 1));
    else
        return ((parseInt(last) - 1) + last);
}


function getBill(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        billCollection.find(function(err, docs) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (docs.length > 0) res.json(docs);
            else res.json({ msg: 'Bill list not available' });
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addBill(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {

        if (req.body != null || req.body != undefined) {

            newBill = new billCollection({
                vendor: req.body.vendor,
                billno: req.body.billno,
                billdate: req.body.billingDate,
                duedate: req.body.dueDate,
                amount: req.body.dueAmount,
                currencytype: req.body.currencytype,
                comment: req.body.comment
            });

            newBill.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });

        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateBill(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            billCollection.findOne({ '_id': req.body._id })
                .exec(function(err, billDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (billDetail != null) {

                        // updation goes here
                        billDetail.vendor = req.body.vendor;
                        billDetail.billno = req.body.billno;
                        billDetail.billdate = req.body.billdate;
                        billDetail.duedate = req.body.duedate;
                        billDetail.amount = req.body.amount;
                        billDetail.currencytype = req.body.currencytype;
                        billDetail.comment = req.body.comment;
                        billDetail.status=req.body.status;

                        billDetail.save(function(err, data) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });

                    } else {
                        res.json({ msg: 'Bill not found' });
                    }
                });
        } else {
            res.json({ msg: 'Bill id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}