var express = require('express'),
    router = express.Router(),
    billsController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/bills')
    .post(tokenVerifier.verifyToken, billsController.addbill)
    .get(tokenVerifier.verifyToken, billsController.getbill)
    .put(tokenVerifier.verifyToken, billsController.updateBill);

router
    .route('/newbillnumber')
    .get(tokenVerifier.verifyToken, billsController.newBillNumber);

module.exports = router;
