var express = require('express'),
    router = express.Router(),
  	customerController = require('./controller'),
   
  	tokenVerifier = require('../../utils/tokenVerifier');


  router
  .route('/customerdetails')
  .post(tokenVerifier.verifyToken, customerController.addCustomerDetails)
  .get(tokenVerifier.verifyToken, customerController.getCustomerDetails)
  .put(tokenVerifier.verifyToken, customerController.updateCustomerDetail);
  /*.delete(tokenVerifier.verifyToken, customerController.deleteEmployeeType);*/

  module.exports = router;