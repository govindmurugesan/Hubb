var customerCollection = require('../../dbModels/customerCollection.js'),
	passwordGenerator = require('../../utils/passwordGenerator.js'),
	resetRedis = require('../../utils/resetRedis'),
	mailSender = require('../../utils/mailSender.js');
	httpStatusCode = require('../../utils/httpStatusCodeConstant'),
	constant = require('../../utils/constantList');
	
	module.exports.addCustomerDetails = addCustomerDetails;
	module.exports.getCustomerDetails = getCustomerDetails;
	module.exports.updateCustomerDetail = updateCustomerDetail;

	function addCustomerDetails(verifiedUser, req, res, next) {
		if (verifiedUser.verified){
			if(req.body.country.name && req.body.name){
				customerCollection.count({}, function(err , count){
					var collectionCount;
					if(count == 0){
					  	collectionCount = 1;
					}else{
						collectionCount = count+1;
					}
				    var customerCode = req.body.country.name.substring(0, 2)+req.body.name.substring(0,3)+collectionCount;
					customer = new customerCollection({
						name: req.body.name,
						customerCode: customerCode.toUpperCase(),
						state: req.body.state,
						city: req.body.city,
						contactNo: req.body.contactno,
						countryCode:  req.body.countryCode,
						country:  req.body.country,
						customerType: req.body.customerType,
						zip: req.body.zip,
						addressline1: req.body.address1,
						addressline2: req.body.address2,
						email: req.body.email,
						website: req.body.website,
						creationDate:Date.now(),
						updatedon: Date.now(),
						status: constant.activeStatus
					});

					customer.save(function(err,data){
						if(err)return res.status(httpStatusCode.DBERROR).send(err);
						res.json({successmsg:'true'});
					});

				});

			}else{
				res.json({"msg":"Bad Input"})
			}
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}

	function getCustomerDetails(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			customerCollection.find(function(err,docs){
				if (err) return res.status(httpStatusCode.DBERROR).send(err);
				if(docs.length > 0) res.json(docs);
				else res.json({msg:'Customer list not available'});
			});
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}

	function updateCustomerDetail(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			if(req.body._id){
				customerCollection.findOne({ '_id': req.body._id})
				.exec(function(err, customerDetail) {
					if(err) return res.status(httpStatusCode.DBERROR).send(err);
					if(customerDetail != null ){
						customerDetail.name = req.body.name
						customerDetail.customerCode = req.body.customerCode.toUpperCase(),
						customerDetail.state = req.body.state,
						customerDetail.city = req.body.city,
						customerDetail.contactNo = req.body.contactNo,
						customerDetail.countryCode =  req.body.countryCode,
						customerDetail.country =  req.body.countryDetail,
						customerDetail.customerType = req.body.customerType,
						customerDetail.zip = req.body.zip,
						customerDetail.addressline1 = req.body.addressline1,
						customerDetail.addressline2 = req.body.addressline2,
						customerDetail.email = req.body.email,
						customerDetail.website = req.body.website,
						customerDetail.updatedon = Date.now(),
						customerDetail.status = req.body.status;

			
						
						customerDetail.save(function(err,data){
							if (err) { return res.status(httpStatusCode.DBERROR).send(err) ;}
							else{
								res.json({successMsg: 'true'})
							}
						});
					}else{
						 res.json({msg:'Customer not found'});
					}
				});
			}else{
				 res.json({msg:'Customer id not available'});
			}
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
	}