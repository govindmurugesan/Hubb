var express = require('express'),
    router = express.Router(),
  	configarationController = require('./controller'),
   
  	tokenVerifier = require('../../utils/tokenVerifier');


router
  .route('/roles')
  .post(tokenVerifier.verifyToken, configarationController.addRole)
  .get(tokenVerifier.verifyToken, configarationController.getRoles);


  router
  .route('/employeetype')
  .post(tokenVerifier.verifyToken, configarationController.addEmployeeType)
  .get(tokenVerifier.verifyToken, configarationController.getEmployeeType)
  .put(tokenVerifier.verifyToken, configarationController.updateEmployeeType)
  .delete(tokenVerifier.verifyToken, configarationController.deleteEmployeeType);

  router
  .route('/customertype')
  .post(tokenVerifier.verifyToken, configarationController.addCustomerType)
  .get(tokenVerifier.verifyToken, configarationController.getCustomerType)
  .put(tokenVerifier.verifyToken, configarationController.updateCustomerType)
  .delete(tokenVerifier.verifyToken, configarationController.deleteCustomerType);

    router
  .route('/ventortype')
  .post(tokenVerifier.verifyToken, configarationController.addVendorType)
  .get(tokenVerifier.verifyToken, configarationController.getVendorType)
  .put(tokenVerifier.verifyToken, configarationController.updateVendorType)
  .delete(tokenVerifier.verifyToken, configarationController.deleteVendorType);

  router
  .route('/timeofftype')
  .post(tokenVerifier.verifyToken, configarationController.addTimeOffType)
  .get(tokenVerifier.verifyToken, configarationController.getTimeOffType)
  .put(tokenVerifier.verifyToken, configarationController.updateTimeOffType)
  .delete(tokenVerifier.verifyToken, configarationController.deleteTimeOffType);
  
router
  .route('/invoicetype')
  .post(tokenVerifier.verifyToken, configarationController.addInvoiceType)
  .get(tokenVerifier.verifyToken, configarationController.getInvoiceType)
  .put(tokenVerifier.verifyToken, configarationController.updateInvoiceType)
  .delete(tokenVerifier.verifyToken, configarationController.deleteInvoiceType);

router
  .route('/paymentterm')
  .post(tokenVerifier.verifyToken, configarationController.addPaymentTerm)
  .get(tokenVerifier.verifyToken, configarationController.getPaymentTerm)
  .put(tokenVerifier.verifyToken, configarationController.updatePaymentTerm)
  .delete(tokenVerifier.verifyToken, configarationController.deletePaymentTerm);

  router
  .route('/invoicepaymentterm')
  .post(tokenVerifier.verifyToken, configarationController.addInvoicePaymentTerm)
  .get(tokenVerifier.verifyToken, configarationController.getInvoicePaymentTerm)
  .put(tokenVerifier.verifyToken, configarationController.updateInvoicePaymentTerm)
  .delete(tokenVerifier.verifyToken, configarationController.deleteInvoicePaymentTerm);


  router
  .route('/employeerole')
  .post(tokenVerifier.verifyToken, configarationController.addEmployeeRole)
  .get(tokenVerifier.verifyToken, configarationController.getEmployeeRole)
  .put(tokenVerifier.verifyToken, configarationController.updateEmployeeRole)
  .delete(tokenVerifier.verifyToken, configarationController.deleteEmployeeRoleType);

  router
  .route('/Department')
  .post(tokenVerifier.verifyToken, configarationController.addDepartment)
  .get(tokenVerifier.verifyToken, configarationController.getDepartment)
  .put(tokenVerifier.verifyToken, configarationController.updateDepartment)
  .delete(tokenVerifier.verifyToken, configarationController.deleteDepartment);

 


  router
  .route('/country')
  .post(tokenVerifier.verifyToken, configarationController.addCountry)
  .get(tokenVerifier.verifyToken, configarationController.getCountryDetails)
  .put(tokenVerifier.verifyToken, configarationController.updateCountryDetails)
  .delete(tokenVerifier.verifyToken, configarationController.deleteCountry);

  router
  .route('/state')
  .post(tokenVerifier.verifyToken, configarationController.addState)
  .get(tokenVerifier.verifyToken, configarationController.getStateDetals)
  .put(tokenVerifier.verifyToken, configarationController.updateStateStatus)
  .delete(tokenVerifier.verifyToken, configarationController.deleteState);

  router
  .route('/worklocation')
  .post(tokenVerifier.verifyToken, configarationController.addWorkLocation)
  .get(tokenVerifier.verifyToken, configarationController.getWorkLocationDetails)
  .put(tokenVerifier.verifyToken, configarationController.updateWorkLocationStatus)
  .delete(tokenVerifier.verifyToken, configarationController.deleteWorkLocation);

   module.exports = router;