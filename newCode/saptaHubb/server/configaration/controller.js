var userModel = require('../../dbModels/usersCollection.js'),
    rolesCollection = require('../../dbModels/rolesCollection.js'),
    employeeTypeCollection = require('../../dbModels/employeeTypeCollection.js'),
    vendorTypeCollection = require('../../dbModels/vendorTypeCollection.js'),
    customerTypeCollection = require('../../dbModels/customerTypeCollection.js'),
    timeOffTypeCollection = require('../../dbModels/timeOffTypeCollection.js'),
    invoiceTypeCollection = require('../../dbModels/invoiceTypeCollection.js'),
    paymentTermCollection = require('../../dbModels/paymentTermCollection.js'),
    invoicePaymentTermCollection = require('../../dbModels/invoicePaymentTermCollection.js'),
    employeeRoleCollection = require('../../dbModels/employeeRoleCollection.js'),
    departmentCollection = require('../../dbModels/departmentCollection.js'),
    countryCollection = require('../../dbModels/countryCollection.js'),
    workLocationCollection = require('../../dbModels/workLocationCollection.js'),
    stateCollection = require('../../dbModels/stateCollection.js'),



    //passwordGenerator = require('../../utils/passwordGenerator.js'),
    //resetRedis = require('../../utils/resetRedis'),
    //mailSender = require('../../utils/mailSender.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.addRole = addRole;
module.exports.getRoles = getRoles;
module.exports.addEmployeeType = addEmployeeType;
module.exports.addCustomerType = addCustomerType;
module.exports.addDepartment = addDepartment;
module.exports.addTimeOffType = addTimeOffType;
module.exports.addInvoiceType = addInvoiceType;
module.exports.addVendorType = addVendorType;
module.exports.addPaymentTerm = addPaymentTerm;
module.exports.addInvoicePaymentTerm = addInvoicePaymentTerm;
module.exports.addEmployeeRole = addEmployeeRole;
module.exports.addCountry = addCountry;
module.exports.addState = addState;
module.exports.getCustomerType = getCustomerType;
module.exports.getDepartment = getDepartment;
module.exports.getEmployeeType = getEmployeeType;
module.exports.getVendorType = getVendorType;
module.exports.getTimeOffType = getTimeOffType;
module.exports.getInvoiceType = getInvoiceType;
module.exports.getPaymentTerm = getPaymentTerm;
module.exports.getInvoicePaymentTerm = getInvoicePaymentTerm;
module.exports.getEmployeeRole = getEmployeeRole;
module.exports.getCountryDetails = getCountryDetails;
module.exports.getStateDetals = getStateDetals;
module.exports.getWorkLocationDetails = getWorkLocationDetails;
module.exports.updateEmployeeType = updateEmployeeType;
module.exports.updateCustomerType = updateCustomerType;
module.exports.updateTimeOffType = updateTimeOffType;
module.exports.updateInvoiceType = updateInvoiceType;
module.exports.updatePaymentTerm = updatePaymentTerm;
module.exports.updateInvoicePaymentTerm = updateInvoicePaymentTerm;
module.exports.updateEmployeeRole = updateEmployeeRole;
module.exports.updateVendorType = updateVendorType;
module.exports.updateDepartment = updateDepartment;
module.exports.deleteCustomerType = deleteCustomerType;
module.exports.deleteEmployeeType = deleteEmployeeType;
module.exports.deleteTimeOffType = deleteTimeOffType;
module.exports.deleteInvoiceType = deleteInvoiceType;
module.exports.deletePaymentTerm = deletePaymentTerm;
module.exports.deleteInvoicePaymentTerm = deleteInvoicePaymentTerm;
module.exports.deleteEmployeeRoleType = deleteEmployeeRoleType;
module.exports.deleteDepartment = deleteDepartment;
module.exports.deleteVendorType = deleteVendorType;
module.exports.deleteCountry = deleteCountry;
module.exports.deleteState = deleteState;
module.exports.deleteWorkLocation = deleteWorkLocation;
module.exports.addWorkLocation = addWorkLocation;
module.exports.updateCountryDetails = updateCountryDetails;
module.exports.updateStateStatus = updateStateStatus;
module.exports.updateWorkLocationStatus = updateWorkLocationStatus;



function addRole(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        role = new rolesCollection({
            name: req.body.roleName,
            discription: req.body.roleDiscription,
            menusAssigned: req.body.menuId,
            submenusassigned: req.body.subMenusId,
            creationDate: Date.now()
        });
        role.save(function(err, data) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            res.json({ successmsg: 'success' });
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getRoles(verifiedUser, req, res, next) {
    rolesCollection.find(function(err, docs) {
        if (err) return res.status(httpStatusCode.DBERROR).send(err);
        if (docs.length > 0) res.json(docs);
        else res.json({ msg: 'Roles not available' });
    });
}

function addEmployeeType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {
            newEmployeeType = new employeeTypeCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });
            newEmployeeType.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function addVendorType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newVendorType = new vendorTypeCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newVendorType.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addCustomerType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newCustomerType = new customerTypeCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newCustomerType.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addTimeOffType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newTimeOffType = new timeOffTypeCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newTimeOffType.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addInvoiceType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newInvoiceType = new invoiceTypeCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newInvoiceType.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addPaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newPaymentTerm = new paymentTermCollection({
                name: req.body.name,
                numberofdays: req.body.numberofdays,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus,

            });

            newPaymentTerm.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addInvoicePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newInvoicePaymentTerm = new invoicePaymentTermCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newInvoicePaymentTerm.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addEmployeeRole(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newEmployeeRole = new employeeRoleCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newEmployeeRole.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addDepartment(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.name != null || req.body.name != undefine) {

            newDepartment = new departmentCollection({
                name: req.body.name,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newDepartment.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {

            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}



function getEmployeeType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        employeeTypeCollection.find(function(err, employeeTypeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (employeeTypeList.length > 0) {
                res.json(employeeTypeList);
            } else {
                res.json({ "msg": "Employee Type is not available" })
            }
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function getCustomerType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        customerTypeCollection.find(function(err, customerTypeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);

            if (customerTypeList.length > 0) {
                res.json(customerTypeList);
            } else {
                res.json({ "msg": "Customer Type is not available please contact admin" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getVendorType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        vendorTypeCollection.find(function(err, vendorsTypeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);

            if (vendorsTypeList.length > 0) {
                res.json(vendorsTypeList);
            } else {
                res.json({ "msg": "Vendor Type is not available please contact admin" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getTimeOffType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        timeOffTypeCollection.find(function(err, timeOffTypeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (timeOffTypeList.length > 0) {
                res.json(timeOffTypeList);
            } else {
                res.json({ "msg": "Times Off Type is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getInvoiceType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        invoiceTypeCollection.find(function(err, invoiceTypeList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (invoiceTypeList.length > 0) {
                res.json(invoiceTypeList);
            } else {
                res.json({ "msg": "Invoice Type is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getPaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        paymentTermCollection.find(function(err, paymentTermList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (paymentTermList.length > 0) {
                res.json(paymentTermList);
            } else {
                res.json({ "msg": "Payment Term is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getEmployeeRole(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        employeeRoleCollection.find(function(err, employeeRoleList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (employeeRoleList.length > 0) {
                res.json(employeeRoleList);
            } else {
                res.json({ "msg": "Employee Role is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getInvoicePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        invoicePaymentTermCollection.find(function(err, invoicePaymentTermList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (invoicePaymentTermList.length > 0) {
                res.json(invoicePaymentTermList);
            } else {
                res.json({ "msg": "Invoice Payment Term is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getDepartment(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        departmentCollection.find(function(err, departmentList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (departmentList.length > 0) {
                res.json(departmentList);
            } else {
                res.json({ "msg": "Department is not available" })
            }
        });

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateVendorType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                vendorTypeCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                vendorTypeCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateEmployeeRole(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                employeeRoleCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                employeeRoleCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateInvoicePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                invoicePaymentTermCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                invoicePaymentTermCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function updatePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                paymentTermCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {
                            data.numberofdays = req.body.numberofdays,
                                data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                paymentTermCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateInvoiceType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                invoiceTypeCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                invoiceTypeCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateTimeOffType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                timeOffTypeCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                timeOffTypeCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateCustomerType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                customerTypeCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                customerTypeCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateDepartment(verifiedUser, req, res, next) {

    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                departmentCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                departmentCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateEmployeeType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                employeeTypeCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.name = req.body.name,
                                data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                employeeTypeCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {

                            data.status = req.body.status,
                                data.updatedon = Date.now()

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}



function updateCountryDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            if (req.body._id) {
                countryCollection.findOne({ '_id': req.body._id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {
                            data.status = req.body.status,
                                data.updatedon = Date.now(),
                                data.name = req.body.name,
                                data.currencyType = req.body.currencyType,
                                data.countryCode = req.body.countryCode,
                                data.callingCode = req.body.callingCode

                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })

            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusUpdate') {
            if (req.body.status && req.body.id) {
                countryCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {
                            data.status = req.body.status,
                                data.updatedon = Date.now()
                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteEmployeeType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.employeeTypeId != null || req.query.employeeTypeId != undefine) {
            employeeTypeCollection.find({ '_id': req.query.employeeTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }


    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function deleteCustomerType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.customerTypeId != null || req.query.customerTypeId != undefine) {
            customerTypeCollection.find({ '_id': req.query.customerTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteTimeOffType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.timeOffTypeId != null || req.query.timeOffTypeId != undefine) {
            timeOffTypeCollection.find({ '_id': req.query.timeOffTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteInvoiceType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.invoiceTypeId != null || req.query.invoiceTypeId != undefine) {
            invoiceTypeCollection.find({ '_id': req.query.invoiceTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deletePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.paymentTermId != null || req.query.paymentTermId != undefine) {
            paymentTermCollection.find({ '_id': req.query.paymentTermId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteInvoicePaymentTerm(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.invoicePaymentTermId != null || req.query.invoicePaymentTermId != undefine) {
            invoicePaymentTermCollection.find({ '_id': req.query.invoicePaymentTermId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function addCountry(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body != null || req.body != undefine) {

            newCountryDetails = new countryCollection({
                name: req.body.name,
                state: req.body.state,
                currencyType: req.body.currencyType,
                countryCode: req.body.countryCode,
                callingCode: req.body.callingCode,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newCountryDetails.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function addState(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            countryCollection.findOne({ '_id': req.body._id })
                .exec(function(err, countryDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    if (countryDetail != null) {
                        var stateDetail = {
                            name: req.body.statename,
                            status: constant.activeStatus
                        }
                        countryDetail.state.push(stateDetail);
                        countryDetail.save(function(err, result) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });
                    } else {
                        res.sendStatus(httpStatusCode.BADREQUEST);
                    }
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }

}

function addWorkLocation(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body != null || req.body != undefine) {
            newWorkLocation = new workLocationCollection({
                country: req.body.country,
                state: req.body.state,
                worklocation: req.body.worklocation,
                creationDate: Date.now(),
                updatedon: Date.now(),
                status: constant.activeStatus
            });

            newWorkLocation.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getCountryDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        countryCollection.find(function(err, countryDetailsList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (countryDetailsList.length > 0) {
                res.json(countryDetailsList);
            } else {
                res.json({ "msg": "Country Details is not available" })
            }
        });


    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getStateDetals(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        stateCollection.find(function(err, stateDetailsList) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (stateDetailsList.length > 0) {
                res.json(stateDetailsList);
            } else {
                res.json({ "msg": "State Details is not available" })
            }
        });


    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getWorkLocationDetails(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        workLocationCollection.find(function(err, worklocationListType) {
            if (err) return res.status(httpStatusCode.DBERROR).send(err);
            if (worklocationListType.length > 0) {
                res.json(worklocationListType);
            } else {
                res.json({ "msg": "WorkList Detail is not available" })
            }
        });


    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteEmployeeRoleType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.deleteEmployeeRoleTypeId != null || req.query.deleteEmployeeRoleTypeId != undefine) {
            employeeRoleCollection.find({ '_id': req.query.deleteEmployeeRoleTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteCountry(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.countryReferenceId != null || req.query.countryReferenceId != undefine) {
            countryCollection.find({ '_id': req.query.countryReferenceId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteState(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.stateReferenceId && req.query.countryReferenceId) {
            countryCollection.findOne({ '_id': req.query.countryReferenceId })
                .exec(function(err, countryDetail) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    if (countryDetail) {
                        if (countryDetail.state.length > 0) {
                            for (var i = 0; i < countryDetail.state.length; i++) {
                                if (countryDetail.state[i]._id == req.query.stateReferenceId) {
                                    countryDetail.state.splice(i, 1);
                                }
                            }
                            countryDetail.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err); } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });

                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                        //res.json({successmsg: 'true'})
                    } else {
                        res.json({ msg: 'Record not available to update' })
                    }

                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteCustomerType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.customerTypeId != null || req.query.customerTypeId != undefine) {
            customerTypeCollection.find({ '_id': req.query.customerTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteVendorType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.vendorTypeId != null || req.query.vendorTypeId != undefine) {
            vendorTypeCollection.find({ '_id': req.query.vendorTypeId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteDepartment(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.departmentId != null || req.query.departmentId != undefine) {
            departmentCollection.find({ '_id': req.query.departmentId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function deleteWorkLocation(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.workLocationReferenceId != null || req.query.workLocationReferenceId != undefine) {
            workLocationCollection.find({ '_id': req.query.workLocationReferenceId }).remove()
                .exec(function(err, removedData) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    res.json({ successmsg: 'true' })
                });
        } else {
            res.json({ "errormsg": "Bad Request" })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}



function updateStateStatus(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            var countryId;
            if (req.body.name._id) countryId = req.body.name._id;
            else countryId = req.body.countryId;
            if (countryId) {
                countryCollection.findOne({ '_id': countryId })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {
                            if (data.state.length > 0) {
                                for (var i = 0; i < data.state.length; i++) {
                                    if (data.state[i]._id == req.body.stateId) {
                                        data.state[i].name = req.body.statename
                                    } else {
                                        /*var stateDetail = {
                                        name : req.body.statename,
                                        status: constant.activeStatus
                                        }
                                        data.state.push(stateDetail);
                                        break;*/
                                    }
                                }
                                data.updatedon = Date.now()
                                data.save(function(err) {
                                    if (err) {
                                        return res.status(httpStatusCode.DBERROR).send(err); } else {
                                        res.json({ successMsg: 'true' })
                                    }
                                });
                            } else {
                                res.json({ msg: 'Record not available to update' })
                            }


                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    });

            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
        if (req.body.oppType == 'statusEdit') {
            if (req.body.status && req.body.id) {
                countryCollection.findOne({ '_id': req.body.id })
                    .exec(function(err, data) {
                        if (err) {
                            return res.status(httpStatusCode.DBERROR).send(err); }
                        if (data) {
                            if (data.state.length > 0) {
                                for (var i = 0; i < data.state.length; i++) {
                                    if (data.state[i]._id == req.body.stateId) {
                                        data.state[i].status = req.body.status,
                                            data.updatedon = Date.now()
                                    }
                                }
                                data.save(function(err) {
                                    if (err) {
                                        return res.status(httpStatusCode.DBERROR).send(err); } else {
                                        res.json({ successMsg: 'true' })
                                    }
                                });

                            } else {
                                res.json({ msg: 'Record not available to update' })
                            }
                        } else {
                            res.json({ msg: 'Record not available to update' })
                        }
                    })
            } else {
                res.sendStatus(httpStatusCode.BADREQUEST);
            }
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateWorkLocationStatus(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.oppType == 'update') {
            workLocationCollection.findOne({ '_id': req.body._id })
                .exec(function(err, data) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    if (data) {
                        data.country = req.body.country,
                            data.state = req.body.state,
                            data.worklocation = req.body.worklocation,
                            data.status = req.body.status,
                            data.updatedon = Date.now()

                        data.save(function(err) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err); } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Record not available to update' })
                    }
                })
        }
        if (req.body.oppType == 'statusEdit') {
            workLocationCollection.findOne({ '_id': req.body.id })
                .exec(function(err, data) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err); }
                    if (data) {

                        data.status = req.body.status,
                            data.updatedon = Date.now()

                        data.save(function(err) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err); } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    } else {
                        res.json({ msg: 'Record not available to update' })
                    }
                })
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
