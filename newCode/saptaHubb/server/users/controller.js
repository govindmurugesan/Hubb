var userModel = require('../../dbModels/usersCollection.js'),
    rolesCollection = require('../../dbModels/rolesCollection.js'),
    menusCollection = require('../../dbModels/menusCollection.js'),
    employeeRoleCollection = require('../../dbModels/employeeRoleCollection.js'),
    //saveRoleCollection = require('../../dbModels/saveRoleCollection.js'),
    //passwordGenerator = require('../../utils/passwordGenerator.js'),
    //resetRedis = require('../../utils/resetRedis'),
    //mailSender = require('../../utils/mailSender.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

/*module.exports.getMenus = getMenus;
module.exports.getEmpRole = getEmpRole;
module.exports.saveRole = saveRole;*/
module.exports.getUserInformation = getUserInformation;
module.exports.getInviteInformation = getInviteInformation;
module.exports.updateUserType = updateUserType;
module.exports.activatePortal = activatePortal;


/*function getMenus(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			menusCollection.find()
			.exec(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}

function getEmpRole(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			employeeRoleCollection.find()
			.exec(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}
*/
function getUserInformation(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        userModel.find({ 'status': constant.activeStatus }, function(err, collection) {

            if (err) {
                return res.status(httpStatusCode.DBERROR).send(err);
            }
            if (collection.length > 0) res.json(collection);
            else res.json({ msg: 'User List not available' })
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function getInviteInformation(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        userModel.find({ 'status': constant.invited }, function(err, collection) {

            if (err) {
                return res.status(httpStatusCode.DBERROR).send(err);
            }
            if (collection.length > 0) res.json(collection);
            else res.json({ msg: 'No invite users' })
        });
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}
/*
function saveRole(verifiedUser,req,res,next){
		if (verifiedUser.verified){
			console.log("req",req);
			nathan = new saveRoleCollection({
				selectRole:req.body.selectRole,
				roleText:req.body.roleText,
				roleCheck:req.body.roleCheck
			});
			console.log("nathan",req.body);
			nathan.save(function(err, collection) {
				if(err){ return res.status(httpStatusCode.DBERROR).send(err) ;}
				if(collection.length > 0) res.json(collection);
				else res.json({msg: 'List not available'})
			});	
		}else{
			res.sendStatus(httpStatusCode.Unauthorized);
		}
}
*/


function updateUserType(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.id != null || req.body.role != undefine) {
            userModel.findOne({ '_id': req.body.id })
                .exec(function(err, data) {
                    if (err) {
                        return res.status(httpStatusCode.DBERROR).send(err);
                    }
                    if (data) {
                        if (data.role.name == constant.superAdmin) {
                            userModel.find({ 'role.name': constant.superAdmin }).exec(function(err, userRoleInfo) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err);
                                }

                                if (userRoleInfo.length > 1) {
                                    data.role = req.body.role
                                    data.save(function(err) {
                                        if (err) {
                                            return res.status(httpStatusCode.DBERROR).send(err);
                                        } else {
                                            res.json({ successMsg: 'true' })
                                        }
                                    });
                                } else {
                                    res.json({ msg: 'Atleast one user should be superadmin' })
                                }
                            })
                        } else {
                            data.role = req.body.role
                            data.save(function(err) {
                                if (err) {
                                    return res.status(httpStatusCode.DBERROR).send(err);
                                } else {
                                    res.json({ successMsg: 'true' })
                                }
                            });
                        }

                    } else {
                        res.json({ msg: 'Record not available to update' })
                    }
                })
        } else {
            res.sendStatus(httpStatusCode.BADREQUEST);
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function activatePortal(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body.id != null || req.body.role != undefine) {
            userModel.findOne({ '_id': req.body.id })
                .exec(function(err, data) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (data) {
                        data.role = req.body.role
                        data.save(function(err) {
                            if (err) {
                                return res.status(httpStatusCode.DBERROR).send(err);
                            } else {
                                res.json({ successMsg: 'true' })
                            }
                        });
                    }

                });
        }
    }
}
