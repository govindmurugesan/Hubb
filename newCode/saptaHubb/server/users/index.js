var express = require('express'),
    router = express.Router(),
  	userController = require('./controller');
  	tokenVerifier = require('../../utils/tokenVerifier');



  router
  .route('/getUserInformation')
  .get(tokenVerifier.verifyToken, userController.getUserInformation); 

   router
  .route('/updateUserType')
  .put(tokenVerifier.verifyToken, userController.updateUserType); 
  
   router
  .route('/getInviteInformation')
  .get(tokenVerifier.verifyToken, userController.getInviteInformation); 

  
  router
  .route('/activatePortal')
  .put(tokenVerifier.verifyToken, userController.activatePortal); 

   module.exports = router;