var express = require('express'),
    router = express.Router(),
    salesReportController = require('./controller'),
    tokenVerifier = require('../../utils/tokenVerifier');


router
    .route('/salesReport')
    .post(tokenVerifier.verifyToken, salesReportController.setTarget)
    .put(tokenVerifier.verifyToken, salesReportController.updateSalesTarget)
    .get(tokenVerifier.verifyToken, salesReportController.getSalesReport);

router
    .route('/checkBDMSalesRecord')
    .get(tokenVerifier.verifyToken, salesReportController.checkBDMSalesRecord);

module.exports = router;
