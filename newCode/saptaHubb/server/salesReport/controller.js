var salesReportCollection = require('../../dbModels/salesReportCollection.js'),
    httpStatusCode = require('../../utils/httpStatusCodeConstant'),
    constant = require('../../utils/constantList');

module.exports.setTarget = setTarget;
module.exports.updateSalesTarget = updateSalesTarget;
module.exports.getSalesReport = getSalesReport;
module.exports.checkBDMSalesRecord = checkBDMSalesRecord;

function setTarget(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body != null || req.body != undefined) {

            newSetTarget = new salesReportCollection({
                bdm: req.body.bdm,
                bdmID: req.body.bdmID,
                q1: req.body.q1,
                q2: req.body.q2,
                q3: req.body.q3,
                q4: req.body.q4,
                financialYearStart: req.body.financialYearStart,
                financialYearEnd: req.body.financialYearEnd
            });

            newSetTarget.save(function(err, data) {
                if (err) return res.status(httpStatusCode.DBERROR).send(err);
                res.json({ successmsg: 'success' });
            });

        } else {
            res.json({ "errormsg": "Bad Request" })
        }

    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}

function updateSalesTarget(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.body._id) {
            salesReportCollection.findOne({ '_id': req.body._id })
                .exec(function(err, targetDetail) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);

                    if (targetDetail != null) {
                        targetDetail.q1 = req.body.q1;
                        targetDetail.q2 = req.body.q2;
                        targetDetail.q3 = req.body.q3;
                        targetDetail.q4 = req.body.q4;

                        targetDetail.save(function(err, data) {
                            if (err) return res.status(httpStatusCode.DBERROR).send(err);
                            res.json({ successmsg: 'success' });
                        });

                    } else {
                        res.json({ msg: 'Target not found' });
                    }
                });
        } else {
            res.json({ msg: 'Employee id not available' });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}


function getSalesReport(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        if (req.query.financialYearStart && req.query.financialYearEnd) {
            salesReportCollection.find({
                    $and: [
                        { 'financialYearStart': { $gte: req.query.financialYearStart, $lt: req.query.financialYearEnd } },
                        { 'financialYearEnd': { $gt: req.query.financialYearStart, $lte: req.query.financialYearEnd } }
                    ]
                })
                .exec(function(err, docs) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (docs.length > 0) res.json(docs);
                    else res.json({ msg: 'Target list not available' });
                });
        } else {
            res.sendStatus(httpStatusCode.Unauthorized);
        }
    }
}

function checkBDMSalesRecord(verifiedUser, req, res, next) {
    if (verifiedUser.verified) {
        var query;
        if (req.query.financialYearStart && req.query.financialYearEnd && (req.query.bdm == undefined||req.query.bdm == '')) {
            query = {
                $and: [
                    { 'financialYearStart': req.query.financialYearStart },
                    { 'financialYearEnd': req.query.financialYearEnd }
                ]
            }
        } else if (req.query.financialYearStart && req.query.financialYearEnd && req.query.bdm) {
            query = {
                $and: [
                    { 'financialYearStart': req.query.financialYearStart },
                    { 'financialYearEnd': req.query.financialYearEnd },
                    { 'bdmID': req.query.bdm }
                ]
            }
        }
        if (query != "") {
            salesReportCollection.find(query)
                .exec(function(err, docs) {
                    if (err) return res.status(httpStatusCode.DBERROR).send(err);
                    if (docs.length > 0) res.json(docs);
                    else res.json({ msg: 'Target is not set for selected BDM' });
                });
        }
    } else {
        res.sendStatus(httpStatusCode.Unauthorized);
    }
}