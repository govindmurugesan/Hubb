var express = require('express'),
    router = express.Router(),
  	authenticateController = require('./controller');
  	//tokenVerifier = require('../../utils/tokenVerifier');


router
  .route('/login')
  .get(authenticateController.loginUser); 

  router
  .route('/forgotpassword')
  .get(authenticateController.forgotPassword); 

   router
  .route('/setpassword')
  .post(authenticateController.setPassword); 

  router
  .route('/logout')
  .get(authenticateController.logOut);

  router
  .route('/inviteUser')
  .post( authenticateController.inviteNewUser);


  module.exports = router;