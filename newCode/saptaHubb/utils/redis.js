var redis = require('redis'),
  redisClient;

  redisClient = redis.createClient(6379);
  console.log('Redis local: Connecting to local redis.');
  
/*if (process.env.REDIS_HOST && process.env.REDIS_PORT && process.env.REDIS_PASSWORD) {
  redisClient = redis.createClient(process.env.REDIS_PORT,
    process.env.REDIS_HOST, {
      no_ready_check: true
    });
  redisClient.auth(process.env.REDIS_PASSWORD, function() {
    console.log('Redis Cloud: Redis Authed and Connected');
  });
} else {
  redisClient = redis.createClient(6379);
  console.log('Redis local: Connecting to local redis.');
}*/

redisClient.on('error', function(err) {
  console.log('Redis: ' + err);
    process.exit(1);
});

redisClient.on('connect', function() {
  console.log('Redis: Started');
});

exports.redis = redis;
exports.redisClient = redisClient;
