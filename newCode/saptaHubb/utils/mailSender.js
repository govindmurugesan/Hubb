var express	= require('express');
	router = express.Router(),
 	nodemailer = require('nodemailer'),
 	jwt = require('jsonwebtoken'),
 	appConstant = require('./appConstant');

exports.sendMail = function sendMail(request, callback) {
	
	
	var current_year =  new Date().getFullYear();
	if(request.type === 'register'){
		var mailOptions = {
		    from: appConstant.email, // sender address 
		    to: request.email, // list of receivers 
		    subject: 'ATP  Register', // Subject line 
		    text: '', // plaintext body 
			html: '<div style=" background-color: #eee; padding: 25px;"><div style=" text-align: center;  background-color: #ffffff; padding: 25px; margin-bottom: 25px; " ><img src="http://saptalabs.com/images/logo_color.png"></img></div><div style=" background-color: #ffffff; padding: 25px;  margin-bottom: 25px; color:#999;"><h1 style="color:#666;">Hello&nbsp;'+request.name+'</h1><h3 style=""><i>Congratulations! you have been registered successfully.</i></h3><p style="">Kindly click below <a href="'+appConstant.appUrl+'/passwordReset?token='+request.token+'&email='+request.email+'&password='+request.password+'">link</a> to activate your account:</p><p></p> </div><div style="padding: 0 25px;  margin-bottom: 25px; color: #999;" ><p>&copy;&nbsp;'+current_year+' &nbsp; All rights Reserved - Saptalabs </p></div></div>'
		};
	}else if(request.type === 'forgotpassword'){
		var mailOptions = {
	    from: appConstant.email, // sender address 
	    to: request.email, // list of receivers
	    subject: 'Password Reset', // Subject line 
	    text: '', // plaintext body 
	    html: 	'<div style=" background-color: #eee; padding: 25px;"><div style=" text-align: center;  background-color: #ffffff; padding: 25px; margin-bottom: 25px; " ><img src="http://saptalabs.com/images/logo_color.png"></img></div><div style=" background-color: #ffffff; padding: 25px;  margin-bottom: 25px; color:#999;"><h1 style="color:#666;">Hello,</h1><h3 style=""><i>Please reset your password </i></h3><p style="">Kindly click this <a href="'+appConstant.appUrl+'/passwordReset?token='+request.token+'&email='+request.email+'&tokenId='+request.password+'">link</a> to reset your password</p></div><div style="padding: 0 25px;  margin-bottom: 25px; color: #999;" ><p>&copy;&nbsp;'+current_year+' &nbsp; All rights Reserved - Saptalabs </p></div></div>'
		};
	
	}else if(request.type === 'invited'){
		var mailOptions = {
	    from: appConstant.email, // sender address 
	    to: request.email, // list of receivers
	    subject: 'Invite New User', // Subject line 
	    text: '', // plaintext body 
	    html: 	'<div style=" background-color: #eee; padding: 25px;"><div style=" text-align: center;  background-color: #ffffff; padding: 25px; margin-bottom: 25px; " ><img src="http://saptalabs.com/images/logo_color.png"></img></div><div style=" background-color: #ffffff; padding: 25px;  margin-bottom: 25px; color:#999;"><h1 style="color:#666;">Hello,</h1><h3 style=""><i>Please reset your password </i></h3><p style="">Kindly click this <a href="'+appConstant.appUrl+'/passwordReset?token='+request.token+'&email='+request.email+'&tokenId='+request.password+'">link</a> to reset your password</p></div><div style="padding: 0 25px;  margin-bottom: 25px; color: #999;" ><p>&copy;&nbsp;'+current_year+' &nbsp; All rights Reserved - Saptalabs </p></div></div>'
		};
		console.log("mailOptions",mailOptions);
	
	}else{
		
	}

	var transporter = nodemailer.createTransport({
	    service: 'gmail',
	    auth: {
	        user: appConstant.email, // my mail
	        pass: appConstant.emailpassword
	    }
	});

	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        callback("",error)
	    }else{
	    	callback("","sent")
		}
	});

}