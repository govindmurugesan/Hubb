var  redis = require('./redis')
     httpStatusCode = require('./httpStatusCodeConstant');
var redisClientObj  = redis.redisClient;

module.exports.verifyToken    = verifyToken;
module.exports.logout = logout;


function verifyToken(req, res, next){
  var checkToken;
  checkToken = getToken(req.headers); 
  if(checkToken.exists){
   redisClientObj.hgetall(checkToken.redisToken, function(err, reply){
      if(err){
        if(res) return res.sendStatus(500);
        else next(new Error(err));
      }
      if(reply === null || undefined){
        if(res)return res.sendStatus(httpStatusCode.Unauthorized); 
        else return next(new Error('You need to be logged in to do this'));
      } else {
        next({verified: true});
      }
    });
  }else{
     return res.sendStatus(httpStatusCode.Unauthorized); 
  }
}


function getToken(reqheaders){
  var tokenInfo = {exists: false, token: ''};
  if(reqheaders && reqheaders.authorization){
    var auth = reqheaders.authorization;
    if(auth){
      tokenInfo.exists = true;
      tokenInfo.token = auth;
      tokenInfo.redisToken = '_token:' + auth;
      return tokenInfo;
    } else {
      return tokenInfo;
    }
  } else {
    return tokenInfo;
  }

}

function logout(req, res){
  if(req){
    expireToken(req.headers);
    //delete req.user;
    return res.json({successMsg: true});
  } else {
    return res.send(401);
  }
}

function expireToken(headers){
  var checkToken = getToken(headers);
  if(checkToken.exists){
    redisClientObj.del(checkToken.redisToken);
  }
}

